# Topological Atlas Codex

-   Text import

Activate virtual environment

```
cd /codex_app/
. ../venv/bin/activate
./manage.py import_text path to the text
```

-   watch

`cd codex_app/codex_app/static/codex_app/    
`

`npx tsc --watch --outDir js --project tsconfig.json --rootDir src/
`

## A tool balanced between making and reflecting

The codex was designed and developed as a part of the research project Topological Atlas.

Its starting point was the questioning of supposedly qualitative research tools available with commercial licenses, that, in fact seemed to generate more quantitative interpretations. The topological codex is an attempt to enable qualitative interpretations of research material, such as texts, interview transciptions, and images.

## First, the terms: the terms codex, building a framework and a shared vocabulary

The terms are qualifiers used across the platform. They constitute the elementary units of meaning that will be associated to textual or visual fragments. They will be put in relation with one another through the interpretation of the research material.

Terms also belong to categories.

> In the case of the Topological Atlas project, various collective work sessions were dedicated to questioning and defining the terms that would constitute the basis for the following steps. The codex evolved along the way, throughout the duration of the project, as need for new meanings emerged from the research itself, or as new frameworks or point of views were brought by the researchers. The definitions of the terms allow for a shared understanding.

## Making annotations

Once there are terms, we can associate them to visual or textual fragments.
This is done through two different interfaces, one dedicated to text, and the other to images.

## Looking at the annotations

A specific view of all the annotations, within the context of the paragraph they emerged from, and linked to all the other terms that were associated with the annotations in the same paragraph is available.

The first relation that emerges between the terms, is that of annotation proximity. This is staged in the viz0-term-pages, a page for each term, which displays all the paragraphs that were annotated with that term. For each paragraph, the other terms that were used to make annotations are displayed, in their pictographic form. We call them the proximity terms.

## Making paths: the path as a narration, the path as relations

The path-making is a device that allow researchers, or participants in the platform to craft a narrative, or an interpretation, based on the existant annotations. Here, the relations between image fragments, text fragments, and terms are made by the researcher, and can be made explicit through adding small transitional texts.

## Reflecting through the spectacular, crossing paths as relations

Eventually, the different paths cross. Their intersection can happen around an image fragment, a text annotation, or a term. This is displayed in the landing page of the edited side of the platform, that is shared with a wider audience.
From the map of the existing paths, the viewer can delve back into the different layers of the material, through different navigational devices:

-   the paths
-   the places
-   the terms and their proximity terms

---

The codex was designed and developed as a part of the research project Topological Atlas.

Its starting point was the questioning of supposedly qualitative research tools available with commercial licenses, that, in fact seemed to generate more quantitative interpretations of a given research material. The topological codex is an attempt to enable qualitative interpretations of research material, such as texts, interview transciptions, and images, while displaying the different perspectives next to each other, and enhance the relations between them.

It is aimed for inter-disciplinary research groups, who wish to visualize the multiple possible interpretations of their shared research material, using a shared language. The tool brings together spatial and semantic analysis, and allows for multiple interpretations of the research material to emerge, and in turn create new relations, and meaning. The relations emerging between the different paths might reveal contradictions, and complexity, which then reveal the specificities of the researchers' situatedness, and framework.

The navigational device of the codex allows for the staging of the visual and textual interpretation towards a wider audience, thereby making some of the research's result available in an interactive way.
