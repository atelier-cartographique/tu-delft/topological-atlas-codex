from django.http import HttpResponse
from django.shortcuts import redirect, render, get_object_or_404
from django.middleware.csrf import get_token
from django.contrib.auth.decorators import login_required
from codex.models import (
    Image,
    ImageAnnotation,
    ParAnnotation,
    Relationship,
    Text,
    Term,
    TermCategory,
    TermCatCat,
    Qualifier,
)
from codex.serializers import (
    serialize_category,
    serialize_image,
    serialize_image_annotation,
    serialize_paragraph,
    serialize_paragraph_annotation,
    serialize_qualifier,
    serialize_term,
)
from json import dumps


def get_root(request):
    if request.user.is_anonymous:
        return render(request, "codex_app/root.html", {})
    return redirect("/index.html")


@login_required
def get_index(request):
    context = {}
    return render(request, "codex_app/index.html", context)


@login_required
def get_text_index(request):
    context = {
        "texts": list(Text.objects.prefetch_related("paragraph_set").all()),
    }
    return render(request, "codex_app/index-text.html", context)


@login_required
def get_image_index(request):
    context = {
        "images": list(Image.objects.all()),
    }
    return render(request, "codex_app/index-image.html", context)


@login_required
def get_term_index(request):
    context = {
        "terms": list(Term.objects.all().filter(is_relation=False).order_by("name")),
    }
    return render(request, "codex_app/index-term.html", context)


@login_required
def get_relationships(request):
    context = {
        "relationships": Relationship.objects.all(),
        "count": Relationship.objects.count(),
    }
    return render(request, "codex_app/rel-list.html", context)


def append_texts_for_term(term, texts):
    for a in term.annotation.select_related("paragraph__text").all():
        text = a.paragraph.text
        if text.id not in texts:
            texts[text.id] = {"text": text, "paragraphs": [], "count": 0}
        if a.paragraph.id not in [p["par"].id for p in texts[text.id]["paragraphs"]]:
            texts[text.id]["paragraphs"].append(
                {"par": a.paragraph, "annotations": [a]}
            )
            texts[text.id]["paragraphs"] = sorted(
                texts[text.id]["paragraphs"], key=lambda x: x["par"].order
            )
        else:
            for p in texts[text.id]["paragraphs"]:
                if p["par"].id == a.paragraph.id:
                    p["annotations"].append(a)

        texts[text.id]["count"] += 1


@login_required
def get_term(request, id):
    term = get_object_or_404(Term, pk=id)
    texts = {}
    append_texts_for_term(term, texts)

    images = {}

    for i in term.images.all():
        if i.image.id not in images:
            images[i.image.id] = {"image": i.image, "annotations": []}
        images[i.image.id]["annotations"].append(i)
    context = {
        "source": Relationship.objects.filter(source=term),
        "destination": Relationship.objects.filter(destination=term),
        "term": term,
        "texts": list(texts.values()),
        "images": list(images.values()),
    }
    return render(request, "codex_app/term.html", context)


@login_required
def get_category(request, id):
    category = get_object_or_404(TermCategory, pk=id)

    texts = {}
    for tcc in TermCatCat.objects.filter(category=category):
        for annot in ParAnnotation.objects.filter(term=tcc.term):
            append_texts_for_term(annot.term, texts)

    images = {}
    for annot in category.images.all():
        if annot.image.id not in images:
            images[annot.image.id] = {"image": annot.image, "annotations": []}
        images[annot.image.id]["annotations"].append(annot)

    context = {
        "category": category,
        "texts": list(texts.values()),
        "images": list(images.values()),
    }

    return render(request, "codex_app/category.html", context)


@login_required
def get_text_list(request):
    context = {
        "texts": Text.objects.all(),
        "count": Text.objects.count(),
    }
    return render(request, "codex_app/text-list.html", context)


# def get_text(request, id):
#     text = get_object_or_404(Text, pk=id)
#     paragraphs = []
#     for par in text.paragraph_set.order_by("order"):
#         blocks = []
#         last_end = 0
#         content = par.content
#         for note in par.annotation.all():
#             start = note.start
#             end = note.end
#             blocks.append({"term": None, "content": content[last_end:start]})
#             blocks.append({"term": note.term, "content": content[start:end]})
#             last_end = end

#         blocks.append({"term": None, "content": content[last_end:]})
#         paragraphs.append(
#             {
#                 "blocks": blocks,
#                 "id": par.id,
#             }
#         )

#     context = {
#         "text": text,
#         "paragraphs": paragraphs,
#         "terms": Term.objects.all(),
#     }
#     return render(request, "codex_app/text.html", context)


@login_required
def get_text(request, id):
    text = get_object_or_404(Text, pk=id)
    paragraphs = text.paragraph_set.order_by("order")
    terms = Term.objects.filter(is_relation=False)
    annotations = ParAnnotation.objects.filter(paragraph__text=text)
    qualifiers = Qualifier.objects.all()
    context = {
        "user": dumps(request.user.id),
        "csrf_token": get_token(request),
        "text": text,
        "paragraphs": dumps([serialize_paragraph(p) for p in paragraphs], indent=2),
        "terms": dumps([serialize_term(t) for t in terms], indent=2),
        "annotations": dumps(
            [serialize_paragraph_annotation(a) for a in annotations], indent=2
        ),
        "qualifiers": dumps([serialize_qualifier(q) for q in qualifiers]),
    }
    return render(request, "codex_app/text.html", context)


@login_required
def get_image(request, id):
    image = get_object_or_404(Image, pk=id)
    terms = Term.objects.filter(is_relation=False)
    categories = TermCategory.objects.all()
    annotations = ImageAnnotation.objects.filter(image=image)
    context = {
        "csrf_token": get_token(request),
        "title": image.name,
        "image": dumps(serialize_image(image)),
        "terms": dumps([serialize_term(t) for t in terms], indent=2),
        "categories": dumps([serialize_category(cat) for cat in categories], indent=2),
        "annotations": dumps(
            [serialize_image_annotation(a) for a in annotations], indent=2
        ),
    }
    return render(request, "codex_app/image.html", context)


@login_required
def get_relationships_image(request):
    import graphviz
    from tempfile import TemporaryDirectory

    dot = graphviz.Digraph(comment="Codex Relationships")
    for rel in Relationship.objects.all():
        dot.node(str(rel.source.id), rel.source.name)
        dot.node(str(rel.destination.id), rel.destination.name)
        dot.edge(str(rel.source.id), str(rel.destination.id), rel.kind.name)

    with TemporaryDirectory() as td:
        dot.render(filename="dot.gv", format="png", directory=td)
        f = open(f"{td}/dot.gv.png", "rb")
        data = f.read()
        f.close()
        return HttpResponse(
            content=data,
            headers={
                "Content-Type": "image/png",
            },
        )


# def append_annotations_for_term(term, annotations):
#     for a in term.annotation.select_related("paragraph__text").all():
#         text = a.paragraph.text
#         if text.id not in texts:
#             texts[text.id] = {"text": text, "paragraphs": [], "count": 0}
#         if a.paragraph.id not in [p["par"].id for p in texts[text.id]["paragraphs"]]:
#             texts[text.id]["paragraphs"].append(
#                 {"par": a.paragraph, "annotations": [a]}
#             )
#             texts[text.id]["paragraphs"] = sorted(
#                 texts[text.id]["paragraphs"], key=lambda x: x["par"].order
#             )
#         else:
#             for p in texts[text.id]["paragraphs"]:
#                 if p["par"].id == a.paragraph.id:
#                     p["annotations"].append(a)

#         texts[text.id]["count"] += 1
