"""codex_app URL Configuration
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings

from .views import (
    get_category,
    get_image,
    get_image_index,
    get_index,
    get_relationships,
    get_relationships_image,
    get_root,
    get_term,
    get_term_index,
    get_text_index,
    get_text_list,
    get_text,
)


urlpatterns = [
    path("index.html", get_index, name="index"),
    path("index-text.html", get_text_index, name="get_text_index"),
    path("index-image.html", get_image_index, name="get_image_index"),
    path("index-term.html", get_term_index, name="get_term_index"),
    path(
        "relationships.html",
        get_relationships,
        name="get_relationships",
    ),
    path(
        "relationships.png",
        get_relationships_image,
        name="get_relationships_image",
    ),
    path(
        "texts.html",
        get_text_list,
        name="get_text_list",
    ),
    path(
        "text/<int:id>.html",
        get_text,
        name="get_text",
    ),
    path(
        "image/<int:id>.html",
        get_image,
        name="get_image",
    ),
    path(
        "term/<int:id>.html",
        get_term,
        name="get_term",
    ),
    path(
        "category/<int:id>.html",
        get_category,
        name="get_category",
    ),
    path("admin/", admin.site.urls),
    path("accounts/", include("django.contrib.auth.urls")),
    path("api/", include(("codex.urls", "codex"), namespace="codex")),
    path("viz0/", include("viz0.urls")),
    # path("viz1/", include("viz1.urls")),
    path("topo/", include("topo.urls")),
    # path("api/", include("codex.urls")),
    # path("__debug__/", include("debug_toolbar.urls")),
    path("", include("viz1.urls")),
]


if getattr(settings, "MEDIA_URL_DEV", None) is not None:
    from django.views.static import serve
    from django.urls import re_path

    MEDIA_ROOT = getattr(settings, "MEDIA_ROOT")
    MEDIA_URL = getattr(settings, "MEDIA_URL_DEV")
    if MEDIA_URL.endswith("/") is False:
        MEDIA_URL = MEDIA_URL + "/"
    if MEDIA_URL.startswith("/"):
        MEDIA_URL = MEDIA_URL[1:]
    urlpatterns += [
        re_path(
            f"^{MEDIA_URL}" + r"(?P<path>.*)$",
            serve,
            {
                "document_root": MEDIA_ROOT,
            },
        ),
    ]
