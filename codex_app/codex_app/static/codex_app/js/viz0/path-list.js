import { index } from "../lib/array.js";
import { attrs, emptyElement, events } from "../lib/dom.js";
import { ANCHOR, BUTTON, CANVAS, DIV, IMG, INPUT, SPAN, TEXTAREA, } from "../lib/html.js";
import { findImage, findImageAnnotation, renderImageAnnotation, } from "../lib/image.js";
import { alt, fromNullable, map, map2, none, orElse, pipe2, some, } from "../lib/option.js";
import { findParAnnotation } from "../lib/paragraph.js";
import { deleteIO, postIO } from "../lib/remote.js";
import { assign, query, update } from "../lib/state.js";
import { findTerm } from "../lib/term.js";
import { createTransitional, findTransitionalText, } from "../lib/transitional.js";
export const currentNode = (pc) => index(pc.index, pc.nodes);
export const getPaths = () => query("paths")
    .concat()
    .sort((a, b) => a.id - b.id);
export const getOpenPath = () => fromNullable(getPaths().find(({ closed }) => !closed));
export const createPath = () => postIO("/api/path/create/", {}).then((path) => update("paths", (old) => old.concat(path)));
export const closePath = (pathId) => postIO(`/api/path/close/${pathId}`, {}).then((path) => update("paths", (old) => old.filter(({ id }) => path.id !== id).concat(path)));
export const openPath = (pathId) => postIO(`/api/path/open/${pathId}`, {}).then((path) => update("paths", (old) => old.filter(({ id }) => path.id !== id).concat(path)));
export const namePath = (path) => postIO(`/api/path/name/${path.id}`, path).then((path) => update("paths", (old) => old.filter(({ id }) => path.id !== id).concat(path)));
const deleteNode = (pathId, nodeId) => deleteIO(`/api/path/delete/${pathId}/${nodeId}`).then((path) => update("paths", (old) => old.filter(({ id }) => path.id !== id).concat(path)));
const swapNode = (path, node, dir) => {
    const sourceIndex = path.nodes.findIndex(({ id }) => id === node.id);
    const withTarget = (tnode) => postIO(`/api/path/swap/${path.id}/${node.id}/${tnode.id}`, {}).then((path) => update("paths", (old) => old.filter(({ id }) => path.id !== id).concat(path)));
    const inc = dir === "down" ? 1 : -1;
    map(withTarget)(index(sourceIndex + inc, path.nodes));
};
const downNode = (path, node) => swapNode(path, node, "down");
const upNode = (path, node) => swapNode(path, node, "up");
const createNodeWithPath = (nodeType, pathId, targetId) => postIO(`/api/path/add/${nodeType}/${pathId}/${targetId}`, {}).then((path) => update("paths", (old) => old.filter(({ id }) => path.id !== id).concat(path)));
export const findPath = (pid) => fromNullable(getPaths().find(({ id }) => id === pid));
export const createNode = (nodeType, targetId) => map(({ id }) => createNodeWithPath(nodeType, id, targetId))(getOpenPath());
const renderCreate = (root) => () => {
    const button = BUTTON("create-path", "create path");
    button.addEventListener("click", createPath);
    root.appendChild(button);
};
const renderNodeTerm = ({ tid }) => map2(map(({ uuid, name, statement }) => DIV("path-node term", DIV("term-head", DIV("", IMG("picto", termIlluURL(uuid))), DIV("label", ANCHOR("", `/viz0/term/${tid}.html`, name))), DIV("description", statement))), orElse(DIV("path-node term  empty")))(findTerm(tid));
const renderNodeImage = (node) => DIV("path-node");
const renderNodeTextNote = (node) => DIV("path-node par-annot", map((a) => a.content)(findParAnnotation(node.tid)));
const withImageAnnotation = (annotation) => {
    const width = 400;
    const height = 400;
    const render = map((i) => {
        const canvas = CANVAS("image-annotation", width, height);
        const ctx = canvas.getContext("2d");
        if (ctx !== null) {
            renderImageAnnotation(i, ctx, annotation, [width, height]);
        }
        return canvas;
    });
    return orElse(DIV("err", "error when loading image"))(render(findImage(annotation.imageId)));
};
const renderNodetransitionalText = (node) => DIV("path-node par-text", map((a) => a.content)(findTransitionalText(node.tid)));
const renderNodeImageNote = (node) => DIV("path-node image-annot", map(withImageAnnotation)(findImageAnnotation(node.tid)));
const labelUp = "⇑";
const labelDown = "⇓";
const renderDown = (path, node) => DIV("up-down", SPAN("up disabled", labelUp), events(SPAN("down", labelDown), (add) => add("click", () => downNode(path, node))));
const renderUpDown = (path, node) => DIV("up-down", events(SPAN("up", labelUp), (add) => add("click", () => upNode(path, node))), events(SPAN("down", labelDown), (add) => add("click", () => downNode(path, node))));
const renderUp = (path, node) => DIV("up-down", events(SPAN("up", labelUp), (add) => add("click", () => upNode(path, node))), SPAN("down disabled", labelDown));
const selectDir = (len, index) => {
    if (len > 1) {
        if (index === 0) {
            return some(renderDown);
        }
        else if (index === len - 1) {
            return some(renderUp);
        }
        return some(renderUpDown);
    }
    return none;
};
const wrapNode = (path, node, index) => (elem) => DIV("node-wrapper", DIV("node-tools", map((render) => render(path, node))(selectDir(path.nodes.length, index)), events(BUTTON("delete", "⤬"), (add) => add("click", () => deleteNode(path.id, node.id)))), elem);
export const renderNode = (node) => {
    switch (node.type) {
        case "te":
            return renderNodeTerm(node);
        case "im":
            return renderNodeImage(node);
        case "ta":
            return renderNodeTextNote(node);
        case "ia":
            return renderNodeImageNote(node);
        case "tt":
            return renderNodetransitionalText(node);
    }
};
const renderWrappedNode = (path) => (node, index) => wrapNode(path, node, index)(renderNode(node));
const renderTransTextInput = () => {
    const textArea = TEXTAREA("");
    return DIV("trans-input", DIV("trans-label", "add a textual transition"), textArea, events(BUTTON("but", "Insert"), (on) => on("click", () => createTransitional(textArea.value).then((tt) => createNode("tt", tt.id)))));
};
const renderPathName = (path) => {
    let name = path.name;
    return attrs(DIV("path-name", events(attrs(INPUT("input-name", "text"), (set) => set("placeholder", path.name ?? "Give it a name")), (on) => on("change", (e) => (name = e.target.value))), events(BUTTON("change", "save"), (on) => on("click", () => {
        if (name !== null) {
            namePath({ ...path, name });
        }
    }))), (set) => set("title", path.id));
};
export const renderPath = (root) => (path) => {
    root.append(DIV("nodes", ...path.nodes.map(renderNode)));
};
const renderListItem = (path) => DIV("path-item", renderPathName(path), map(renderNode)(index(0, path.nodes)), events(BUTTON("load", "open path"), (on) => on("click", () => {
    Promise.all(getPaths()
        .filter(({ id, closed }) => id !== path.id && !closed)
        .map(({ id }) => closePath(id)))
        .then(() => openPath(path.id))
        .then(() => assign("viz0/path-mode", "single"))
        .catch((err) => console.log(err));
})));
const renderList = (root) => {
    root.append(DIV("path-list", ...getPaths().map(renderListItem)));
};
const renderModeSwitch = (current) => DIV("mode-switch", current === "single"
    ? events(BUTTON("", "list of paths"), (on) => on("click", () => assign("viz0/path-mode", "list")))
    : events(BUTTON("", "single path"), (on) => on("click", () => assign("viz0/path-mode", "single"))));
export const renderPathBox = (root) => {
    emptyElement(root);
    const mode = query("viz0/path-mode");
    root.append(renderModeSwitch(mode));
    if (mode === "single") {
        pipe2(getOpenPath(), map(renderPath(root)), alt(renderCreate(root)));
    }
    else {
        renderList(root);
    }
};
//# sourceMappingURL=path-list.js.map