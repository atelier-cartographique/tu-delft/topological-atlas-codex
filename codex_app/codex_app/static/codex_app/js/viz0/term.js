import { init, update } from "../lib/state.js";
import { mount, app } from "../lib/app.js";
import { createNode, renderPathBox } from "../lib/path.js";
import { highlightCode } from "../lib/term.js";
import renderImages from "./image.js";
export const main = (state) => {
    init(state);
    mount(document.getElementById("path-box"), renderPathBox, "paths", "viz0/path-mode");
    mount(document.getElementById("image-box"), renderImages, "annotations/image", "paths");
    app().then(() => update("_version", (v) => v + 1));
    document.querySelectorAll(".text-content").forEach(highlightCode);
    document.querySelectorAll(".code").forEach((e) => {
        e.addEventListener("click", () => {
            console.log(`click => ${e.getAttribute("data-annotation")}`);
            const tid = parseInt(e.getAttribute("data-annotation") ?? "", 10);
            if (!Number.isNaN(tid)) {
                createNode("ta", tid);
            }
        });
    });
};
//# sourceMappingURL=term.js.map