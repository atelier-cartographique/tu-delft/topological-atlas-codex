import { assign, init, query, update } from "../lib/state.js";
import { mount, app } from "../lib/app.js";
import { findPath, getPaths, renderNode, renderPath, } from "./path-list.js";
import { bind, fromNullable, isNone, map, map2 } from "../lib/option.js";
import { emptyElement, events } from "../lib/dom.js";
import { BUTTON, DIV } from "../lib/html.js";
import { index } from "../lib/array.js";
const selectUser = (username) => {
    assign("viz0/user", username);
    assign("viz0/path", null);
};
const isCurrentUser = (username) => query("viz0/user") === username;
const renderUsers = (root) => {
    emptyElement(root);
    getPaths()
        .reduce((acc, path) => acc.add(path.username), new Set())
        .forEach((username) => {
        const className = isCurrentUser(username) ? "user current" : "user";
        root.append(events(DIV(className, username), (on) => on("click", () => selectUser(username))));
    });
};
const selectPath = (path) => {
    assign("viz0/path", path.id);
};
const isCurrentPath = (path) => query("viz0/path") === path.id;
const renderUserPaths = (root) => {
    emptyElement(root);
    const renderListItem = (path) => DIV(isCurrentPath(path) ? "path-item current" : "path-item", path.name ?? "--", map(renderNode)(index(0, path.nodes)), events(BUTTON("load", "open path"), (on) => on("click", () => selectPath(path))));
    const username = query("viz0/user");
    root.append(...getPaths()
        .filter((path) => path.username === username)
        .map(renderListItem));
};
const renderCurrentPath = (root) => {
    emptyElement(root);
    const pathId = fromNullable(query("viz0/path"));
    const render = map(renderPath(root));
    const getPath = bind(findPath);
    map2(getPath, render)(pathId);
    if (isNone(pathId)) {
        root.classList.remove("not-empty");
    }
    else {
        root.classList.add("not-empty");
    }
};
export const main = (state) => {
    init(state);
    mount(document.getElementById("user-list"), renderUsers, "viz0/user");
    mount(document.getElementById("user-paths"), renderUserPaths, "viz0/user", "viz0/path");
    mount(document.getElementById("current-path"), renderCurrentPath, "viz0/path");
    app().then(() => update("_version", (v) => v + 1));
    document.querySelectorAll(".path-box").forEach((e) => {
        const pid = parseInt(e.getAttribute("data-id") ?? "", 10);
        if (!Number.isNaN(pid)) {
            map(renderPath(e))(findPath(pid));
        }
    });
};
//# sourceMappingURL=path.js.map