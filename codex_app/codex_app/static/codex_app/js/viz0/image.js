import { emptyElement, events, removeElement } from "../lib/dom.js";
import { BUTTON, CANVAS, DIV, IMG } from "../lib/html.js";
import { findImage, renderImageAnnotation, } from "../lib/image.js";
import { fromNullable, map, orElse } from "../lib/option.js";
import { createNode, getOpenPath } from "../lib/path.js";
import { query } from "../lib/state.js";
import { iife } from "../lib/util.js";
const getSize = () => ({
    width: 200,
    height: 200,
});
const randColor = () => {
    const r = Math.round(Math.random() * 255);
    const g = Math.round(Math.random() * 255);
    const b = Math.round(Math.random() * 255);
    return `rgba(${r},${g},${b}, 1)`;
};
const renderBig = (root, annotId) => {
    const loader = CANVAS("loader", 100, 100);
    let itv = null;
    const withContext = map((ctx) => {
        let on = "l";
        const frame = () => {
            ctx.clearRect(0, 0, 100, 100);
            const x = iife(() => {
                switch (on) {
                    case "l":
                        return 100 * 0.25;
                    case "c":
                        return 100 * 0.5;
                    case "r":
                        return 100 * 0.75;
                }
            });
            on = iife(() => {
                switch (on) {
                    case "l":
                        return "c";
                    case "c":
                        return "r";
                    case "r":
                        return "l";
                }
            });
            ctx.fillStyle = randColor();
            ctx.beginPath();
            ctx.ellipse(x, 50, 15, 15, 0, 0, 2 * Math.PI);
            ctx.fill();
        };
        itv = setInterval(frame, 300);
        frame();
    });
    withContext(fromNullable(loader.getContext("2d")));
    const img = events(IMG("zzz", `/api/image/annotation/image/${annotId}`), (on) => on("load", () => {
        removeElement(loader);
        if (itv !== null) {
            clearInterval(itv);
        }
    }));
    const tools = DIV("tools", events(BUTTON("close", "close"), (on) => on("click", () => removeElement(root))), events(BUTTON("select", "select"), (on) => on("click", () => {
        createNode("ia", annotId);
        removeElement(root);
    })));
    root.append(DIV("www", loader, img, tools));
};
export const renderImages = (root) => {
    emptyElement(root);
    const { width, height } = getSize();
    const currentTerm = orElse(-1)(fromNullable(query("currentTerm")));
    query("annotations/image")
        .filter((a) => ("termId" in a ? a.termId === currentTerm : false))
        .map((annotation) => {
        const render = map((i) => {
            const canvas = CANVAS("image-annotation", width, height);
            const ctx = canvas.getContext("2d");
            if (ctx !== null) {
                renderImageAnnotation(i, ctx, annotation, [width, height]);
                map((path) => {
                    if (path.nodes
                        .filter(({ type }) => type === "ia")
                        .every(({ tid }) => tid !== annotation.id)) {
                        canvas.classList.add("selectable");
                        canvas.addEventListener("click", () => {
                            const ip = DIV("i-p");
                            renderBig(ip, annotation.id);
                            document.body.append(ip);
                        });
                    }
                })(getOpenPath());
                root.append(canvas);
            }
        });
        render(findImage(annotation.imageId));
    });
};
export default renderImages;
//# sourceMappingURL=image.js.map