import { init } from "../lib/state.js";
import { mount, app } from "../lib/app.js";
import renderImage from "./image.js";
import { clearImage, getImageList, setImage } from "../lib/image.js";
import { map } from "../lib/option.js";
import { index } from "../lib/array.js";
import { DIV } from "../lib/html.js";
import { emptyElement } from "../lib/dom.js";
const renderTools = (root) => {
    emptyElement(root);
    const tools = DIV("tools-inner");
    root.appendChild(tools);
};
export const main = (state) => {
    init(state);
    const canvasBox = document.getElementById("canvas-box");
    const tools = document.getElementById("tools");
    mount(tools, renderTools, "image");
    mount(canvasBox, renderImage, "image");
    app().then(() => map(setImage)(index(0, getImageList())));
    window.setInterval(clearImage, 16);
};
//# sourceMappingURL=index.js.map