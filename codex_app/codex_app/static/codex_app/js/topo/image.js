import { findCategory } from "../lib/category.js";
import { getAnnotationsForImage, getGridScale, GRID_SIZE, TRIANGLE_SIDE, selectedOpsRow, clearImage, } from "../lib/image.js";
import { fromNullable, map, orElse, pipe2 } from "../lib/option.js";
import { assign } from "../lib/state.js";
import { findTerm } from "../lib/term.js";
import { placesExtent, placesWithPoint, } from "../lib/place.js";
const point = (x, y) => ({ x, y });
const mulS = (n, { x, y }) => ({ x: x * n, y: y * n });
const divS = (n, { x, y }) => ({ x: x / n, y: y / n });
const add = (p1, p2) => ({ x: p1.x + p2.x, y: p1.y + p2.y });
const isTriangle = (ns) => ns.length === 3;
let inited = false;
const canvas = {
    back: document.createElement("canvas"),
    middle: document.createElement("canvas"),
    fore: document.createElement("canvas"),
};
const context = {
    back: null,
    middle: null,
    fore: null,
};
const getContext = (key) => {
    let ctx = context[key];
    while (ctx === null) {
        ctx = canvas[key].getContext("2d");
        context[key] = ctx;
    }
    return ctx;
};
const initCanvas = (root, key) => {
    const { width, height } = root.getBoundingClientRect();
    canvas[key].width = width * 2;
    canvas[key].height = height * 2;
    canvas[key].setAttribute("class", `canvas-${key}`);
    root.appendChild(canvas[key]);
};
const getSize = () => ({
    width: canvas.fore.width,
    height: canvas.fore.height,
});
const line = (start, end) => [start, end];
const pointToStr = ({ x, y }) => `(${x}; ${y})`;
const lineToStr = ([start, end]) => `[${pointToStr(start)}, ${pointToStr(end)}]`;
const lineSet = () => {
    const _values = [];
    const _rest = [];
    const eqPoint = (a, b) => Math.abs(a.x - b.x) < 0.1 && Math.abs(a.y - b.y) < 0.1;
    const eq = (l0, l1) => (eqPoint(l0[0], l1[0]) && eqPoint(l0[1], l1[1])) ||
        (eqPoint(l0[0], l1[1]) && eqPoint(l0[1], l1[0]));
    const indexOf = (li) => _values.findIndex((l) => eq(li, l));
    const has = (li) => indexOf(li) >= 0;
    const put = (li) => {
        const index = indexOf(li);
        if (index < 0) {
            _values.push(li);
        }
        else {
            _values.splice(index, 1);
            _rest.push(li);
        }
    };
    const values = () => _values;
    const rest = () => _rest;
    return {
        has,
        put,
        values,
        rest,
    };
};
const mergeTriangles = (ts) => {
    const lines = lineSet();
    ts.forEach(([p1, p2, p3]) => {
        lines.put(line(p1, p2));
        lines.put(line(p2, p3));
        lines.put(line(p3, p1));
    });
    return lines;
};
const highlight = (hit) => {
    const ctx = getContext("fore");
    const { width, height } = getSize();
    const set = mergeTriangles(hit.triangles);
    ctx.clearRect(0, 0, width, height);
    ctx.save();
    ctx.beginPath();
    ctx.strokeStyle = "white";
    ctx.lineWidth = 8;
    ctx.lineCap = "round";
    set.values().forEach(([start, end]) => {
        ctx.moveTo(start.x, start.y);
        ctx.lineTo(end.x, end.y);
    });
    ctx.stroke();
    ctx.beginPath();
    ctx.strokeStyle = "#8dcaff";
    ctx.lineWidth = 1;
    ctx.lineCap = "round";
    set.rest().forEach(([start, end]) => {
        ctx.moveTo(start.x, start.y);
        ctx.lineTo(end.x, end.y);
    });
    ctx.stroke();
    ctx.restore();
};
const init = (root) => {
    if (!inited) {
        inited = true;
        initCanvas(root, "back");
        initCanvas(root, "middle");
        initCanvas(root, "fore");
        const back = getContext("back");
        back.fillRect(0, 0, canvas.back.width, canvas.back.height);
        const middle = getContext("middle");
        middle.clearRect(0, 0, canvas.middle.width, canvas.middle.height);
        const fore = getContext("fore");
        fore.clearRect(0, 0, canvas.fore.width, canvas.fore.height);
        let annotId = null;
        let cleared = false;
        canvas.fore.addEventListener("click", clearImage);
    }
};
const sign = ([p1, p2, p3]) => {
    return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y) > 0;
};
const inTriangle = (pt) => ([p1, p2, p3]) => {
    const d1 = sign([pt, p1, p2]);
    const d2 = sign([pt, p2, p3]);
    const d3 = sign([pt, p3, p1]);
    const result = (d1 && d2 && d3) || !(d1 || d2 || d3);
    return result;
};
const centroid = ([p1, p2, p3]) => ({
    x: (p1.x + p2.x + p3.x) / 3,
    y: (p1.y + p2.y + p3.y) / 3,
});
const gravity = (ts) => divS(ts.length, ts.map(centroid).reduce((acc, p) => add(acc, p)));
export const setClipping = (ctx, annotations, gridXScale, gridYScale) => {
    ctx.beginPath();
    const clippingRow = (y, startIndex) => {
        let r = selectedOpsRow(y);
        for (let i = 0; i < GRID_SIZE; i += 1) {
            r = r.fn(startIndex + i, r.acc, r.x, r.y);
        }
        return r.acc;
    };
    const zones = annotations.map((annotation) => {
        const triangles = [];
        for (let i = 0; i < GRID_SIZE; i += 1) {
            clippingRow(i * TRIANGLE_SIDE, i * GRID_SIZE).forEach(({ n, ops }) => {
                if (annotation.triangles.indexOf(n) >= 0) {
                    let points = [];
                    for (const op of ops) {
                        switch (op.tag) {
                            case "M":
                                ctx.moveTo(gridXScale * op.x, gridYScale * op.y);
                                points.push(point(gridXScale * op.x, gridYScale * op.y));
                                break;
                            case "L":
                                ctx.lineTo(gridXScale * op.x, gridYScale * op.y);
                                points.push(point(gridXScale * op.x, gridYScale * op.y));
                                break;
                            case "Z":
                                ctx.closePath();
                                break;
                        }
                    }
                    const mat = ctx.getTransform();
                    const original = points.map((p) => {
                        const orig = mat.transformPoint(p);
                        return {
                            x: orig.x,
                            y: orig.y,
                        };
                    });
                    if (isTriangle(original)) {
                        triangles.push(original);
                    }
                }
            });
        }
        return { triangles, annotation };
    });
    if (zones.length > 0) {
        ctx.clip();
    }
    return zones;
};
const catName = (cid) => pipe2(findCategory(cid), map(({ name }) => name), orElse("~~~~"));
const termName = (tid) => pipe2(findTerm(tid), map(({ name }) => name), orElse("~~~~"));
export const label = (ctx, annotations, gridXScale, gridYScale) => {
    const clippingRow = (y, startIndex) => {
        let r = selectedOpsRow(y);
        for (let i = 0; i < GRID_SIZE; i += 1) {
            r = r.fn(startIndex + i, r.acc, r.x, r.y);
        }
        return r.acc;
    };
    ctx.font = "bold 200px sans";
    ctx.fillStyle = "white";
    ctx.strokeStyle = "white";
    ctx.lineWidth = 5;
    annotations.map((annotation) => {
        const triangles = [];
        for (let i = 0; i < GRID_SIZE; i += 1) {
            clippingRow(i * TRIANGLE_SIDE, i * GRID_SIZE).forEach(({ n, ops }) => {
                if (annotation.triangles.indexOf(n) >= 0) {
                    let points = [];
                    for (const op of ops) {
                        switch (op.tag) {
                            case "M":
                                points.push(point(gridXScale * op.x, gridYScale * op.y));
                                break;
                            case "L":
                                points.push(point(gridXScale * op.x, gridYScale * op.y));
                                break;
                            case "Z":
                                break;
                        }
                    }
                    if (isTriangle(points)) {
                        triangles.push(points);
                    }
                }
            });
        }
        if (triangles.length > 0) {
            const { x, y } = gravity(triangles);
            const name = termName(annotation.termId);
            const met = ctx.measureText(name);
            ctx.strokeText(name, x - met.width / 2, y + met.actualBoundingBoxAscent / 2);
        }
    });
    ctx.restore();
};
export const label2 = (ctx, annotations, gridXScale, gridYScale) => {
    const clippingRow = (y, startIndex) => {
        let r = selectedOpsRow(y);
        for (let i = 0; i < GRID_SIZE; i += 1) {
            r = r.fn(startIndex + i, r.acc, r.x, r.y);
        }
        return r.acc;
    };
    ctx.save();
    ctx.font = "bold 130px sans";
    ctx.fillStyle = "white";
    annotations.map((annot) => {
        let minX = Number.MAX_VALUE;
        let minY = Number.MAX_VALUE;
        let maxX = Number.MIN_VALUE;
        let maxY = Number.MIN_VALUE;
        const updateExtent = (x, y) => {
            minX = Math.min(minX, x);
            minY = Math.min(minY, y);
            maxX = Math.max(maxX, x);
            maxY = Math.max(maxY, y);
        };
        for (let i = 0; i < GRID_SIZE; i += 1) {
            clippingRow(i * TRIANGLE_SIDE, i * GRID_SIZE).forEach(({ n, ops }) => {
                if (annot.triangles.indexOf(n) >= 0) {
                    for (const op of ops) {
                        switch (op.tag) {
                            case "M":
                                updateExtent(gridXScale * op.x, gridYScale * op.y);
                                break;
                            case "L":
                                updateExtent(gridXScale * op.x, gridYScale * op.y);
                                break;
                            case "Z":
                                break;
                        }
                    }
                }
            });
        }
        const dx = maxX - minX;
        const dy = maxY - minY;
        const centerX = minX + dx / 2;
        const centerY = minY + dy / 2;
        const name = termName(annot.termId);
        ctx.fillText(name, centerX, centerY);
    });
    ctx.restore();
};
const { clearZones, pushZones, testZones } = (() => {
    let zones = [];
    const clearZones = () => (zones = []);
    const pushZones = (zs) => (zones = zs);
    const testZones = (x, y) => fromNullable(zones.find(({ triangles }) => triangles.findIndex(inTriangle(mulS(2, point(x, y)))) >= 0));
    return { clearZones, pushZones, testZones };
})();
const getCenteringMatrix = (width, height, imgWidth, imgHeight, origin = [0, 0]) => {
    const scale = imgWidth > imgHeight ? width / imgWidth : height / imgHeight;
    const { tx, ty } = (() => {
        if (imgWidth > imgHeight) {
            return {
                tx: 0,
                ty: (height - imgHeight * scale) / 2,
            };
        }
        return {
            tx: (width - imgWidth * scale) / 2,
            ty: 0,
        };
    })();
    const base = new DOMMatrix();
    const translated = base.translate(origin[0] + tx, origin[1] + ty);
    const scaled = translated.scale(scale, scale);
    return scaled;
};
const withImage = (i) => {
    const ctx = getContext("middle");
    const { width, height } = getSize();
    const img = new Image(i.file.width, i.file.height);
    clearZones();
    img.addEventListener("load", () => {
        ctx.save();
        ctx.fillStyle = "rgba(0,0,0, 0.3)";
        ctx.fillRect(0, 0, width, height);
        const cm = getCenteringMatrix(width, height, i.file.width, i.file.height);
        ctx.setTransform(cm);
        const annotations = getAnnotationsForImage(i.id);
        const { xScale, yScale } = getGridScale(i.file.width, i.file.height);
        ctx.save();
        pushZones(setClipping(ctx, annotations, xScale, yScale));
        ctx.drawImage(img, 0, 0);
        ctx.restore();
        label(ctx, annotations, xScale, yScale);
        ctx.restore();
    }, false);
    img.src = i.file.url;
};
const withoutImage = () => {
    const ctx = getContext("back");
};
const rad = (degree) => (degree * Math.PI) / 180;
const wrapOp = (ctx, f) => {
    ctx.save();
    f(ctx);
    ctx.restore();
};
const drawImage = (ctx, width, height, transform) => (i, x, y) => {
    const img = new Image(i.file.width, i.file.height);
    img.addEventListener("load", () => {
        wrapOp(ctx, (ctx) => {
            const cm = getCenteringMatrix(width, height, i.file.width, i.file.height);
            ctx.setTransform(transform.multiply(cm));
            const annotations = getAnnotationsForImage(i.id);
            const { xScale, yScale } = getGridScale(i.file.width, i.file.height);
            wrapOp(ctx, (ctx) => {
                pushZones(setClipping(ctx, annotations, xScale, yScale));
                ctx.drawImage(img, x, y);
            });
        });
    }, false);
    img.src = i.file.url;
};
const { getMove, setMove } = (() => {
    let mx = 0.5;
    let my = 0.5;
    const setMove = (x, y) => {
        const { width, height } = getSize();
        mx = 1 - x / (width / 2);
        my = y / (height / 2);
    };
    const getMove = () => ({ mx, my });
    return { getMove, setMove };
})();
const move = ([x, y]) => {
    const { mx, my } = getMove();
    const dx = Math.random() - mx;
    const dy = Math.random() - my;
    return [x + dx, y + dy];
};
let firstClear = false;
export const renderImage = (root) => {
    if (root) {
        init(root);
        root.addEventListener("mousemove", (e) => setMove(e.x, e.y));
        const { width, height } = getSize();
        const ctx = getContext("fore");
        if (!firstClear) {
            firstClear = true;
            ctx.clearRect(0, 0, width, height);
        }
        else {
            ctx.fillStyle = "rgba(0,0,0,0.04)";
            ctx.fillRect(0, 0, width, height);
        }
        const [minx, miny, maxx, maxy] = placesExtent();
        const ew = maxx - minx;
        const eh = maxy - miny;
        const hs = width / 360;
        const vs = height / 180;
        const base = new DOMMatrix();
        const translated = base.translate(width / 2, height / 2);
        const scaled = translated.scale(hs, -vs);
        ctx.save();
        ctx.setTransform(scaled);
        ctx.fillStyle = "green";
        ctx.beginPath();
        ctx.fillRect(-6 / hs, -6 / vs, 12 / hs, 12 / vs);
        ctx.beginPath();
        const { mx, my } = getMove();
        ctx.strokeStyle = "green";
        const dx = ((0.5 - mx) * 1000) / hs;
        const dy = ((0.5 - my) * 1000) / vs;
        ctx.moveTo(-dx, -dy);
        ctx.lineTo(dx, dy);
        ctx.stroke();
        ctx.fillStyle = "blue";
        ctx.beginPath();
        ctx.fillRect(minx - 6 / hs, miny - 6 / vs, 12 / hs, 12 / vs);
        ctx.fillRect(minx - 6 / hs, maxy - 6 / vs, 12 / hs, 12 / vs);
        ctx.fillRect(maxx - 6 / hs, miny - 6 / vs, 12 / hs, 12 / vs);
        ctx.fillRect(maxx - 6 / hs, maxy - 6 / vs, 12 / hs, 12 / vs);
        ctx.fillStyle = "red";
        const places = placesWithPoint().map((p) => ({
            ...p,
            point: {
                ...p.point,
                coordinates: move(p.point.coordinates),
            },
        }));
        placesWithPoint().forEach((place) => {
            const { point, name } = place;
            const [x, y] = point.coordinates;
            ctx.beginPath();
            ctx.fillRect(x - 3 / hs, y - 3 / vs, 6 / hs, 6 / vs);
        });
        ctx.restore();
        assign("places", places);
    }
};
export default renderImage;
//# sourceMappingURL=image.js.map