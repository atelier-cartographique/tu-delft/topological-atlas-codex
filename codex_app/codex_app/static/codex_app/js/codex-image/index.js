import { init, update } from "../lib/state.js";
import { mount, app } from "../lib/app.js";
import { renderTooltip } from "../lib/tooltip.js";
import { renderAnnotations, renderImage } from "../lib/image.js";
import { renderCodex } from "./codex.js";
export const main = (state) => {
    init(state);
    const image = document.getElementById("image");
    const annot = document.getElementById("annot");
    const codex = document.getElementById("codex");
    const tooltip = document.getElementById("tooltip-box");
    mount(image, renderImage, "categories", "annotations/image", "image/loaded");
    mount(annot, renderAnnotations, "categories", "annotations/image", "image/loaded");
    mount(codex, renderCodex, "categories", "annotations/image", "image/select-list");
    mount(tooltip, renderTooltip, "tooltip");
    app().then(() => update("_version", (v) => v + 1));
};
//# sourceMappingURL=index.js.map