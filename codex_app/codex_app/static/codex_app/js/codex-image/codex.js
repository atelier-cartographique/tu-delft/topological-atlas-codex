import { findTerm, postImageAnnotation } from "../lib/term.js";
import { ANCHOR, BUTTON, DIV, H3 } from "../lib/html.js";
import { emptyElement } from "../lib/dom.js";
import { map } from "../lib/option.js";
import { getCurrentImage, getImageSelection, } from "../lib/image.js";
import { assign, query, update } from "../lib/state.js";
import { hasSelection } from "../lib/app.js";
const selectForImage = (term) => map((image) => {
    const triangles = getImageSelection();
    postImageAnnotation({
        tag: "term",
        imageId: image.id,
        termId: term.id,
        triangles,
    })
        .then((a) => update("annotations/image", (as) => as.concat(a)))
        .catch((err) => console.log("post annotation error", err));
    assign("image/select-list", []);
})(getCurrentImage());
export const renderTermButtonForImage = (term) => {
    const button = BUTTON("cat-select", term.name);
    if (hasSelection()) {
        button.addEventListener("click", () => selectForImage(term));
    }
    else {
        button.setAttribute("disabled", "true");
    }
    const node = DIV("cat-wrapper", button);
    return node;
};
const renderTerm = (term) => {
    const button = BUTTON("term-select term-select-short", "✓");
    if (hasSelection()) {
        button.addEventListener("click", () => selectForImage(term));
    }
    else {
        button.setAttribute("disabled", "true");
    }
    const anchor = ANCHOR("term-link", `/term/${term.id}.html`, term.name);
    anchor.setAttribute("target", "_blank");
    const title = H3("term-name", anchor, button);
    const statement = DIV("term-statement", term.statement);
    const node = DIV("term", title, statement);
    return node;
};
export const renderCodex = (root) => {
    emptyElement(root);
    const buttons = DIV("buttons");
    const terms = DIV("categories");
    query("annotations/image")
        .filter((a) => a.tag === "term")
        .map((a) => a)
        .reduce((acc, { termId }) => {
        const tup = acc.find(({ t, c }) => t === termId);
        if (tup === undefined) {
            return acc.concat({ t: termId, c: 1 });
        }
        tup.c += 1;
        return acc;
    }, [])
        .sort((a, b) => b.c - a.c)
        .forEach(({ t }) => map((term) => buttons.appendChild(renderTermButtonForImage(term)))(findTerm(t)));
    query("terms")
        .sort((a, b) => a.name.localeCompare(b.name))
        .forEach((t) => {
        terms.appendChild(renderTerm(t));
    });
    root.appendChild(buttons);
    root.appendChild(terms);
};
//# sourceMappingURL=codex.js.map