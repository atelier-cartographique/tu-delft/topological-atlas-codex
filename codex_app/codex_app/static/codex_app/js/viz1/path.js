import { emptyElement, events } from "../lib/dom.js";
import { CANVAS, DIV, IMG, SPAN } from "../lib/html.js";
import { renderImageAnnotation, findImage, findImageAnnotation, getCurrentImage, getAnnotationsForImage, } from "../lib/image.js";
import { bind, fromNullable, map, orElse, orElseL, pipe2, } from "../lib/option.js";
import { findParAnnotation } from "../lib/paragraph.js";
import { currentNode, } from "../lib/path.js";
import { findTransitionalText } from "../lib/transitional.js";
import { assign, query } from "../lib/state.js";
import { findTerm, getViz1Term } from "../lib/term.js";
import { navigateHome } from "./route.js";
const selectPath = (pc) => assign("viz1/path", pc);
const incWrap = (i, len) => (i < len - 1 ? i + 1 : 0);
export const forward = (pc) => assign("viz1/path", { ...pc, index: incWrap(pc.index, pc.nodes.length) });
export const setCurrentPath = (pc, index) => assign("viz1/path", { ...pc, index });
export const resetPath = () => assign("viz1/path", null);
export const getSelectPath = () => fromNullable(query("viz1/path"));
export const getCurrentNode = () => bind(currentNode)(getSelectPath());
const union = (a, b) => {
    const u = new Set(a);
    b.forEach((v) => u.add(v));
    return u;
};
const termInPath = (tids) => (p) => {
    for (const n of p.nodes) {
        if (n.type === "te" && tids.indexOf(n.tid) >= 0) {
            return true;
        }
    }
    return false;
};
const getImageForTerm = (tid) => {
    const annot = query("annotations/image").find((a) => a.tag === "term" && a.termId === tid);
    return bind(({ imageId }) => findImage(imageId))(fromNullable(annot));
};
const renderNodeTerm = ({ tid }) => {
    const className = "path-node term";
    const node = DIV(className);
    map(({ uuid, name, statement }) => {
        node.append(DIV("_x", DIV("term-head", DIV("", IMG("term-picto", termIlluURL(uuid))), SPAN("term-name", name)), DIV("description", statement)));
    })(findTerm(tid));
    return node;
};
const renderNodeImage = (node) => DIV("path-node");
const renderNodeTextNote = (node) => DIV("path-node par-annot", DIV("window", map((a) => a.content)(findParAnnotation(node.tid))));
const withImageAnnotation = (annotation) => {
    const width = 500;
    const height = 500;
    const render = map((i) => {
        const canvas = CANVAS("image-annotation", width, height);
        const ctx = canvas.getContext("2d");
        if (ctx !== null) {
            renderImageAnnotation(i, ctx, annotation, [width, height]);
        }
        return canvas;
    });
    return orElse(DIV("err", "error when loading image"))(render(findImage(annotation.imageId)));
};
const renderNodeImageNote = (node) => DIV("path-node image-annot", map(withImageAnnotation)(findImageAnnotation(node.tid)));
const renderNodetransitionalText = (node) => DIV("path-node par-text", map((a) => a.content)(findTransitionalText(node.tid)));
const renderNode = (node) => {
    switch (node.type) {
        case "te":
            return renderNodeTerm(node);
        case "im":
            return renderNodeImage(node);
        case "ta":
            return renderNodeTextNote(node);
        case "ia":
            return renderNodeImageNote(node);
        case "tt":
            return renderNodetransitionalText(node);
    }
};
const renderPathNavLink = (pc) => events(DIV("path", DIV("path-name", pc.name ?? pc.id)), (on) => on("click", () => selectPath(pc)));
const termInPath2 = (tid, p) => {
    for (let i = 0; i < p.nodes.length; i++) {
        const node = p.nodes[i];
        if (node.type === "te" && tid === node.tid) {
            return i;
        }
    }
    return -1;
};
const withCurrentPath = map((path) => DIV("path-cursor", renderNode(path.nodes[path.index]), events(DIV("cursor", "⤬"), (on) => on("click", resetPath)), events(DIV("cursor", "↬"), (on) => on("click", () => forward(path)))));
const renderBackIndex = () => DIV("to-index", events(DIV("label", "◢ map of paths"), (on) => on("click", navigateHome)));
const withCurrentPath2 = map((path) => DIV("path-x", renderBackIndex(), events(DIV("cursor", "⤬"), (on) => on("click", resetPath)), DIV("path-title", path.name || "-"), ...path.nodes.map(renderNode)));
export const renderPathNav = (root) => {
    emptyElement(root);
    const allPaths = query("paths");
    const selectedPaths = [];
    const withImage = map((img) => {
        const annotations = getAnnotationsForImage(img.id);
        annotations.forEach((a) => {
            allPaths.forEach((path) => {
                const index = termInPath2(a.termId, path);
                if (index >= 0) {
                    selectedPaths.push({ ...path, index });
                }
            });
        });
    });
    const withTerm = map((term) => {
        allPaths.forEach((path) => {
            const index = termInPath2(term.id, path);
            if (index >= 0) {
                selectedPaths.push({ ...path, index });
            }
        });
    });
    withImage(getCurrentImage());
    withTerm(getViz1Term());
    const element = pipe2(getSelectPath(), withCurrentPath2, orElseL(() => DIV("xxx", renderBackIndex(), ...selectedPaths.map(renderPathNavLink))));
    root.append(element);
};
//# sourceMappingURL=path.js.map