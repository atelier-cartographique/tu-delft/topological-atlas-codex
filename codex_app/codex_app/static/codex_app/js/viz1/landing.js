import { attrs, emptyElement, events } from "../lib/dom.js";
import { CANVAS, DIV, SPAN } from "../lib/html.js";
import { renderImageAnnotation, findImage, findImageAnnotation, renderImageAnnotationInExtentFull, } from "../lib/image.js";
import { alt, bind, fromNullable, map, map2, orElse, } from "../lib/option.js";
import { findParAnnotation } from "../lib/paragraph.js";
import { getPaths, } from "../lib/path.js";
import { findTerm } from "../lib/term.js";
import { findTransitionalText } from "../lib/transitional.js";
import { fst, mapFirst, mapSecond, snd, tuple } from "../lib/tuple.js";
import { iife } from "../lib/util.js";
import { navigateImage, navigateTerm } from "./route.js";
import { drawCurve, pointsToCurve } from "../lib/smooth-curve.js";
import { getImageLocation } from "../lib/place.js";
import { assign, query } from "../lib/state.js";
import { termFont } from "./image.js";
const px = (n) => `${n}px`;
const wrapDraw = (ctx, f) => {
    ctx.save();
    f();
    ctx.restore();
};
export const renderLanding = (root) => {
    emptyElement(root);
    const view = DIV("view");
    root.append(view);
    const without = (path, paths) => paths.filter(({ id }) => path.id !== id);
    let paths = getPaths();
    let rect = { left: 1000, right: 1000, bottom: 120, top: 120 };
    let dir = "right";
    const posNode = (node) => {
        const elem = renderNode(node);
        root.append(elem);
        const { width, height } = elem.getBoundingClientRect();
        const { x, y } = iife(() => {
            switch (dir) {
                case "right":
                    return { x: rect.right, y: rect.top };
                case "left":
                    return { x: rect.left - width, y: rect.top };
                case "up":
                    return { x: rect.left, y: rect.top - height };
                case "down":
                    return { x: rect.left, y: rect.bottom };
            }
        });
        elem.style.left = px(x);
        elem.style.top = px(y);
        const { left, right, bottom, top } = elem.getBoundingClientRect();
        rect = { left, right, bottom, top };
        return elem;
    };
    const splitAt = (i, path) => {
        const leftNodes = path.nodes.slice(0, i);
        const rightNodes = path.nodes.slice(i + 1);
        return tuple({ ...path, nodes: leftNodes.reverse() }, { ...path, nodes: rightNodes });
    };
    const processNode = (path, paths, depth) => (node) => {
        if (depth > 3) {
            return;
        }
        const nodeElement = events(attrs(posNode(node), (set) => set("data-path", path.id)), (on) => on("click", () => {
            view.querySelectorAll(".path-node").forEach((e) => e.classList.remove("selected"));
            view.querySelectorAll(`[data-path="${path.id}"]`).forEach((e) => e.classList.add("selected"));
            view.querySelectorAll(`[data-intersects="${path.id}"]`).forEach((e) => e.classList.add("selected"));
        }));
        view.append(nodeElement);
        findIntersectingPaths(node, without(path, paths)).map((t) => {
            const splitIndex = snd(t);
            const path = fst(t);
            nodeElement.classList.add("intersection");
            attrs(nodeElement, (set) => set("data-intersects", path.id));
            const parts = splitAt(splitIndex, path);
            const doLeft = mapFirst((path) => path.nodes.map(processNode(path, without(path, paths), (depth += 1))));
            const doRight = mapSecond((path) => path.nodes.map(processNode(path, without(path, paths), (depth += 1))));
            dir = iife(() => {
                switch (dir) {
                    case "left":
                    case "right":
                        return "up";
                    case "up":
                    case "down":
                        return "left";
                }
            });
            doLeft(parts);
            dir = iife(() => {
                switch (dir) {
                    case "left":
                    case "right":
                        return "down";
                    case "up":
                    case "down":
                        return "right";
                }
            });
            doRight(parts);
        });
    };
    paths.map((path, i) => {
        path.nodes.map(processNode(path, paths, 0));
        dir = iife(() => {
            switch (dir) {
                case "left":
                case "right":
                    return "down";
                case "up":
                case "down":
                    return "right";
            }
        });
    });
};
const deg2rad = (d) => (d * Math.PI) / 180;
const polar2cartesian = (R, theta, center) => {
    const x = R * Math.cos(deg2rad(theta)) + center[0];
    const y = R * Math.sin(deg2rad(theta)) + center[1];
    return [x, y];
};
const extentCenter = ([x1, y1, x2, y2]) => [
    x1 + (x2 - x1) / 2,
    y1 + (y2 - y1) / 2,
];
const extentWidth = ([x1, y1, x2, y2]) => x2 - x1;
const extentHeight = ([x1, y1, x2, y2]) => y2 - y1;
const scaleExtent = (s, e) => {
    const c = extentCenter(e);
    const w = (s * extentWidth(e)) / 2;
    const h = (s * extentHeight(e)) / 2;
    return [c[0] - w, c[1] - h, c[0] + w, c[1] + h];
};
const transformExtent = (t, e) => {
    const min = t.transformPoint(new DOMPoint(e[0], e[1]));
    const max = t.transformPoint(new DOMPoint(e[2], e[3]));
    return [min.x, min.y, max.x, max.y];
};
const intersects = ([ax1, ay1, ax2, ay2], [bx1, by1, bx2, by2]) => {
    return !(ax2 < bx1 || bx2 < ax1 || ay2 < by1 || by2 < ay1);
};
const HS = 24;
const extentAt = (x, y) => [
    x - HS,
    y - HS,
    x + HS,
    y + HS,
];
const colorDict = {
    "dark-turquoise": "#119a90",
    "grey-turquoise": "#74b4ad",
    pink: "#ffaee6",
    "old-pink": "#e38698",
    yellow: "#ffffc7",
    green: "#7fea9a",
    "grey-blue": "#7e98bb",
    turquoise: "#5df0e6",
    "dark-green": "#a5bbb3",
    "dark-pink": "#b43d5d",
    "light-turquoise": "#cbeae4",
    "light-pink": "#fff9fa",
    violet: "#5d1bb3",
    "hover-green": "#68ff8e",
    white: "white",
};
const colorIterator = () => {
    const colors = Object.values(colorDict);
    let cindex = -1;
    return () => {
        cindex += 1;
        if (cindex > colors.length - 1) {
            cindex = 0;
        }
        return colors[cindex];
    };
};
const pickColor = colorIterator();
const drawTextInExtent = (ctx, extent, text, stop = true) => {
    const { fontBoundingBoxAscent, fontBoundingBoxDescent } = ctx.measureText(text);
    const lineHeight = 1 * (fontBoundingBoxAscent + fontBoundingBoxDescent);
    const words = text.split(" ");
    const spaceMeasure = ctx.measureText(" ");
    let wordIndex = 0;
    const width = extent[2] - extent[0];
    const base = extent[0];
    for (let i = 0; i < 20; i++) {
        const y = extent[1] + (fontBoundingBoxAscent + i * lineHeight);
        let x = 0;
        let startOfLine = true;
        while (wordIndex < words.length) {
            const w = words[wordIndex];
            const wm = ctx.measureText(w);
            if (!startOfLine && x + wm.width > width) {
                break;
            }
            else {
                ctx.fillText(w, base + x, y);
                x += wm.width + spaceMeasure.width;
                wordIndex += 1;
            }
            startOfLine = false;
        }
        if (stop && y + lineHeight > extent[3]) {
            break;
        }
    }
};
const drawNodeTerm = (ctx, extent, { tid }) => {
    map(({ id, name }) => {
        ctx.fillStyle = "white";
        ctx.font = `bold 13pt ${termFont(id)}`;
        drawTextInExtent(ctx, extent, name);
    })(findTerm(tid));
};
const drawNodeImage = (ctx, extent, { tid }) => {
};
const drawNodeTextNote = (ctx, extent, node) => map((a) => {
    ctx.font = `normal 5pt sans-serif`;
    drawTextInExtent(ctx, extent, a.content);
})(findParAnnotation(node.tid));
const PictoCache = {};
const drawImageAnnotation = (ctx, extent) => (annotation) => {
    const render = map((i) => renderImageAnnotationInExtentFull(i, ctx, annotation, extent));
    render(findImage(annotation.imageId));
    return;
    const draw = (picto) => {
        wrapDraw(ctx, () => {
            ctx.drawImage(picto, 0, 0, picto.naturalWidth, picto.naturalHeight, extent[0], extent[1], extentWidth(extent), extentHeight(extent));
            const renderLocation = map((loc) => {
                ctx.fillStyle = "black";
                ctx.strokeStyle = "white";
                ctx.lineWidth = 4;
                ctx.font = "10pt sans-serif";
                const { width, fontBoundingBoxAscent, fontBoundingBoxDescent, } = ctx.measureText(loc.name);
                const x = extent[0] + (extentWidth(extent) - width) / 2;
                const y = extent[3] -
                    (fontBoundingBoxAscent + fontBoundingBoxDescent);
                ctx.strokeText(loc.name, x, y);
                ctx.fillText(loc.name, x, y);
            });
            const withLocation = bind(getImageLocation);
            map2(withLocation, renderLocation)(findImage(annotation.imageId));
        });
    };
    const key = annotation.id.toString();
    if (!(key in PictoCache)) {
        const picto = new Image(extentWidth(extent), extentHeight(extent));
        picto.addEventListener("load", () => {
            PictoCache[key] = picto;
            draw(picto);
        }, false);
        picto.src = `/api/image/annotation/picto/${annotation.id}?color=${encodeURIComponent(pickColor())}`;
    }
    else {
        draw(PictoCache[key]);
    }
};
const drawNodeImageNote = (ctx, extent, node) => map(drawImageAnnotation(ctx, extent))(findImageAnnotation(node.tid));
const drawNodetransitionalText = (ctx, extent, node) => map((a) => {
    ctx.fillStyle = pickColor();
    ctx.fillStyle = "white";
    ctx.font = "bold 11pt sans-serif";
    drawTextInExtent(ctx, extent, a.content, false);
})(findTransitionalText(node.tid));
const drawNode = (ctx, extent, node) => {
    switch (node.type) {
        case "te":
            return drawNodeTerm(ctx, extent, node);
        case "im":
            return drawNodeImage(ctx, extent, node);
        case "ta":
            return drawNodeTextNote(ctx, extent, node);
        case "ia":
            return drawNodeImageNote(ctx, extent, node);
        case "tt":
            return drawNodetransitionalText(ctx, extent, node);
    }
};
const interactionBox = DIV("interaction");
interactionBox.style.position = "absolute";
interactionBox.style.pointerEvents = "none";
const posElem = (e, [minx, miny, maxx, maxy]) => {
    interactionBox.style.top = px(miny);
    interactionBox.style.left = px(minx);
    interactionBox.style.width = px(maxx - minx);
    interactionBox.style.height = px(maxy - miny);
};
export const renderLandingInteraction = (root) => {
    if (interactionBox.parentElement === null) {
        root.append(interactionBox);
    }
    emptyElement(interactionBox);
    const interaction = query("viz1/landing/interaction");
    if (interaction !== null) {
        posElem(interactionBox, interaction.extent);
        const node = interaction.node;
        if (node.type === "te" || node.type === "ia") {
            interactionBox.append(events(attrs(DIV(`landing-link ${node.type}`), (set) => set("style", "pointer-events:auto")), (on) => on("click", () => {
                switch (node.type) {
                    case "te":
                        return navigateTerm(node.tid, false);
                    case "ia":
                        return map((a) => navigateImage(a.imageId, false))(findImageAnnotation(node.tid));
                }
            })));
        }
    }
    else {
        posElem(interactionBox, [0, 0, 0, 0]);
    }
};
export const renderLanding3 = (root) => {
    emptyElement(root);
    root.style.position = "absolute";
    root.style.top = "0";
    root.style.right = "0";
    root.style.bottom = "0";
    root.style.left = "0";
    const paths = getPaths();
    const interestingNodes = (path) => path.nodes;
    const nodeScore = Array.from(paths
        .reduce((acc, path) => {
        interestingNodes(path).forEach((node) => {
            map2(map((val) => acc.set(node.tid, val.concat({ ...node, parent: path.id }))), alt(() => acc.set(node.tid, [{ ...node, parent: path.id }])))(fromNullable(acc.get(node.tid)));
        });
        return acc;
    }, new Map())
        .entries()).sort((a, b) => b[1].length - a[1].length);
    const { width, height } = root.getBoundingClientRect();
    const canvas = CANVAS("landing-inner", width, height);
    const withContext = map((ctx) => {
        ctx.fillStyle = pickColor();
        const scale = 1.5;
        ctx.scale(scale, scale);
        ctx.fillStyle = "#ffc1075e";
        ctx.strokeStyle = "blue";
        ctx.lineWidth = 0.05;
        ctx.font = "6pt sans-serif";
        const drawPoint = (x, y) => {
            ctx.beginPath();
            ctx.arc(x, y, 0.1, 0, deg2rad(360));
            ctx.fill();
        };
        const drawExtent = ([x1, y1, x2, y2]) => {
            ctx.beginPath();
            ctx.rect(x1, y1, x2 - x1, y2 - y1);
            ctx.fill();
            ctx.stroke();
        };
        const frames = [];
        const notIntersects = (e) => frames.every((f) => !intersects(e, f.extent));
        const notIntersectsAt = (e, i) => frames.slice(i).every((f) => !intersects(e, f.extent));
        const nodeExtent = (x, y, node) => {
            const termSize = [66, 24];
            const imageSize = [0, 0];
            const textAnnotSize = [24, 24];
            const imageAnnotSize = [66 * 2, 66 * 2];
            const transiSize = [88, 66];
            switch (node.type) {
                case "te":
                    return [
                        x - termSize[0],
                        y - termSize[1],
                        x + termSize[0],
                        y + termSize[1],
                    ];
                case "im":
                    return [
                        x - imageSize[0],
                        y - imageSize[1],
                        x + imageSize[0],
                        y + imageSize[1],
                    ];
                case "ta":
                    return [
                        x - textAnnotSize[0],
                        y - textAnnotSize[1],
                        x + textAnnotSize[0],
                        y + textAnnotSize[1],
                    ];
                case "ia":
                    return [
                        x - imageAnnotSize[0],
                        y - imageAnnotSize[1],
                        x + imageAnnotSize[0],
                        y + imageAnnotSize[1],
                    ];
                case "tt":
                    return [
                        x - transiSize[0],
                        y - transiSize[1],
                        x + transiSize[0],
                        y + transiSize[1],
                    ];
            }
        };
        const transform = ctx.getTransform();
        const inCanvas = (ix, iy) => {
            const { x, y } = transform.transformPoint({ x: ix, y: iy });
            return x > 0 && x < width && y > 0 && y < height;
        };
        const sw = width / scale;
        const sh = height / scale;
        const lineNumber = Math.ceil(nodeScore.length / 10);
        const linePos = (column, line) => [
            (sw / lineNumber) * column,
            sh - (sh / lineNumber) * line,
        ];
        let currentframestart = 0;
        const nextSlot2 = (tid, column, line, node) => {
            if (frames.length === 0) {
                const [x, y] = linePos(column, line);
                const extent = nodeExtent(x, y, node);
                frames.push({ tid, extent, node });
                return [column, line];
            }
            let curLine = line;
            let curColumn = column;
            for (let i = 0; i < 10000; i++) {
                const [x, y] = linePos(curColumn, curLine);
                const extent = nodeExtent(x, y, node);
                if (notIntersectsAt(extent, currentframestart)) {
                    break;
                }
                if (curColumn + 1 >= lineNumber) {
                    curColumn = 0;
                    curLine += 1;
                    currentframestart = frames.length - 1;
                    break;
                }
                else {
                    curColumn += 1;
                }
            }
            const [x, y] = linePos(curColumn, curLine);
            const extent = nodeExtent(x, y, node);
            frames.push({ tid, extent, node });
            return [curColumn, curLine];
        };
        nodeScore.reduce(([r, t], [tid, nodes]) => nextSlot2(tid, r, t, nodes[0]), [0, 0]);
        const drawBase = () => frames
            .slice()
            .reverse()
            .map(({ extent, node }, idx) => {
            wrapDraw(ctx, () => {
                ctx.fillStyle = "#ff7b00ec";
                drawNode(ctx, extent, node);
            });
        });
        const pathMap = paths.reduce((acc, path) => {
            const extents = interestingNodes(path).map((node) => frames.find((f) => f.tid === node.tid).extent);
            interestingNodes(path).forEach(({ tid }) => {
                const key = tid.toString();
                const val = {
                    pid: path.id,
                    extents,
                };
                if (!(key in acc)) {
                    acc[key] = [val];
                }
                else {
                    acc[key].push(val);
                }
            });
            return acc;
        }, {});
        const clearCanvas = () => {
            wrapDraw(ctx, () => {
                const t = ctx.getTransform();
                ctx.resetTransform();
                ctx.clearRect(0, 0, width, height);
                ctx.setTransform(t);
            });
        };
        const DRAW_CURVE = true;
        const drawTid = (tid) => {
            const pm = pathMap[tid.toString()];
            if (pm) {
                clearCanvas();
                wrapDraw(ctx, () => {
                    ctx.lineWidth = ctx.lineWidth * 60;
                    ctx.lineJoin = "round";
                    ctx.lineCap = "round";
                    pm.forEach(({ pid, extents }) => {
                        const c = pickColor();
                        ctx.strokeStyle = c;
                        if (DRAW_CURVE) {
                            const curve = pointsToCurve(extents.map(extentCenter));
                            drawCurve(ctx, curve);
                        }
                        else {
                            ctx.beginPath();
                            extents.map(extentCenter).map((c, i) => {
                                if (i === 0) {
                                    ctx.moveTo(c[0], c[1]);
                                }
                                else {
                                    ctx.lineTo(c[0], c[1]);
                                }
                            });
                        }
                        ctx.stroke();
                    });
                });
                drawBase();
            }
            else {
                console.error("arrrrg", tid);
            }
        };
        canvas.addEventListener("mousemove", (e) => {
            const t = ctx.getTransform();
            const inv = t.inverse();
            const { x, y } = inv.transformPoint({ x: e.clientX, y: e.clientY });
            const interaction = query("viz1/landing/interaction");
            for (let i = 0; i < frames.length; i++) {
                const frame = frames[i];
                if (intersects(frame.extent, [x, y, x, y])) {
                    if (interaction === null || interaction.frameIndex !== i) {
                        const nodes = nodeScore;
                        assign("viz1/landing/interaction", {
                            frameIndex: i,
                            extent: transformExtent(t, frame.extent),
                            node: frame.node,
                        });
                    }
                    return;
                }
            }
            if (interaction !== null) {
                assign("viz1/landing/interaction", null);
            }
        });
        canvas.addEventListener("click", (e) => {
            const { x, y } = ctx
                .getTransform()
                .inverse()
                .transformPoint({ x: e.clientX, y: e.clientY });
            for (let i = 0; i < frames.length; i++) {
                const frame = frames[i];
                if (intersects(frame.extent, [x, y, x, y])) {
                    drawTid(frame.tid);
                    return;
                }
            }
            clearCanvas();
            drawBase();
        });
        drawBase();
    });
    withContext(fromNullable(canvas.getContext("2d")));
    root.append(canvas);
};
const findIntersectingPaths = (node, paths) => {
    const result = [];
    paths.forEach((path) => {
        const i = path.nodes.findIndex(({ type, tid }) => tid === node.tid && type === node.type);
        if (i >= 0) {
            result.push(tuple(path, i));
        }
    });
    return result;
};
const renderNodeTerm = ({ tid }) => {
    const className = "path-node term";
    const node = DIV(className);
    map(({ name, statement }) => {
        node.append(DIV("_x", SPAN("term-name", name), SPAN("term-description", statement), events(DIV("link", "↬"), (on) => on("click", () => navigateTerm(tid, true)))));
    })(findTerm(tid));
    return node;
};
const renderNodeImage = (node) => DIV("path-node");
const renderNodeTextNote = (node) => DIV("path-node par-annot", map((a) => a.content)(findParAnnotation(node.tid)));
const withImageAnnotation = (annotation) => {
    const width = 200;
    const height = 200;
    const render = map((i) => {
        const canvas = CANVAS("image-annotation-canvas", width, height);
        const ctx = canvas.getContext("2d");
        if (ctx !== null) {
            renderImageAnnotation(i, ctx, annotation, [width, height]);
        }
        return DIV("image-annotation", canvas, events(DIV("link", "↬"), (on) => on("click", () => navigateImage(i.id, true))));
    });
    return orElse(DIV("err", "error when loading image"))(render(findImage(annotation.imageId)));
};
const renderNodeImageNote = (node) => DIV("path-node image-annot", map(withImageAnnotation)(findImageAnnotation(node.tid)));
const renderNodetransitionalText = (node) => DIV("path-node par-text", map((a) => a.content)(findTransitionalText(node.tid)));
const renderNode = (node) => {
    switch (node.type) {
        case "te":
            return renderNodeTerm(node);
        case "im":
            return renderNodeImage(node);
        case "ta":
            return renderNodeTextNote(node);
        case "ia":
            return renderNodeImageNote(node);
        case "tt":
            return renderNodetransitionalText(node);
    }
};
//# sourceMappingURL=landing.js.map