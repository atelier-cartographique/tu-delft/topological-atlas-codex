import { emptyElement, events } from "../lib/dom.js";
import { DIV } from "../lib/html.js";
import { findImage } from "../lib/image.js";
import { bind, fromNullable } from "../lib/option.js";
import { query } from "../lib/state.js";
import { navigateImage, navigateTerm } from "./route.js";
const getImageForTerm = (term) => {
    let annot = query("annotations/image").find((a) => a.tag === "term" && a.termId === term.id);
    return bind(({ imageId }) => findImage(imageId))(fromNullable(annot));
};
const renderEvent = (event) => {
    switch (event.tag) {
        case "image":
            return events(DIV("event event-image"), (add) => add("click", () => navigateImage(event.id, false)));
        case "term":
            return events(DIV("event event-term"), (add) => add("click", () => navigateTerm(event.id, false)));
        case "image-annotation":
            return DIV("event event-image-annotation");
    }
};
export const renderLog = (root) => {
    emptyElement(root);
    query("viz1/user-log").map((event) => root.append(renderEvent(event)));
};
//# sourceMappingURL=log.js.map