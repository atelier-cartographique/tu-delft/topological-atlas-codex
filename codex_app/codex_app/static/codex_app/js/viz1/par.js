import { attrs, emptyElement, events } from "../lib/dom.js";
import { CANVAS, DIV, H1, SPAN } from "../lib/html.js";
import { findImage, renderImageAnnotation, } from "../lib/image.js";
import { bind, fromNullable, map } from "../lib/option.js";
import { query } from "../lib/state.js";
import { findTerm, getViz1Term } from "../lib/term.js";
import { termFont } from "./image.js";
import { navigateImage, navigateTerm } from "./route.js";
let inited = false;
const init = (root) => {
    if (inited) {
        return;
    }
    inited = true;
};
const ACTIVE_CLASS = "active";
const getImageForTerm = (term, cid) => {
    let annot = query("annotations/image").find((a) => a.tag === "term" && a.termId === term.id && cid !== a.imageId);
    return bind(({ imageId }) => findImage(imageId))(fromNullable(annot));
};
const uniq = (eq) => (xs) => {
    const result = [];
    for (const x of xs) {
        if (result.findIndex((r) => eq(r, x)) < 0) {
            result.push(x);
        }
    }
    return result;
};
const sameTermAnnot = uniq((a, b) => a.termId === b.termId);
const renderAnnotContent = (annot) => DIV("annot", DIV("content", annot.content), DIV("proximity", ...sameTermAnnot(query("annotations/paragraph").filter(({ parId, id }) => parId === annot.parId && id !== annot.id)).map(({ termId, content }) => map((term) => attrs(events(DIV("term", term.name), (add) => {
    add("click", () => {
        navigateTerm(term.id, true);
    });
}), (set) => set("title", content)))(findTerm(termId)))));
const renderImages = (root) => (tid) => {
    const imageBox = DIV("image-box");
    query("annotations/image")
        .filter((a) => a.tag === "term" && a.termId === tid)
        .map((annotation) => {
        const width = 400;
        const height = 400;
        const render = map((i) => {
            const canvas = events(CANVAS("image-annotation", width, height), (add) => add("click", () => {
                navigateImage(i.id, true);
            }));
            const ctx = canvas.getContext("2d");
            if (ctx !== null) {
                renderImageAnnotation(i, ctx, annotation, [width, height]);
                imageBox.append(canvas);
            }
        });
        render(findImage(annotation.imageId));
    });
    return imageBox;
};
export const renderPar = (root) => {
    init(root);
    emptyElement(root);
    root.classList.remove(ACTIVE_CLASS);
    const withTerm = (term) => {
        root.classList.add(ACTIVE_CLASS);
        root.append(H1("title", attrs(SPAN("term-link", term.name), (set) => set("style", `font: bold 1.5em ${termFont(term.id)}`))));
        const imgBox = renderImages(root)(term.id);
        const sameTerm = query("annotations/paragraph")
            .filter(({ termId }) => term.id === termId)
            .sort((a, b) => b.content.length - a.content.length);
        const annotBox = DIV("annot-box");
        sameTerm.forEach((annotation) => {
            annotBox.append(renderAnnotContent(annotation));
        });
        const parBox = DIV("par-box");
        parBox.append(imgBox, annotBox);
        root.append(parBox);
    };
    map(withTerm)(getViz1Term());
};
//# sourceMappingURL=par.js.map