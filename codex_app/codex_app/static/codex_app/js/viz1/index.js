import { assign, init, query } from "../lib/state.js";
import { mount, app } from "../lib/app.js";
import renderImage from "./image.js";
import { getAnnotationsForImage, getCurrentImage, setImage, } from "../lib/image.js";
import { bind, map, orElse, pipe2 } from "../lib/option.js";
import { DIV, IMG } from "../lib/html.js";
import { emptyElement, events } from "../lib/dom.js";
import { renderPar } from "./par.js";
import { getImageLocation } from "../lib/place.js";
import { renderPathNav } from "./path.js";
import { renderLog } from "./log.js";
import { initial, landing, navigateImage } from "./route.js";
import { renderLanding3, renderLandingInteraction } from "./landing.js";
const renderTools = (root) => {
    emptyElement(root);
    map((p) => {
        const tools = DIV("tools-inner");
        const annotations = query("annotations/image");
        const current = ({ id }) => pipe2(getCurrentImage(), map((i) => i.id === id), orElse(false));
        const count = ({ id }) => annotations.filter((a) => a.imageId === id).length;
        query("images").forEach((image) => {
            if (p.id === image.location) {
                const n = count(image);
                const isCurrent = current(image);
                const currentClass = isCurrent ? "current" : "";
                const pictoUrl = (base) => isCurrent ? `${base}?color=${encodeURIComponent("#FFAEE6")}` : base;
                const blocks = getAnnotationsForImage(image.id)
                    .map((a, i) => DIV(`elem elem-${i + 1}`, IMG("picto", pictoUrl(`/api/image/annotation/picto/${a.id}`))))
                    .concat(DIV("base"));
                tools.appendChild(events(DIV(`around count-${n} ${currentClass}`, ...blocks), (add) => add("click", () => {
                    navigateImage(image.id, true);
                })));
            }
        });
        root.appendChild(tools);
        root.append(DIV("place", p.name));
    })(bind(getImageLocation)(getCurrentImage()));
};
const renderInfo = (root) => {
    emptyElement(root);
    if (query("viz1/landing/info")) {
        root.classList.add("expand");
        const leftText = `
    On the left, are the link to the paths, edited narratives, or interpretations based on the existing textual or visual annotations.    

    ① This leads back to the map of existing paths.    

    ② When crossing a path, the path's name appears. One can read through the path and then come back to the main navigation.     
    `;
        const middleText = `
    ③  The images, and the fragments of images that were put in relation with a term are displayed. This is a navigation by proximity of meaning, between the visual fragment and the textual fragment.

    ④ The name of the place in which the image was taken is displayed. This allows for a navigation within one place, moving from one image to another. 
    `;
        const rightText = `
    ⑤ The beads represent the terms and images that were visited and allow to go backwards in the navigation.
    `;
        const topText = `
    A tool to track threads of meaning through digital artefacts. It starts with a codex, an evolving vocabulary, that serves to annotate segments. The tool is designed to emphasise how analytical terms turn into places by aggregating fragments of images and texts. From there we experimented
    with forms of mechanical navigation through a possible landscape in which researchers were invited to trace paths, commenting along the way. Those paths thus constitute sensible circulations, bringing together reﬂections on experiences, emotions and directions. When aggregated together through their overlapping meanings they form a mapping device for navigating the archive.
    `;
        const top = DIV("top", topText);
        const left = DIV("info-content left", leftText);
        const middle = DIV("info-content middle", IMG("image-info", "/static/codex_app/illus/info.png"), middleText);
        const right = DIV("info-content right", rightText);
        root.append(DIV("tools", events(DIV("close", "close"), (on) => on("click", () => assign("viz1/landing/info", false)))));
        root.append(top);
        root.append(left);
        root.append(middle);
        root.append(right);
    }
    else {
        root.classList.remove("expand");
        root.append(events(DIV("info-button", "?"), (on) => on("click", () => assign("viz1/landing/info", true))));
    }
};
const disrupt = (timeout) => {
    console.log("Will disrupt in", timeout / 1000);
    setTimeout(() => {
        const is = query("images");
        const i = is[Math.floor(Math.random() * (is.length - 1))];
        setImage(i);
        disrupt(Math.random() * 100000);
    }, timeout);
};
const landingBox = DIV("landing");
const landingBoxInfo = DIV("info");
const onLanding = (render) => {
    return (root) => {
        if (landing()) {
            landingBox.classList.remove("hidden");
            render(root);
        }
        else {
            emptyElement(root);
        }
    };
};
const notOnLanding = (render) => {
    return (root) => {
        if (landing()) {
            emptyElement(root);
        }
        else {
            landingBox.classList.add("hidden");
            render(root);
        }
    };
};
export const main = (state) => {
    init(state);
    document.body.append(landingBox);
    document.body.append(landingBoxInfo);
    const canvasBox = document.getElementById("canvas-box");
    const paragraphs = document.getElementById("paragraphs");
    const tools = document.getElementById("tools");
    const pathBox = document.getElementById("path-box");
    const logBox = document.getElementById("log-box");
    mount(tools, notOnLanding(renderTools), "image");
    mount(canvasBox, notOnLanding(renderImage), "image", "viz1/landing");
    mount(paragraphs, notOnLanding(renderPar), "viz1/term", "viz1/landing");
    mount(pathBox, notOnLanding(renderPathNav), "paths", "image", "viz1/path", "viz1/term", "viz1/landing");
    mount(logBox, notOnLanding(renderLog), "viz1/user-log", "viz1/landing");
    mount(landingBox, onLanding(renderLanding3), "viz1/landing");
    mount(landingBoxInfo, onLanding(renderInfo), "viz1/landing", "viz1/landing/info");
    mount(landingBox, onLanding(renderLandingInteraction), "viz1/landing/interaction", "viz1/landing");
    app().then(initial);
};
//# sourceMappingURL=index.js.map