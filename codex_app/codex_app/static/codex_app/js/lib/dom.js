export const attrs = (e, f) => {
    const set = (k, v) => {
        e.setAttribute(k, v.toString());
    };
    f(set);
    return e;
};
export const events = (e, f) => {
    const add = (k, listener) => {
        e.addEventListener(k, listener);
    };
    f(add);
    return e;
};
const uniq = (xs) => Array.from(new Set(xs));
export function addClass(elem, c) {
    const ecStr = elem.getAttribute("class");
    const ec = ecStr ? ecStr.split(" ") : [];
    ec.push(c);
    elem.setAttribute("class", uniq(ec).join(" "));
}
export function toggleClass(elem, c) {
    const ecStr = elem.getAttribute("class");
    const ec = ecStr ? ecStr.split(" ") : [];
    if (ec.indexOf(c) < 0) {
        addClass(elem, c);
    }
    else {
        removeClass(elem, c);
    }
}
export function hasClass(elem, c) {
    const ecStr = elem.getAttribute("class");
    const ec = ecStr ? ecStr.split(" ") : [];
    return !(ec.indexOf(c) < 0);
}
export function removeClass(elem, c) {
    const ecStr = elem.getAttribute("class");
    const ec = ecStr ? ecStr.split(" ") : [];
    elem.setAttribute("class", ec.filter((cc) => cc !== c).join(" "));
}
export function emptyElement(elem) {
    while (elem.firstChild) {
        removeElement(elem.firstChild);
    }
    return elem;
}
export function removeElement(elem, keepChildren = false) {
    if (!keepChildren) {
        emptyElement(elem);
    }
    const parent = elem.parentNode;
    if (parent) {
        parent.removeChild(elem);
    }
}
//# sourceMappingURL=dom.js.map