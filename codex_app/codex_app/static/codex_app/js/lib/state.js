const nullable = (a) => a;
const state = {
    _version: -1,
    "app/user": nullable(null),
    "app/csrf": nullable(null),
    "app/mode": nullable(null),
    tooltip: nullable(null),
    selection: nullable(null),
    image: [],
    paragraphs: [],
    "annotations/paragraph": [],
    "annotations/image": [],
    terms: [],
    categories: [],
    places: [],
    highlight: nullable(null),
    "image/select-list": [],
    "image/loaded": false,
    paths: [],
    currentTerm: nullable(null),
    qualifiers: [],
    images: [],
    "viz1/landing": false,
    "viz1/term": nullable(null),
    "viz1/path": nullable(null),
    "viz1/user-log": [],
    "viz1/landing/interaction": nullable(null),
    "viz1/landing/info": true,
    "viz0/user": nullable(null),
    "viz0/path": nullable(null),
    "viz0/path-mode": "single",
    transitionals: [],
};
const updateSym = Symbol("sym");
let updatedKeys = [];
export const init = (initState) => Object.keys(initState)
    .map((k) => k)
    .forEach((k) => {
    const value = initState[k];
    if (value !== undefined) {
        assign(k, value, updateSym);
    }
});
export const query = (key) => state[key];
export const assign = (key, value, quiet = nullable(null)) => {
    state[key] = value;
    state._version = state._version + 1;
    console.log(`[${state._version}] assign`, key, value);
    if (quiet === null) {
        updatedKeys.push(key);
    }
};
export const update = (key, u) => assign(key, u(state[key]));
let interval = null;
const framer = (update) => {
    const FRAME_RATE = 16;
    let lastUpdateVersion = -1;
    let lastTimeUpdate = 0;
    let init = false;
    const frame = (animTimestamp) => {
        const stateVersion = state._version;
        if (init === false) {
            init = true;
            update(Object.keys(state));
            lastUpdateVersion = stateVersion;
            lastTimeUpdate = animTimestamp;
        }
        else if (lastUpdateVersion < stateVersion &&
            updatedKeys.length > 0 &&
            animTimestamp - lastTimeUpdate > FRAME_RATE) {
            update(updatedKeys);
            lastUpdateVersion = stateVersion;
            lastTimeUpdate = animTimestamp;
            updatedKeys = [];
        }
        interval = window.requestAnimationFrame(frame);
    };
    return frame;
};
export const start = (update) => {
    if (interval !== null) {
        throw new Error("Framer already started");
    }
    interval = window.requestAnimationFrame(framer(update));
    state._version = 0;
    return Promise.resolve(0);
};
//# sourceMappingURL=state.js.map