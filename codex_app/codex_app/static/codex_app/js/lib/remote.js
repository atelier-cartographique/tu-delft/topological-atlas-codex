import { getCSRF } from "./app.js";
import { map } from "./option.js";
export const defaultFetchOptions = (contentType = "application/json") => {
    const headers = new Headers();
    switch (contentType) {
        case "none":
            break;
        case "application/json":
            headers.append("Content-Type", "application/json");
    }
    map((csrf) => headers.append("X-CSRFToken", csrf))(getCSRF());
    return {
        mode: "cors",
        cache: "default",
        redirect: "follow",
        credentials: "same-origin",
        headers,
    };
};
const absoluteURLRegex = new RegExp("^https?:");
export const checkURLScheme = (url) => {
    if (!absoluteURLRegex.test(url)) {
        return url;
    }
    const lp = window.location.protocol;
    if ("http:" === lp || url.slice(0, lp.length) === lp) {
        return url;
    }
    return `https${url.slice(4)}`;
};
const pseudoValidate = (a) => {
    return true;
};
export const fetchIO = (url, getOptions = {}) => {
    const options = {
        method: "GET",
        ...defaultFetchOptions(),
        ...getOptions,
    };
    return fetch(checkURLScheme(url), options)
        .then((response) => {
        if (response.ok) {
            return response.json();
        }
        throw response;
    })
        .then((obj) => {
        if (pseudoValidate(obj)) {
            return obj;
        }
        throw new Error(`Validation Error: ${url}`);
    });
};
export const postIO = (url, data, postOptions = {}) => {
    const options = {
        body: JSON.stringify(data),
        method: "POST",
        ...defaultFetchOptions(),
        ...postOptions,
    };
    return fetch(checkURLScheme(url), options)
        .then((response) => {
        if (response.ok) {
            return response.json();
        }
        throw response;
    })
        .then((obj) => {
        if (pseudoValidate(obj)) {
            return obj;
        }
        throw new Error(`Validation Error: ${url}`);
    });
};
export const deleteIO = (url) => {
    const options = {
        method: "DELETE",
        ...defaultFetchOptions(),
    };
    return fetch(checkURLScheme(url), options)
        .then((response) => {
        if (response.ok) {
            return response.json();
        }
        throw response;
    })
        .then((obj) => {
        if (pseudoValidate(obj)) {
            return obj;
        }
        throw new Error(`Validation Error: ${url}`);
    });
};
//# sourceMappingURL=remote.js.map