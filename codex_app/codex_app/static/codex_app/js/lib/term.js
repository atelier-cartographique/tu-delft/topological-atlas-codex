import { emptyElement } from "./dom.js";
import { bind, fromNullable, map, orElse, pipe2 } from "./option.js";
import { postIO } from "./remote.js";
import { query } from "./state.js";
export const TermLocationUUID = "40b39814-4783-4db7-bb34-8bd14fbcede7";
export const postParagraphAnnotation = (data) => postIO("/api/paragraph/annotation/", data);
export const findTerm = (id) => fromNullable(query("terms").find((t) => t.id === id));
export const getTermsInCategory = (cid) => query("terms").filter((t) => t.categories.indexOf(cid) >= 0);
export const postImageAnnotation = (data) => postIO("/api/image/annotation/", data);
export const getViz1Term = () => bind(findTerm)(fromNullable(query("viz1/term")));
const sortInterval = (a, b) => a[0] - b[0];
const sortIntervals = (is) => is.sort(sortInterval);
const compileAnnotations = (content, rawIntervals) => {
    const start = 0;
    const end = content.length;
    const intervals = [
        ...new Set(sortIntervals(rawIntervals).reduce((acc, i) => acc.concat(i[0], i[1]), [start, end])),
    ].sort((a, b) => a - b);
    const findInterval = (left, right) => rawIntervals.find((a) => a[0] < right && a[1] > left);
    const blocks = [];
    for (let i = 1; i < intervals.length; i += 1) {
        const int = findInterval(intervals[i - 1], intervals[i]);
        const isCoded = int !== undefined;
        const annotation = isCoded ? int[2] : null;
        blocks.push({
            start: intervals[i - 1],
            end: intervals[i],
            content: content.slice(intervals[i - 1], intervals[i]),
            annotation,
        });
    }
    return blocks;
};
const withIntervals = (elem) => (data) => {
    const intervals = JSON.parse(data);
    const content = pipe2(fromNullable(elem.getAttribute("data-content")), map((c) => JSON.parse(`"${c}"`)), orElse(elem.textContent ?? ""));
    const blocks = compileAnnotations(content, intervals);
    emptyElement(elem);
    blocks.forEach((block) => {
        const span = document.createElement("span");
        const text = document.createTextNode(block.content);
        if (block.annotation === null) {
            span.setAttribute("class", "block");
        }
        else {
            span.setAttribute("class", "block code");
            span.setAttribute("data-annotation", block.annotation.toString());
        }
        span.appendChild(text);
        elem.appendChild(span);
    });
};
export const highlightCode = (elem) => {
    map(withIntervals(elem))(fromNullable(elem.getAttribute("data-intervals")));
};
//# sourceMappingURL=term.js.map