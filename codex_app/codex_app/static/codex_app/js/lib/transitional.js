import { fromNullable } from "./option.js";
import { postIO } from "./remote.js";
import { query, update } from "./state.js";
export const findTransitionalText = (id) => fromNullable(query("transitionals").find((t) => t.id === id));
export const createTransitional = (content) => postIO("/api/transitional/create/", { content }).then((tt) => {
    update("transitionals", (old) => old.filter(({ id }) => tt.id !== id).concat(tt));
    return tt;
});
//# sourceMappingURL=transitional.js.map