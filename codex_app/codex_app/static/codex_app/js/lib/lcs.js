const roughlyEquals = (r) => r === 'identical' || r === 'modified';
export const getDiff = (comp) => (set1, set2) => {
    const lcsMatrix = Array(set2.length + 1)
        .fill(null)
        .map(() => Array(set1.length + 1).fill(null));
    for (let columnIndex = 0; columnIndex <= set1.length; columnIndex += 1) {
        lcsMatrix[0][columnIndex] = 0;
    }
    for (let rowIndex = 0; rowIndex <= set2.length; rowIndex += 1) {
        lcsMatrix[rowIndex][0] = 0;
    }
    for (let rowIndex = 1; rowIndex <= set2.length; rowIndex += 1) {
        for (let columnIndex = 1; columnIndex <= set1.length; columnIndex += 1) {
            if (roughlyEquals(comp(set1[columnIndex - 1], set2[rowIndex - 1]))) {
                lcsMatrix[rowIndex][columnIndex] =
                    lcsMatrix[rowIndex - 1][columnIndex - 1] + 1;
            }
            else {
                lcsMatrix[rowIndex][columnIndex] = Math.max(lcsMatrix[rowIndex - 1][columnIndex], lcsMatrix[rowIndex][columnIndex - 1]);
            }
        }
    }
    if (!lcsMatrix[set2.length][set1.length]) {
        const dels = set1.map((e) => ['del', e]);
        const adds = set2.map((e) => ['add', e]);
        return dels.concat(adds);
    }
    const longestSequence = [];
    const diff = [];
    let columnIndex = set1.length;
    let rowIndex = set2.length;
    while (columnIndex > 0 && rowIndex > 0) {
        const from = set1[columnIndex - 1];
        const to = set2[rowIndex - 1];
        const up = lcsMatrix[rowIndex - 1][columnIndex];
        const left = lcsMatrix[rowIndex][columnIndex - 1];
        const cr = comp(from, to);
        if (cr === 'identical') {
            longestSequence.unshift(to);
            diff.push(['id', to]);
            columnIndex -= 1;
            rowIndex -= 1;
        }
        else if (up > left) {
            diff.push(['add', to]);
            rowIndex -= 1;
        }
        else {
            diff.push(['del', from]);
            columnIndex -= 1;
        }
    }
    console.log(longestSequence);
    return diff.reverse();
};
//# sourceMappingURL=lcs.js.map