const point = (x, y) => ({ x, y });
const getFirstControlPoints = (rhs) => {
    const n = rhs.length;
    const x = new Array(n);
    const tmp = new Array(n);
    let b = 2.0;
    x[0] = rhs[0] / b;
    for (let i = 1; i < n; i += 1) {
        tmp[i] = 1 / b;
        b = (i < n - 1 ? 4.0 : 2.0) - tmp[i];
        x[i] = (rhs[i] - x[i - 1]) / b;
    }
    for (let i = 1; i < n; i += 1) {
        x[n - i - 1] -= tmp[n - i] * x[n - i];
    }
    return x;
};
export const getCurveControlPoints = (knots) => {
    const n = knots.length - 1;
    if (n < 1) {
        return {
            first: [],
            second: [],
        };
    }
    const rhs = new Array(n);
    for (let i = 1; i < n - 1; i += 1) {
        rhs[i] = 4 * knots[i].x + 2 * knots[i + 1].x;
    }
    rhs[0] = knots[0].x + 2 * knots[1].x;
    rhs[n - 1] = 3 * knots[n - 1].x;
    const x = getFirstControlPoints(rhs);
    for (let i = 1; i < n - 1; i += 1) {
        rhs[i] = 4 * knots[i].y + 2 * knots[i + 1].y;
    }
    rhs[0] = knots[0].y + 2 * knots[1].y;
    rhs[n - 1] = 3 * knots[n - 1].y;
    const y = getFirstControlPoints(rhs);
    const first = new Array(n);
    const second = new Array(n);
    for (let i = 0; i < n; i += 1) {
        first[i] = point(x[i], y[i]);
        if (i < n - 1) {
            second[i] = point(2 * knots[i + 1].x - x[i + 1], 2 * knots[i + 1].y - y[i + 1]);
        }
        else {
            second[i] = point((knots[n].x + x[n - 1]) / 2, (knots[n].y + y[n - 1]) / 2);
        }
    }
    return { first, second };
};
export const pointsToCurve = (points) => {
    const { first, second } = getCurveControlPoints(points.map(([x, y]) => ({ x, y })));
    const nullPoint = { x: 0, y: 0 };
    return points.map(([x, y], i) => ({
        x,
        y,
        cp1: i > 0 ? first[i - 1] : nullPoint,
        cp2: i > 0 ? second[i - 1] : nullPoint,
        first: i === 0,
    }));
};
export const drawCurve = (ctx, curve) => {
    ctx.beginPath();
    curve.map(({ x, y, cp1, cp2, first }) => {
        if (first) {
            ctx.moveTo(x, y);
        }
        else {
            ctx.bezierCurveTo(cp1.x, cp1.y, cp2.x, cp2.y, x, y);
        }
    });
};
//# sourceMappingURL=smooth-curve.js.map