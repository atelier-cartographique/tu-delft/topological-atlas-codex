const create = (tag) => document.createElementNS("http://www.w3.org/2000/svg", tag);
export const events = (e, f) => {
    const add = (k, listener) => {
        e.addEventListener(k, listener);
    };
    f(add);
    return e;
};
export const style = (e, f) => {
    f(e.style);
    return e;
};
export const moveTo = (x, y) => ({
    tag: "M",
    x,
    y,
});
export const lineTo = (x, y) => ({
    tag: "L",
    x,
    y,
});
export const close = () => ({
    tag: "Z",
});
const opToString = (op) => {
    switch (op.tag) {
        case "Z":
            return "Z";
        case "M":
            return `M${op.x},${op.y}`;
        case "L":
            return `L${op.x},${op.y}`;
    }
};
const opsToString = (ops) => ops.map(opToString).join(" ");
export const SVG = (els, properties = {}) => {
    const el = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    Object.keys(properties).forEach((key) => el.setAttribute(key, properties[key]));
    els.forEach((e) => el.appendChild(e));
    return el;
};
export const LINE = (x1, y1, x2, y2) => {
    const l = create("line");
    l.setAttribute("x1", x1.toString());
    l.setAttribute("y1", y1.toString());
    l.setAttribute("x2", x2.toString());
    l.setAttribute("y2", y2.toString());
    return l;
};
export const PATH = (ops, properties = {}) => {
    const p = create("path");
    Object.keys(properties).forEach((key) => p.setAttribute(key, properties[key]));
    p.setAttribute("d", opsToString(ops));
    return p;
};
export const GROUP = (elements) => {
    const g = create("g");
    elements.forEach((e) => g.appendChild(e));
    return g;
};
//# sourceMappingURL=svg.js.map