import { bind, fromNullable } from "./option.js";
import { query } from "./state.js";
export const isPlaceWithPoint = (p) => p.point !== null;
export const getImageLocation = (i) => bind((pid) => fromNullable(query("places").find(({ id }) => id === pid)))(fromNullable(i.location));
export const findPlaceByName = (placeName) => {
    const pn = placeName.toLowerCase();
    return fromNullable(query("places").find(({ name, alias }) => name.toLowerCase() === pn ||
        alias.map((a) => a.toLowerCase()).indexOf(pn) >= 0));
};
export const findImageInPlace = (place) => fromNullable(query("images").find((i) => i.location === place.id));
export const findPlace = (pid) => fromNullable(query("places").find(({ id }) => id === pid));
export const placesWithPoint = () => {
    const places = [];
    query("places").forEach((p) => {
        if (isPlaceWithPoint(p)) {
            places.push(p);
        }
    });
    return places;
};
export const placesExtent = () => {
    let minx = Number.MAX_VALUE;
    let miny = Number.MAX_VALUE;
    let maxx = Number.MIN_SAFE_INTEGER;
    let maxy = Number.MIN_SAFE_INTEGER;
    placesWithPoint().forEach(({ point }) => {
        const [x, y] = point.coordinates;
        minx = Math.min(minx, x);
        miny = Math.min(miny, y);
        maxx = Math.max(maxx, x);
        maxy = Math.max(maxy, y);
    });
    return [minx, miny, maxx, maxy];
};
//# sourceMappingURL=place.js.map