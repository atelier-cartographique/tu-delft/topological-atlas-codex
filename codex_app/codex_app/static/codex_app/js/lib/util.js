import { none, some } from "./option.js";
export const tryNumber = (n) => {
    if (typeof n === "number") {
        return some(n);
    }
    if (typeof n === "string") {
        const tn = parseFloat(n);
        if (!Number.isNaN(tn)) {
            return some(tn);
        }
    }
    return none;
};
export const iife = (f) => f();
//# sourceMappingURL=util.js.map