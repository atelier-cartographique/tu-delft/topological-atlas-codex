import { fromNullable, map } from "./option.js";
import { second, tuple } from "./tuple.js";
const defaultRouteParser = (p) => p;
export const Router = (appName) => {
    const isRoute = (r) => r !== null &&
        r !== undefined &&
        typeof r === "object" &&
        "kind" in r &&
        typeof r.kind === "string" &&
        "path" in r &&
        Array.isArray(r.path) &&
        r.path.every((x) => typeof x === "string");
    const handlers = [
        tuple("__null_route__", () => void 0),
    ];
    const cleanPath = (p) => p.reduce((acc, s) => {
        if (s.length > 0) {
            return acc.concat([s]);
        }
        return acc;
    }, []);
    window.onpopstate = ({ state }) => {
        if (isRoute(state)) {
            navigateInternal(state.kind, state.path);
        }
        else {
            navigateInternal("", []);
        }
    };
    const findHandler = (key) => map(second)(fromNullable(handlers.find(([name]) => name === key)));
    const handleRoute = (key, path) => map((handler) => handler(cleanPath(path)))(findHandler(key));
    const pushState = (kind, path) => {
        const state = { kind, path };
        const url = [`/${appName}`, kind].concat(path).join("/");
        window.history.pushState(state, kind, url);
    };
    const route = (r, e, parser = defaultRouteParser) => {
        handlers.push(tuple(r, (p) => e(parser(p))));
    };
    const home = (r, e, parser = defaultRouteParser) => {
        handlers.push(tuple(r, (p) => e(parser(p))));
    };
    const navigateInternal = (t, path) => {
        const stringPath = path.map((a) => a.toString());
        handleRoute(t, path.map((a) => a.toString()));
        return stringPath;
    };
    const navigate = (t, path) => {
        pushState(t, navigateInternal(t, path));
    };
    const back = () => window.history.back();
    return { home, route, navigate, back };
};
//# sourceMappingURL=router.js.map