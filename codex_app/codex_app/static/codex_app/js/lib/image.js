import { index, last } from "./array.js";
import { emptyElement } from "./dom.js";
import { CANVAS, DIV, IMG } from "./html.js";
import { fromNullable, map, none, orElseL, pipe2, some, } from "./option.js";
import { assign, query, update } from "./state.js";
import { events, close, GROUP, lineTo, moveTo, PATH, SVG, style, } from "./svg.js";
import { findTerm } from "./term.js";
export const findImage = (imageId) => fromNullable(query("images").find((i) => i.id === imageId));
export const getCurrentImage = () => last(query("image"));
export const getPreviousImage = () => {
    const is = query("image");
    return index(is.length - 2, is);
};
export const setImage = (i) => {
    assign("viz1/term", null);
    update("image", (is) => is.concat(i));
};
export const clearImage = () => assign("image", []);
export const getImageList = () => query("images");
export const selectNextImage = () => {
    pipe2(getCurrentImage(), map((image) => {
        const images = getImageList();
        if (images.length > 0) {
            const index = images.findIndex((i) => i.id === image.id);
            if (index < 0 || index >= images.length - 1) {
                setImage(images[0]);
            }
            else {
                setImage(images[index + 1]);
            }
        }
    }), orElseL(() => {
        const images = getImageList();
        if (images.length > 0) {
            setImage(images[0]);
        }
    }));
};
export const selectPreviousImage = () => {
    pipe2(getCurrentImage(), map((image) => {
        const images = getImageList();
        if (images.length > 0) {
            const index = images.findIndex((i) => i.id === image.id);
            if (index < 0 || index === 0) {
                setImage(images[images.length - 1]);
            }
            else {
                setImage(images[index - 1]);
            }
        }
    }), orElseL(() => {
        const images = getImageList();
        if (images.length > 0) {
            setImage(images[images.length - 1]);
        }
    }));
};
export const getAllImageAnnotations = () => query("annotations/image");
export const findImageAnnotation = (aid) => fromNullable(getAllImageAnnotations().find(({ id }) => aid === id));
export const getAnnotationsForImage = (id) => getAllImageAnnotations()
    .filter(({ imageId }) => imageId === id)
    .sort((a, b) => a.triangles.length - b.triangles.length);
export const selectNextImageWithAnnotations = () => { };
const resetSelection = () => assign("image/select-list", []);
const addSelection = (n) => update("image/select-list", (s) => s.filter((x) => x !== n).concat(n));
const removeSelection = (n) => update("image/select-list", (s) => s.filter((x) => x !== n));
const getSelection = () => query("image/select-list");
const isSelected = (n) => getSelection().indexOf(n) >= 0;
const isLoaded = () => query("image/loaded");
const setLoaded = () => assign("image/loaded", true);
export const getImageSelection = getSelection;
export const GRID_SIZE = 6 * 4;
export const TRIANGLE_SIDE = 100;
const SOURCE_IMAGE_SIZE = 800;
const THE_SOURCE_IMAGE_ID = "the-source-image";
export const GRID_WIDTH = (GRID_SIZE * TRIANGLE_SIDE) / 2;
export const GRID_HEIGHT = GRID_SIZE * TRIANGLE_SIDE;
export const getGridScale = (width, height) => {
    const xScale = width / GRID_WIDTH;
    const yScale = height / GRID_HEIGHT;
    return { xScale, yScale };
};
const selectTriangle = (n, inverter) => map((el) => {
    if (inverter && isSelected(n)) {
        el.style.stroke = "burlywood";
        el.style.strokeWidth = "0.5";
        el.style.fill = "#ffffff60";
        removeSelection(n);
    }
    else {
        el.style.stroke = "none";
        el.style.strokeWidth = "0";
        el.style.fill = "none";
        addSelection(n);
    }
});
const triangleEvents = (n) => (add) => {
    const selectOnClick = selectTriangle(n, true);
    const selectOnEnter = selectTriangle(n, false);
    add("click", (e) => selectOnClick(fromNullable(e.currentTarget)));
    add("mouseenter", (e) => {
        if (e.buttons === 1) {
            selectOnEnter(fromNullable(e.currentTarget));
        }
    });
};
const triangleStyle = (n) => (style) => {
    if (isSelected(n)) {
        style.stroke = "none";
        style.strokeWidth = "0";
        style.fill = "none";
    }
    else {
        style.stroke = "burlywood";
        style.strokeWidth = "0.5";
        style.fill = "#ffffff60";
    }
};
const ops0 = (x, y) => [
    moveTo(x, y),
    lineTo(x + TRIANGLE_SIDE, y),
    lineTo(x, y + TRIANGLE_SIDE),
    close(),
];
const ops1 = (x, y) => [
    moveTo(x + TRIANGLE_SIDE, y),
    lineTo(x + TRIANGLE_SIDE, y + TRIANGLE_SIDE),
    lineTo(x, y + TRIANGLE_SIDE),
    close(),
];
const ops2 = (x, y) => [
    moveTo(x, y),
    lineTo(x + TRIANGLE_SIDE, y + TRIANGLE_SIDE),
    lineTo(x, y + TRIANGLE_SIDE),
    close(),
];
const ops3 = (x, y) => [
    moveTo(x, y),
    lineTo(x + TRIANGLE_SIDE, y),
    lineTo(x + TRIANGLE_SIDE, y + TRIANGLE_SIDE),
    close(),
];
const draw0 = (n, acc, x, y) => ({
    tag: 1,
    x,
    y,
    acc: acc.concat(style(events(PATH(ops0(x, y)), triangleEvents(n)), triangleStyle(n))),
    fn: draw1,
});
const draw1 = (n, acc, x, y) => ({
    tag: 2,
    x: x + TRIANGLE_SIDE,
    y,
    acc: acc.concat(style(events(PATH(ops1(x, y)), triangleEvents(n)), triangleStyle(n))),
    fn: draw2,
});
const draw2 = (n, acc, x, y) => ({
    tag: 3,
    x,
    y,
    acc: acc.concat(style(events(PATH(ops2(x, y)), triangleEvents(n)), triangleStyle(n))),
    fn: draw3,
});
const draw3 = (n, acc, x, y) => ({
    tag: 0,
    x: x + TRIANGLE_SIDE,
    y,
    acc: acc.concat(style(events(PATH(ops3(x, y)), triangleEvents(n)), triangleStyle(n))),
    fn: draw0,
});
const svgRow = (y) => ({
    tag: 0,
    x: 0,
    y,
    acc: [],
    fn: draw0,
});
const drawRow = (y, startIndex) => {
    let r = svgRow(y);
    for (let i = 0; i < GRID_SIZE; i += 1) {
        r = r.fn(startIndex + i, r.acc, r.x, r.y);
    }
    r.acc.forEach((e) => e.setAttribute("pointer-events", "all"));
    return r.acc;
};
const theGrid = () => {
    const triangles = [];
    for (let i = 0; i < GRID_SIZE; i += 1) {
        triangles.push(GROUP(drawRow(i * TRIANGLE_SIDE, i * GRID_SIZE)));
    }
    return triangles;
};
const select0 = (n, acc, x, y) => ({
    tag: 1,
    x,
    y,
    acc: acc.concat({ n, ops: ops0(x, y) }),
    fn: select1,
});
const select1 = (n, acc, x, y) => ({
    tag: 2,
    x: x + TRIANGLE_SIDE,
    y,
    acc: acc.concat({ n, ops: ops1(x, y) }),
    fn: select2,
});
const select2 = (n, acc, x, y) => ({
    tag: 3,
    x,
    y,
    acc: acc.concat({ n, ops: ops2(x, y) }),
    fn: select3,
});
const select3 = (n, acc, x, y) => ({
    tag: 0,
    x: x + TRIANGLE_SIDE,
    y,
    acc: acc.concat({ n, ops: ops3(x, y) }),
    fn: select0,
});
export const selectedOpsRow = (y) => ({
    tag: 0,
    x: 0,
    y,
    acc: [],
    fn: select0,
});
const noopContext = {
    beginPath: () => void 0,
    moveTo: () => void 0,
    lineTo: () => void 0,
    closePath: () => void 0,
    clip: () => void 0,
    stroke: () => void 0,
};
export const setClipping = (ctx, annotations, xScale, yScale, stroke = false) => {
    ctx.beginPath();
    const clippingRow = (y, startIndex) => {
        let r = selectedOpsRow(y);
        for (let i = 0; i < GRID_SIZE; i += 1) {
            r = r.fn(startIndex + i, r.acc, r.x, r.y);
        }
        return r.acc;
    };
    let minX = Number.MAX_VALUE;
    let minY = Number.MAX_VALUE;
    let maxX = Number.MIN_VALUE;
    let maxY = Number.MIN_VALUE;
    const updateExtent = (x, y) => {
        minX = Math.min(minX, x);
        minY = Math.min(minY, y);
        maxX = Math.max(maxX, x);
        maxY = Math.max(maxY, y);
    };
    annotations.map((annot) => {
        for (let i = 0; i < GRID_SIZE; i += 1) {
            clippingRow(i * TRIANGLE_SIDE, i * GRID_SIZE).forEach(({ n, ops }) => {
                if (annot.triangles.indexOf(n) >= 0) {
                    for (const op of ops) {
                        switch (op.tag) {
                            case "M":
                                ctx.moveTo(xScale * op.x, yScale * op.y);
                                updateExtent(xScale * op.x, yScale * op.y);
                                break;
                            case "L":
                                ctx.lineTo(xScale * op.x, yScale * op.y);
                                updateExtent(xScale * op.x, yScale * op.y);
                                break;
                            case "Z":
                                ctx.closePath();
                                break;
                        }
                    }
                }
            });
        }
    });
    if (stroke) {
        ctx.stroke();
    }
    else {
        ctx.clip();
    }
    return [minX, minY, maxX, maxY];
};
const extractor = (img) => (annot) => {
    const width = img.naturalWidth;
    const height = img.naturalHeight;
    if (width > 0 && height > 0) {
        const gridWidth = (GRID_SIZE * TRIANGLE_SIDE) / 2;
        const gridHeight = GRID_SIZE * TRIANGLE_SIDE;
        const xScale = width / gridWidth;
        const yScale = height / gridHeight;
        let canvas = CANVAS("extract", width, height);
        map((ctx) => {
            const [minX, minY, maxX, maxY] = setClipping(ctx, [annot], xScale, yScale);
            ctx.drawImage(img, 0, 0, width, height);
            const newWidth = maxX - minX;
            const newHeight = maxY - minY;
            const data = ctx.getImageData(minX, minY, newWidth, newHeight);
            canvas.width = newWidth;
            canvas.height = newHeight;
            ctx.clearRect(0, 0, newWidth, newHeight);
            ctx.putImageData(data, 0, 0);
        })(fromNullable(canvas.getContext("2d")));
        return some(canvas);
    }
    return none;
};
const withName = map((cat) => cat.name);
const px = (n) => `${n}px`;
const annotWithImage = (img, root) => {
    const extract = extractor(img);
    const annot = DIV("annot");
    getAllImageAnnotations().forEach((a) => {
        const item = DIV("annot-item");
        const c = extract(a);
        item.appendChild(DIV(a.tag, map((t) => t.name)(findTerm(a.termId))));
        map((c) => item.appendChild(c))(c);
        annot.appendChild(item);
    });
    root.appendChild(annot);
};
const computeSize = (image) => {
    const nw = image.file.width;
    const nh = image.file.height;
    const s = nw > nh ? SOURCE_IMAGE_SIZE / nw : SOURCE_IMAGE_SIZE / nh;
    const width = nw * s;
    const height = nh * s;
    return { width, height };
};
const withImage = (root) => map((image) => {
    emptyElement(root);
    const img = IMG("", image.file.url);
    img.id = THE_SOURCE_IMAGE_ID;
    const { width, height } = computeSize(image);
    img.width = width;
    img.height = height;
    const wrapper = DIV("image-wrapper", img);
    const grid = events(SVG(theGrid()), (add) => { });
    grid.setAttribute("preserveAspectRatio", "none");
    wrapper.appendChild(grid);
    root.appendChild(wrapper);
    grid.setAttribute("width", px(img.width));
    grid.setAttribute("height", px(img.height));
    grid.setAttribute("viewBox", `0 0 ${(GRID_SIZE * TRIANGLE_SIDE) / 2} ${GRID_SIZE * TRIANGLE_SIDE}`);
});
const withoutImage = (root) => orElseL(() => {
    emptyElement(root);
    root.appendChild(DIV("error", "sorry, no image in sight"));
});
export const renderImage = (root) => pipe2(getCurrentImage(), withImage(root), withoutImage(root));
const annotWithImg = (root) => map((img) => {
    emptyElement(root);
    if (isLoaded() && false) {
        annotWithImage(img, root);
    }
    else {
        img.addEventListener("load", () => {
            annotWithImage(img, root);
        });
    }
});
const annotWithoutImg = (root) => orElseL(() => {
    emptyElement(root);
    root.appendChild(DIV("-no"));
});
export const renderAnnotations = (root) => pipe2(fromNullable(document.getElementById(THE_SOURCE_IMAGE_ID)), annotWithImg(root), annotWithoutImg(root));
export const getCenteringMatrix = (width, height, imgWidth, imgHeight, from = new DOMMatrix()) => {
    const scale = imgWidth > imgHeight ? width / imgWidth : height / imgHeight;
    const { tx, ty } = (() => {
        if (imgWidth > imgHeight) {
            return {
                tx: 0,
                ty: (height - imgHeight * scale) / 2,
            };
        }
        return {
            tx: (width - imgWidth * scale) / 2,
            ty: 0,
        };
    })();
    const base = new DOMMatrix(from.toString());
    const translated = base.translate(tx, ty);
    const scaled = translated.scale(scale, scale);
    return scaled;
};
export const renderImageAnnotation = (i, ctx, annotation, [width, height]) => {
    const img = new Image(i.file.width, i.file.height);
    img.addEventListener("load", () => {
        ctx.save();
        ctx.clearRect(0, 0, width, height);
        const cm = getCenteringMatrix(width, height, i.file.width, i.file.height);
        ctx.setTransform(cm);
        const { xScale, yScale } = getGridScale(i.file.width, i.file.height);
        ctx.save();
        setClipping(ctx, [annotation], xScale, yScale);
        ctx.drawImage(img, 0, 0);
        ctx.restore();
        ctx.restore();
    }, false);
    img.src = i.file.url;
};
const ImageCache = {};
export const renderImageAnnotationInExtent = (i, ctx, annotation, extent) => {
    const draw = (img) => {
        const x = extent[0];
        const y = extent[1];
        const width = extent[2] - x;
        const height = extent[3] - y;
        ctx.save();
        ctx.translate(x, y);
        const cm = getCenteringMatrix(width, height, i.file.width, i.file.height, ctx.getTransform());
        ctx.setTransform(cm);
        const { xScale, yScale } = getGridScale(i.file.width, i.file.height);
        ctx.save();
        setClipping(ctx, [annotation], xScale, yScale);
        ctx.drawImage(img, 0, 0);
        ctx.restore();
        ctx.restore();
    };
    const key = i.id.toString();
    if (!(key in ImageCache)) {
        const img = new Image(i.file.width, i.file.height);
        img.addEventListener("load", () => {
            ImageCache[key] = img;
            draw(img);
        }, false);
        img.src = i.file.url;
    }
    else {
        draw(ImageCache[key]);
    }
};
export const renderImageAnnotationInExtentFull = (i, ctx, annotation, extent) => {
    const draw = (img) => {
        const x = extent[0];
        const y = extent[1];
        const width = extent[2] - x;
        const height = extent[3] - y;
        const { xScale, yScale } = getGridScale(i.thumbnail.width, i.thumbnail.height);
        const [minx, miny, maxx, maxy] = setClipping(noopContext, [annotation], xScale, yScale);
        const clippedWidth = maxx - minx;
        const clippedHeight = maxy - miny;
        const offsetX = i.thumbnail.width - clippedWidth;
        const offsetY = i.thumbnail.height - clippedHeight;
        ctx.save();
        ctx.translate(x, y);
        const cm = (() => {
            const scale = clippedWidth > clippedHeight
                ? width / clippedWidth
                : height / clippedHeight;
            const { tx, ty } = (() => {
                if (clippedWidth > clippedHeight) {
                    return {
                        tx: -minx * scale,
                        ty: ((height - clippedHeight + offsetY) * scale) / 2,
                    };
                }
                return {
                    tx: ((width - clippedWidth + offsetX) * scale) / 2,
                    ty: -miny * scale,
                };
            })();
            const base = new DOMMatrix(ctx.getTransform().toString());
            const translated = base.translate(tx, ty);
            const scaled = translated.scale(scale, scale);
            return scaled;
        })();
        ctx.setTransform(cm);
        ctx.save();
        setClipping(ctx, [annotation], xScale, yScale);
        ctx.drawImage(img, 0, 0);
        ctx.restore();
        ctx.restore();
    };
    const key = i.id.toString();
    if (!(key in ImageCache)) {
        const img = new Image(i.thumbnail.width, i.thumbnail.height);
        img.addEventListener("load", () => {
            ImageCache[key] = img;
            draw(img);
        }, false);
        img.src = i.thumbnail.url;
    }
    else {
        draw(ImageCache[key]);
    }
};
//# sourceMappingURL=image.js.map