import { getImageSelection } from "./image.js";
import { fromNullable, map, orElse, pipe2 } from "./option.js";
import { query, start } from "./state.js";
const { pushMount, foreachMount } = (() => {
    const mountedElements = [];
    const pushMount = (e) => mountedElements.push(e);
    const foreachMount = (f) => mountedElements.forEach(f);
    return { pushMount, foreachMount };
})();
export const getCSRF = () => fromNullable(query("app/csrf"));
export const getMode = () => fromNullable(query("app/mode"));
export const mount = (element, update, ...keys) => {
    if (element) {
        pushMount({
            element,
            update,
            keys,
        });
    }
};
export const mountOnce = (element, update) => {
    if (element) {
        update(element);
    }
};
const matchKey = (keys, updated) => {
    for (const key of keys) {
        if (updated.indexOf(key) >= 0) {
            return true;
        }
    }
    return false;
};
const emptyElement = (elem) => {
    while (elem.firstChild) {
        removeElement(elem.firstChild);
    }
    return elem;
};
const removeElement = (elem) => {
    emptyElement(elem);
    const parent = elem.parentNode;
    if (parent) {
        parent.removeChild(elem);
    }
};
const update = (updatedKeys) => {
    foreachMount(({ element, update, keys }) => {
        if (matchKey(keys, updatedKeys)) {
            update(element);
        }
    });
};
export const hasSelection = () => pipe2(getMode(), map((m) => {
    switch (m) {
        case "image":
            return getImageSelection().length > 0;
        case "text":
            return query("selection") !== null;
        default:
            return false;
    }
}), orElse(false));
export const app = () => start(update);
//# sourceMappingURL=app.js.map