import { tuple } from "./tuple.js";
const _none = "none";
const _some = "some";
export const none = { tag: _none };
export const some = (value) => ({ tag: _some, value });
export const isOption = (n) => typeof n === "object" &&
    n !== null &&
    "tag" in n &&
    (n["tag"] === _none || n["tag"] === _some);
export const isNone = (o) => o.tag === "none";
export const isSome = (o) => o.tag === "some";
export const map = (f) => (o) => isNone(o) ? none : some(f(o.value));
export const bind = (f) => (o) => isNone(o) ? none : f(o.value);
export const alt = (f) => (o) => isSome(o) ? none : some(f());
export const orElse = (value) => (o) => isNone(o) ? value : o.value;
export const orElseL = (value) => (o) => isNone(o) ? value() : o.value;
export const fromNullable = (value) => value === null || value === undefined ? none : some(value);
export const toNullable = (opt) => isSome(opt) ? opt.value : null;
export const map2 = (fa, fb) => (a) => {
    if (isOption(a)) {
        return fb(fa(a));
    }
    return fb(fa(some(a)));
};
export const pipe2 = (a, fa, fb) => fb(fa(a));
export const merge = (a, b) => bind((sa) => map((sb) => tuple(sa, sb))(b))(a);
//# sourceMappingURL=option.js.map