export const tuple = (a, b) => [a, b];
export const first = (t) => t[0];
export const second = (t) => t[1];
export const fst = first;
export const snd = second;
export const mapFirst = (f) => (t) => f(t[0]);
export const mapSecond = (f) => (t) => f(t[1]);
//# sourceMappingURL=tuple.js.map