import { attrs } from "./dom.js";
import { isOption, map } from "./option.js";
const createBase = (tag) => document.createElement(tag);
export const appendText = (text) => (node) => {
    return node.appendChild(document.createTextNode(text));
};
const createWithClass = (tag, className) => {
    const node = createBase(tag);
    node.setAttribute("class", className);
    return node;
};
const appendLiteral = (node) => (value) => {
    if (typeof value === "number") {
        appendText(value.toLocaleString())(node);
    }
    else {
        appendText(value)(node);
    }
};
const appendElement = (node) => (value) => {
    node.appendChild(value);
};
const appendBaseNode = (node, value) => {
    if (typeof value === "number" || typeof value === "string") {
        appendLiteral(node)(value);
    }
    else {
        appendElement(node)(value);
    }
};
const appendNode = (node) => (value) => {
    if (isOption(value)) {
        map((inner) => appendBaseNode(node, inner))(value);
    }
    else {
        appendBaseNode(node, value);
    }
};
const createWithChildren = (tag, className, ns) => {
    const node = createWithClass(tag, className);
    ns.forEach(appendNode(node));
    return node;
};
export const DIV = (className, ...ns) => createWithChildren("div", className, ns);
export const SPAN = (className, ...ns) => createWithChildren("span", className, ns);
export const LABEL = (className, ...ns) => createWithChildren("label", className, ns);
export const FIELDSET = (className, ...ns) => createWithChildren("fieldset", className, ns);
export const H1 = (className, ...ns) => createWithChildren("h1", className, ns);
export const H2 = (className, ...ns) => createWithChildren("h2", className, ns);
export const H3 = (className, ...ns) => createWithChildren("h3", className, ns);
export const BUTTON = (className, ...ns) => createWithChildren("button", className, ns);
export const SUPERSCRIPT = (className, ...ns) => createWithChildren("sup", className, ns);
export const TEXTAREA = (className, ...ns) => createWithChildren("textarea", className, ns);
export const INPUT = (className, inputType) => attrs(createWithClass("input", className), (set) => set("type", inputType));
export const ANCHOR = (className, href, ...ns) => attrs(createWithChildren("a", className, ns), (set) => set("href", href));
export const IMG = (className, src) => {
    const el = createWithClass("img", className);
    el.setAttribute("src", src);
    return el;
};
export const CANVAS = (className, width, height) => attrs(createWithClass("canvas", className), (set) => {
    set("width", width);
    set("height", height);
});
//# sourceMappingURL=html.js.map