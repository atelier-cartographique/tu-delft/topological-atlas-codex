import { update } from "./state.js";
export const eventTerm = (id) => ({ tag: "term", id });
export const eventImage = (id) => ({ tag: "image", id });
export const eventImageAnnotation = (id) => ({
    tag: "image-annotation",
    id,
});
export const logEvent = (event) => update("viz1/user-log", (log) => log.concat(event));
//# sourceMappingURL=log-event.js.map