import { none, some } from "./option.js";
export const index = (n, xs) => n >= 0 && n < xs.length ? some(xs[n]) : none;
export const indexC = (n) => (xs) => index(n, xs);
export const first = (xs) => index(0, xs);
export const last = (xs) => index(xs.length - 1, xs);
//# sourceMappingURL=array.js.map