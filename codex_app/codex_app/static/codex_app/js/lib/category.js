import { fromNullable } from "./option.js";
import { query } from "./state.js";
export const findCategory = (catId) => fromNullable(query("categories").find(({ id }) => id === catId));
//# sourceMappingURL=category.js.map