import { none, some } from "./option.js";

export const index = <T>(n: number, xs: T[]) =>
    n >= 0 && n < xs.length ? some(xs[n]) : none;

export const indexC =
    <T>(n: number) =>
    (xs: T[]) =>
        index(n, xs);

export const first = <T>(xs: T[]) => index(0, xs);

export const last = <T>(xs: T[]) => index(xs.length - 1, xs);
