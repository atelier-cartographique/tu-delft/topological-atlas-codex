/**
 * This is a port of
 * https://github.com/sb-js/typescript-algorithms/tree/master/src/algorithms/sets/longest-common-subsequence
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Oleksii Trekhleb
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

export type CompResult = 'identical' | 'different' | 'modified';

export type Comp<T> = (a: T, b: T) => CompResult;

const roughlyEquals = (r: CompResult) => r === 'identical' || r === 'modified';

export type OpName = 'id' | 'add' | 'del' | 'mod';
type Identity<T> = ['id', T];
type Add<T> = ['add', T];
type Del<T> = ['del', T];
type Mod<T> = ['mod', T, T];

export type Op<T> = Identity<T> | Add<T> | Del<T> | Mod<T>;

export type Diff<T> = Op<T>[];

/**
 *
 * @param set1
 * @param set2
 */
export const getDiff =
    <T>(comp: Comp<T>) =>
    (set1: T[], set2: T[]): Diff<T> => {
        // Init LCS matrix.
        const lcsMatrix = Array(set2.length + 1)
            .fill(null)
            .map(() => Array(set1.length + 1).fill(null));

        // Fill first row with zeros.
        for (
            let columnIndex = 0;
            columnIndex <= set1.length;
            columnIndex += 1
        ) {
            lcsMatrix[0][columnIndex] = 0;
        }

        // Fill first column with zeros.
        for (let rowIndex = 0; rowIndex <= set2.length; rowIndex += 1) {
            lcsMatrix[rowIndex][0] = 0;
        }

        // Fill rest of the column that correspond to each of two strings.
        for (let rowIndex = 1; rowIndex <= set2.length; rowIndex += 1) {
            for (
                let columnIndex = 1;
                columnIndex <= set1.length;
                columnIndex += 1
            ) {
                if (
                    roughlyEquals(
                        comp(set1[columnIndex - 1], set2[rowIndex - 1])
                    )
                ) {
                    lcsMatrix[rowIndex][columnIndex] =
                        lcsMatrix[rowIndex - 1][columnIndex - 1] + 1;
                } else {
                    lcsMatrix[rowIndex][columnIndex] = Math.max(
                        lcsMatrix[rowIndex - 1][columnIndex],
                        lcsMatrix[rowIndex][columnIndex - 1]
                    );
                }
            }
        }

        // Calculate LCS based on LCS matrix.
        if (!lcsMatrix[set2.length][set1.length]) {
            const dels = set1.map<Op<T>>((e) => ['del', e]);
            const adds = set2.map<Op<T>>((e) => ['add', e]);
            return dels.concat(adds);
        }

        const longestSequence: T[] = [];
        const diff: Diff<T> = [];
        let columnIndex = set1.length;
        let rowIndex = set2.length;

        while (columnIndex > 0 && rowIndex > 0) {
            const from = set1[columnIndex - 1];
            const to = set2[rowIndex - 1];
            const up = lcsMatrix[rowIndex - 1][columnIndex];
            const left = lcsMatrix[rowIndex][columnIndex - 1];
            const cr = comp(from, to);
            if (cr === 'identical') {
                // Move by diagonal left-top.
                longestSequence.unshift(to);
                diff.push(['id', to]);
                columnIndex -= 1;
                rowIndex -= 1;
            }
            // else if (cr === 'modified') {
            //     longestSequence.unshift(to);
            //     diff.push(['mod', from, to]);
            //     columnIndex -= 1;
            //     rowIndex -= 1;
            // }
            else if (up > left) {
                // Move up.
                diff.push(['add', to]);
                rowIndex -= 1;
            } else {
                // Move left.
                diff.push(['del', from]);
                columnIndex -= 1;
            }
            //   else if (up < left) {
            //     // Move left.
            //     diff.push(['del', from]);
            //     columnIndex -= 1;
            // } else {
            //     // Move up.
            //     diff.push(['add', to]);
            //     rowIndex -= 1;
            // }
        }
        console.log(longestSequence);
        return diff.reverse();
    };

// export const diffByWord = (str1: string, str2: string) => {
//     // (str1: string, str2: string) => {
//     // const str1 = 'a b c d f g h j q z';
//     // const str2 = 'a b c d e f g i j k r x y z';
//     const strTable1 = str1.split(' ');
//     const strTable2 = str2.split(' ');
//     const diff = getDiff<string>({ equals: (a, b) => a === b })(
//         strTable1,
//         strTable2
//     );
//     // diff.forEach(d => console.log(`------------------- ${d[0]}: ${d[1]}`));
//     return diff;
// };
