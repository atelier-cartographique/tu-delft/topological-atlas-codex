import { getCSRF } from "./app.js";
import { map } from "./option.js";

export type ContentType = "none" | "application/json";

export const defaultFetchOptions = (
    contentType = "application/json" as ContentType
): RequestInit => {
    const headers = new Headers();
    switch (contentType) {
        case "none":
            break;
        case "application/json":
            headers.append("Content-Type", "application/json");
    }
    map((csrf: string) => headers.append("X-CSRFToken", csrf))(getCSRF());

    return {
        mode: "cors",
        cache: "default",
        redirect: "follow",
        credentials: "same-origin",
        headers,
    };
};

const absoluteURLRegex = new RegExp("^https?:");
/**
 * checkScheme ensures that URL has the same scheme than the location
 * fixes Mixed Content when getting URLs from a proxied server
 * @param url
 */
export const checkURLScheme = (url: string): string => {
    if (!absoluteURLRegex.test(url)) {
        return url; // browser will take care
    }
    const lp = window.location.protocol;
    if ("http:" === lp || url.slice(0, lp.length) === lp) {
        return url; // we're on the same page
    }

    // we're asking for http while we're on https
    return `https${url.slice(4)}`;
};

const pseudoValidate = <T>(a: unknown): a is T => {
    return true;
};

export const fetchIO = <T>(url: string, getOptions: RequestInit = {}) => {
    const options: RequestInit = {
        method: "GET",
        ...defaultFetchOptions(),
        ...getOptions,
    };

    return fetch(checkURLScheme(url), options)
        .then((response) => {
            if (response.ok) {
                return response.json();
            }
            throw response;
        })
        .then((obj) => {
            if (pseudoValidate<T>(obj)) {
                return obj;
            }
            throw new Error(`Validation Error: ${url}`);
        });
};

export const postIO = <T, DT>(
    url: string,
    data: DT,
    postOptions: RequestInit = {}
) => {
    const options: RequestInit = {
        body: JSON.stringify(data),
        method: "POST",
        ...defaultFetchOptions(),
        ...postOptions,
    };

    return fetch(checkURLScheme(url), options)
        .then((response) => {
            if (response.ok) {
                return response.json();
            }
            throw response;
        })
        .then((obj) => {
            if (pseudoValidate<T>(obj)) {
                return obj;
            }
            throw new Error(`Validation Error: ${url}`);
        });
};

export const deleteIO = <T>(
    url: string,
) => {
    const options: RequestInit = {
        method: "DELETE",
        ...defaultFetchOptions(),
    };

    return fetch(checkURLScheme(url), options)
        .then((response) => {
            if (response.ok) {
                return response.json();
            }
            throw response;
        })
        .then((obj) => {
            if (pseudoValidate<T>(obj)) {
                return obj;
            }
            throw new Error(`Validation Error: ${url}`);
        });
};
