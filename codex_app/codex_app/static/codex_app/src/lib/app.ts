import { getImageSelection } from "./image.js";
import { fromNullable, map, orElse, pipe2 } from "./option.js";
import { query, start, StateKey } from "./state.js";

type Mounted = {
    element: HTMLElement;
    update: (root: HTMLElement) => void;
    keys: StateKey[];
};

const { pushMount, foreachMount } = (() => {
    const mountedElements: Mounted[] = [];
    const pushMount = (e: Mounted) => mountedElements.push(e);
    const foreachMount = (f: (e: Mounted) => void) =>
        mountedElements.forEach(f);
    return { pushMount, foreachMount };
})();

export const getCSRF = () => fromNullable(query("app/csrf"));

export const getMode = () => fromNullable(query("app/mode"));

export const mount = (
    element: HTMLElement | null,
    update: (root: HTMLElement) => void,
    ...keys: StateKey[]
) => {
    if (element) {
        pushMount({
            element,
            update,
            keys,
        });
    }
};
export const mountOnce = (
    element: HTMLElement | null,
    update: (root: HTMLElement) => void
) => {
    if (element) {
        update(element);
    }
};

const matchKey = (keys: StateKey[], updated: StateKey[]) => {
    for (const key of keys) {
        if (updated.indexOf(key) >= 0) {
            return true;
        }
    }
    return false;
};

const emptyElement = (elem: Node) => {
    while (elem.firstChild) {
        removeElement(elem.firstChild);
    }
    return elem;
};

const removeElement = (elem: Node) => {
    emptyElement(elem);
    const parent = elem.parentNode;
    if (parent) {
        parent.removeChild(elem);
    }
};

const update = (updatedKeys: StateKey[]) => {
    foreachMount(({ element, update, keys }) => {
        if (matchKey(keys, updatedKeys)) {
            update(element);
        }
    });
};

export const hasSelection = () =>
    pipe2(
        getMode(),
        map((m) => {
            switch (m) {
                case "image":
                    return getImageSelection().length > 0;
                case "text":
                    return query("selection") !== null;
                default:
                    return false;
            }
        }),
        orElse(false)
    );

export const app = () => start(update);
