import { events } from "./dom.js";
import { ANCHOR, BUTTON, DIV, SPAN, SUPERSCRIPT } from "./html.js";
import { fromNullable, map, orElse, pipe2 } from "./option.js";
import { postIO, deleteIO } from "./remote.js";
import { assign, query, update } from "./state.js";
import { findTerm, Term } from "./term.js";

export type Paragraph = {
	id: number;
	index: number;
	textId: number;
	content: string;
};

export type ParagraphAnnotation = {
	id: number;
	user: number;
	parId: number;
	termId: number;
	textId: number;
	start: number;
	end: number;
	content: string;
	qualities: number[];
};

export type Qualifier = {
	id: number;
	name: string;
};

type Block = {
	start: number;
	end: number;
	content: string;
	annotations: ParagraphAnnotation[];
};

export type ParSel = {
	parId: number;
	start: number;
	end: number;
	content: string;
};

export type Highlight = {
	parId: number;
	termId: number;
};

export const findParAnnotation = (pid: number) =>
	fromNullable(query("annotations/paragraph").find(({ id }) => id === pid));

export const findQualifier = (id: number) =>
	fromNullable(query("qualifiers").find((q) => q.id === id));

const getHighlight = () => fromNullable(query("highlight"));

const setHighlight = (parId: number, termId: number) =>
	assign("highlight", { parId, termId });

const getSortedAnnotations = (parId: number) =>
	query("annotations/paragraph")
		.filter((a) => a.parId === parId)
		.sort((a, b) => a.start - b.start);

const deleteAnnotation = (id: number) =>
	deleteIO(`/api/paragraph/annotation/${id}`)
		.then(() =>
			update("annotations/paragraph", (xs) => xs.filter((a) => a.id !== id)),
		)
		.catch((err) => console.log("delete error", err));

const clamp =
	(start: number, end: number) => (annotation: ParagraphAnnotation) => ({
		...annotation,
		start: Math.max(start, annotation.start),
		end: Math.min(end, annotation.end),
	});

const compileAnnotations = ({ id, content }: Paragraph) => {
	const start = 0;
	const end = content.length;
	const subset = getSortedAnnotations(id).filter(
		(a) => a.start < end && a.end > start,
	);
	// .map(clamp(start, end));
	const intervals = [
		...new Set(
			subset.reduce<number[]>(
				(acc, a) => acc.concat(a.start, a.end),
				[start, end],
			),
		),
	].sort((a, b) => a - b);
	const findAnnot = (left: number, right: number) =>
		subset.filter((a) => a.start < right && a.end > left);
	const blocks: Block[] = [];
	for (let i = 1; i < intervals.length; i += 1) {
		blocks.push({
			start: intervals[i - 1],
			end: intervals[i],
			content: content.slice(intervals[i - 1], intervals[i]),
			annotations: findAnnot(intervals[i - 1], intervals[i]),
		});
	}
	return blocks;
};

const matchParent = (parent: Node, node: Node): boolean => {
	if (node === parent) {
		return true;
	}
	if (node.parentNode !== null) {
		return matchParent(parent, node.parentNode);
	}
	return false;
};

const select = (par: Paragraph, root: Node) => (event: MouseEvent) => {
	const computeBaseOffset = (startNode: Node) => {
		const startBlock = startNode.parentNode!;
		let baseOffset = 0;
		const childBlocks = Array.from(root.childNodes);
		for (const child of childBlocks) {
			if (child.isSameNode(startBlock)) {
				return baseOffset;
			}
			baseOffset += child.textContent?.length ?? 0;
		}
		throw new Error("Woo");
	};
	const withSelection = map((sel: Selection) => {
		const range = sel.getRangeAt(0);
		const startNode = range.startContainer;
		const endNode = range.endContainer;
		const content = range.toString().trim();

		if (content.length === 0) {
			return;
		}
		event.stopPropagation();

		if (!matchParent(root, startNode)) {
			range.setStart(root, 0);
		} else if (!matchParent(root, endNode)) {
			range.setEndAfter(root);
		}
		const baseOffset = computeBaseOffset(startNode);
		console.log(
			`select +${baseOffset} ${range.startOffset} ${range.endOffset}`,
		);
		assign("selection", {
			parId: par.id,
			start: baseOffset + range.startOffset,
			end: baseOffset + range.startOffset + content.length,
			content,
		});
	});
	withSelection(fromNullable(window.getSelection()));
};

const slightlyDifferent = (a: number, b: number) => Math.abs(a - b) > 1;

const highlightClass = (par: Paragraph, annotations: ParagraphAnnotation[]) =>
	pipe2(
		getHighlight(),
		map((h) =>
			h.parId === par.id &&
			annotations.findIndex(({ termId }) => h.termId === termId) >= 0
				? "highlight"
				: "",
		),
		orElse(""),
	);

const renderQualitySelect = (a: ParagraphAnnotation, q: Qualifier) => {
	const element = DIV("qualifier-select", q.name);
	element.addEventListener("click", () =>
		postIO("/api/annotation/quality/", {
			qualifierId: q.id,
			annotationId: a.id,
		})
			.then(() =>
				update("annotations/paragraph", (as) =>
					as.map((annot) => {
						if (annot.id === a.id) {
							const qualities = annot.qualities;
							qualities.push(q.id);
							return {
								...annot,
								qualities,
							};
						}
						return annot;
					}),
				),
			)
			.catch((err) => console.log("poste quality error:", err)),
	);
	return element;
};

const renderQualitySelected = (q: Qualifier) =>
	DIV("qualifier-selected", q.name);

const renderQuality = (root: Node, a: ParagraphAnnotation) => {
	const wrapper = DIV("qualifiers");
	query("qualifiers").forEach((q) => {
		if (a.qualities.indexOf(q.id) >= 0) {
			wrapper.appendChild(renderQualitySelected(q));
		} else {
			wrapper.appendChild(renderQualitySelect(a, q));
		}
	});
	root.appendChild(wrapper);
};

const renderDelete = (root: Node, a: ParagraphAnnotation) => {
	root.appendChild(
		DIV(
			"delete-annot",
			events(BUTTON("but", "delete"), (on) =>
				on("click", () => deleteAnnotation(a.id)),
			),
		),
	);
};

const renderSpan = (par: Paragraph) => ({ annotations, content }: Block) => {
	const rQualities = annotations.reduce(
		(acc, a) => acc + a.qualities.length,
		0,
	);
	const annotationClass =
		annotations.length > 0
			? `annotated n-${annotations.length} q-${rQualities}`
			: "";

	const span = SPAN(
		`block ${annotationClass} ${highlightClass(par, annotations)}`,
		content,
	);
	if (annotations.length > 0) {
		let startPos: [number, number] = [0, 0];
		span.addEventListener(
			"mousedown",
			({ screenX, screenY }) => (startPos = [screenX, screenY]),
		);
		span.addEventListener("click", ({ screenX, screenY }) => {
			if (
				slightlyDifferent(screenX, startPos[0]) ||
				slightlyDifferent(screenY, startPos[1])
			) {
				return;
			}
			assign("tooltip", {
				refElement: span,
				render: (inner) => {
					const renderTerm = (annot: ParagraphAnnotation) =>
						map(({ name, statement, lang, id }: Term) => {
							const anchor = ANCHOR(name, `/term/${id}.html`, name);
							anchor.setAttribute("target", "_blank");
							const nameNode = DIV(
								"term-name",
								anchor,
								SUPERSCRIPT("term-lang", lang),
							);
							const statementNode = DIV("term-statement", statement);
							inner.appendChild(DIV("term", nameNode, statementNode));

							renderQuality(inner, annot);
							if (annot.user === query("app/user")) {
								renderDelete(inner, annot);
							}
						});
					annotations.forEach((annot) =>
						renderTerm(annot)(findTerm(annot.termId)),
					);
				},
			});
		});
	}
	return span;
};

const withTerm = map((t: Term) => t.name);

const highlightClassForTerm = (par: Paragraph, termId: number) =>
	pipe2(
		getHighlight(),
		map((h) => (h.parId === par.id && h.termId === termId ? "highlight" : "")),
		orElse(""),
	);

const renderNote = (par: Paragraph) =>
	DIV(
		"note",
		...getSortedAnnotations(par.id)
			.reduce<number[]>(
				(acc, a) => (acc.indexOf(a.termId) >= 0 ? acc : acc.concat(a.termId)),
				[],
			)
			.map((termId) =>
				events(
					DIV(
						"note-annotation",
						SPAN(
							highlightClassForTerm(par, termId),
							pipe2(findTerm(termId), withTerm, orElse(termId.toString())),
						),
					),
					(add) => {
						add("click", () => setHighlight(par.id, termId));
					},
				),
			),
	);

export const renderParagraph = (par: Paragraph) => {
	const spans = compileAnnotations(par).map(renderSpan(par));
	const text = DIV("text", ...spans);
	const note = renderNote(par);
	const node = DIV(`paragraph par-${par.id}`, note, text);
	text.addEventListener("mousedown", () => assign("selection", null));
	text.addEventListener("mouseup", select(par, text));
	return node;
};
