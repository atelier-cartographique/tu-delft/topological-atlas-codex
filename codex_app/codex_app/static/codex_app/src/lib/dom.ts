// import { map, Option } from "./option.js";

// interface HTMLTags {
//     a: HTMLAnchorElement;
//     abbr: HTMLElement;
//     address: HTMLElement;
//     area: HTMLAreaElement;
//     article: HTMLElement;
//     aside: HTMLElement;
//     audio: HTMLAudioElement;
//     b: HTMLElement;
//     base: HTMLBaseElement;
//     bdi: HTMLElement;
//     bdo: HTMLElement;
//     big: HTMLElement;
//     blockquote: HTMLElement;
//     body: HTMLBodyElement;
//     br: HTMLBRElement;
//     button: HTMLButtonElement;
//     canvas: HTMLCanvasElement;
//     caption: HTMLElement;
//     cite: HTMLElement;
//     code: HTMLElement;
//     col: HTMLTableColElement;
//     colgroup: HTMLTableColElement;
//     data: HTMLElement;
//     datalist: HTMLDataListElement;
//     dd: HTMLElement;
//     del: HTMLElement;
//     details: HTMLElement;
//     dfn: HTMLElement;
//     dialog: HTMLElement;
//     div: HTMLDivElement;
//     dl: HTMLDListElement;
//     dt: HTMLElement;
//     em: HTMLElement;
//     embed: HTMLEmbedElement;
//     fieldset: HTMLFieldSetElement;
//     figcaption: HTMLElement;
//     figure: HTMLElement;
//     footer: HTMLElement;
//     form: HTMLFormElement;
//     h1: HTMLHeadingElement;
//     h2: HTMLHeadingElement;
//     h3: HTMLHeadingElement;
//     h4: HTMLHeadingElement;
//     h5: HTMLHeadingElement;
//     h6: HTMLHeadingElement;
//     head: HTMLElement;
//     header: HTMLElement;
//     hgroup: HTMLElement;
//     hr: HTMLHRElement;
//     html: HTMLHtmlElement;
//     i: HTMLElement;
//     iframe: HTMLIFrameElement;
//     img: HTMLImageElement;
//     input: HTMLInputElement;
//     ins: HTMLModElement;
//     kbd: HTMLElement;
//     keygen: HTMLElement;
//     label: HTMLLabelElement;
//     legend: HTMLLegendElement;
//     li: HTMLLIElement;
//     link: HTMLLinkElement;
//     main: HTMLElement;
//     map: HTMLMapElement;
//     mark: HTMLElement;
//     menu: HTMLElement;
//     menuitem: HTMLElement;
//     meta: HTMLMetaElement;
//     meter: HTMLElement;
//     nav: HTMLElement;
//     noscript: HTMLElement;
//     // object: HTMLObjectElement;
//     ol: HTMLOListElement;
//     optgroup: HTMLOptGroupElement;
//     option: HTMLOptionElement;
//     output: HTMLElement;
//     p: HTMLParagraphElement;
//     param: HTMLParamElement;
//     picture: HTMLElement;
//     pre: HTMLPreElement;
//     progress: HTMLProgressElement;
//     q: HTMLQuoteElement;
//     rp: HTMLElement;
//     rt: HTMLElement;
//     ruby: HTMLElement;
//     s: HTMLElement;
//     samp: HTMLElement;
//     script: HTMLScriptElement;
//     section: HTMLElement;
//     select: HTMLSelectElement;
//     small: HTMLElement;
//     source: HTMLSourceElement;
//     span: HTMLSpanElement;
//     strong: HTMLElement;
//     style: HTMLStyleElement;
//     sub: HTMLElement;
//     summary: HTMLElement;
//     sup: HTMLElement;
//     table: HTMLTableElement;
//     tbody: HTMLTableSectionElement;
//     td: HTMLTableCellElement;
//     textarea: HTMLTextAreaElement;
//     tfoot: HTMLTableSectionElement;
//     th: HTMLTableCellElement;
//     thead: HTMLTableSectionElement;
//     time: HTMLElement;
//     title: HTMLTitleElement;
//     tr: HTMLTableRowElement;
//     track: HTMLTrackElement;
//     u: HTMLElement;
//     ul: HTMLUListElement;
//     var: HTMLElement;
//     video: HTMLVideoElement;
//     wbr: HTMLElement;
// }

// const create = <T extends keyof HTMLTags>(tag: T): HTMLTags[T] =>
//     document.createElement(tag) as HTMLTags[T];

// export const appendText = (text: string) => (node: Element) => {
//     return node.appendChild(document.createTextNode(text));
// };

// export type AcNode =
//     | HTMLElement
//     | string
//     | number
//     | Option<HTMLElement | string | number>;

// const createWithClass = <T extends keyof HTMLTags>(
//     tag: T,
//     className: string
// ) => {
//     const node = create(tag);
//     node.setAttribute("class", className);
//     return node;
// };

// const appendLiteral = (node: HTMLElement) => (value: string | number) => {
//     if (typeof value === "number") {
//         appendText(value.toLocaleString())(node);
//     } else {
//         appendText(value)(node);
//     }
// };

// const appendElement = (node: HTMLElement) => (value: HTMLElement) => {
//     node.appendChild(value);
// };

// const appendOption = (node: HTMLElement) =>
//     map<HTMLElement | string | number, void>((value) => {
//         if (value instanceof HTMLElement) {
//             appendElement(node)(value);
//         } else {
//             appendLiteral(node)(value);
//         }
//     });

// const appendNode = (node: HTMLElement) => (value: AcNode) => {
//     if (typeof value === "number" || typeof value === "string") {
//         appendLiteral(node)(value);
//     } else if (value instanceof HTMLElement) {
//         appendElement(node)(value);
//     } else {
//         appendOption(node)(value);
//     }
// };

// const createWithChildren = <T extends keyof HTMLTags>(
//     tag: T,
//     className: string,
//     ns: AcNode[]
// ) => {
//     const node = createWithClass(tag, className);
//     ns.flat().forEach(appendNode(node));
//     return node;
// };

// const activeClass = (a: boolean) => (a ? "selected" : "unselected");

// export const DIV = (className: string, ...ns: AcNode[]) =>
//     createWithChildren("div", className, ns);

// export const SPAN = (className: string, ...ns: AcNode[]) =>
//     createWithChildren("span", className, ns);

// export const LABEL = (className: string, ...ns: AcNode[]) =>
//     createWithChildren("label", className, ns);

// export const FIELDSET = (className: string, ...ns: AcNode[]) =>
//     createWithChildren("fieldset", className, ns);

// export const H1 = (className: string, ...ns: AcNode[]) =>
//     createWithChildren("h1", className, ns);

// export const H2 = (className: string, ...ns: AcNode[]) =>
//     createWithChildren("h2", className, ns);

// export const H3 = (className: string, ...ns: AcNode[]) =>
//     createWithChildren("h3", className, ns);

// export const NODISPLAY = () => {
//     const div = DIV("");
//     div.style.display = "none";
//     return div;
// };

interface ToString {
	toString(): string;
}

export const attrs = <E extends HTMLElement>(
	e: E,
	f: (s: (k: string, v: ToString) => void) => void,
) => {
	const set = (k: string, v: ToString) => {
		e.setAttribute(k, v.toString());
	};
	f(set);
	return e;
};

export const events = <E extends HTMLElement>(
	e: E,
	f: (
		s: <K extends keyof HTMLElementEventMap>(
			k: K,
			listener: (ev: HTMLElementEventMap[K]) => void,
		) => void,
	) => void,
) => {
	const add = <K extends keyof HTMLElementEventMap>(
		k: K,
		listener: (ev: HTMLElementEventMap[K]) => void,
	) => {
		e.addEventListener(k, listener);
	};
	f(add);
	return e;
};

const uniq = <T>(xs: T[]) => Array.from(new Set(xs));

export function addClass(elem: Element, c: string) {
	const ecStr = elem.getAttribute("class");
	const ec = ecStr ? ecStr.split(" ") : [];
	ec.push(c);
	elem.setAttribute("class", uniq(ec).join(" "));
}

export function toggleClass(elem: Element, c: string) {
	const ecStr = elem.getAttribute("class");
	const ec = ecStr ? ecStr.split(" ") : [];
	if (ec.indexOf(c) < 0) {
		addClass(elem, c);
	} else {
		removeClass(elem, c);
	}
}

export function hasClass(elem: Element, c: string) {
	const ecStr = elem.getAttribute("class");
	const ec = ecStr ? ecStr.split(" ") : [];
	return !(ec.indexOf(c) < 0);
}

export function removeClass(elem: Element, c: string) {
	const ecStr = elem.getAttribute("class");
	const ec = ecStr ? ecStr.split(" ") : [];
	elem.setAttribute("class", ec.filter((cc) => cc !== c).join(" "));
}

export function emptyElement(elem: Node) {
	while (elem.firstChild) {
		removeElement(elem.firstChild);
	}
	return elem;
}

export function removeElement(elem: Node, keepChildren = false) {
	if (!keepChildren) {
		emptyElement(elem);
	}
	const parent = elem.parentNode;
	if (parent) {
		parent.removeChild(elem);
	}
}
