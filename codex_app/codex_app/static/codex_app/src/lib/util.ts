import { none, some } from "./option.js";

export const tryNumber = (n: unknown) => {
    if (typeof n === "number") {
        return some(n);
    }
    if (typeof n === "string") {
        const tn = parseFloat(n);
        if (!Number.isNaN(tn)) {
            return some(tn);
        }
    }
    return none;
};

export const iife = <T>(f: () => T) => f();
