/**
 *
 * Oleg V. Polikarpotchkin
 * https://ovpwp.wordpress.com/2008/12/17/how-to-draw-a-smooth-curve-through-a-set-of-2d-points-with-bezier-methods/

/// <summary>
/// Bezier Spline methods
/// </summary>
public static class BezierSpline
{
    /// <summary>
    /// Get open-ended Bezier Spline Control Points.
    /// </summary>
    /// <param name="knots">Input Knot Bezier spline points.</param>
    /// <param name="firstControlPoints">Output First Control points array of knots.Length - 1 length.</param>
    /// <param name="secondControlPoints">Output Second Control points array of knots.Length - 1 length.</param>
    public static void GetCurveControlPoints(Point[] knots, out Point[] firstControlPoints, out Point[] secondControlPoints)
    {
         int n = knots.Length - 1;
         if (n < 1)
         {
             firstControlPoints = new Point[0];
             secondControlPoints = new Point[0];
             return;
         }

         // Calculate first Bezier control points
         // Right hand side vector
         double[] rhs = new double[n];

         // Set right hand side X values
         for (int i = 1; i < n - 1; ++i)
             rhs[i] = 4 * knots[i].X + 2 * knots[i + 1].X;
         rhs[0] = knots[0].X + 2 * knots[1].X;
         rhs[n - 1] = 3 * knots[n - 1].X;
         // Get first control points X-values
         double[] x = GetFirstControlPoints(rhs);

         // Set right hand side Y values
         for (int i = 1; i < n - 1; ++i)
             rhs[i] = 4 * knots[i].Y + 2 * knots[i + 1].Y;
         rhs[0] = knots[0].Y + 2 * knots[1].Y;
         rhs[n - 1] = 3 * knots[n - 1].Y;
         // Get first control points Y-values
         double[] y = GetFirstControlPoints(rhs);

         // Fill output arrays.
         firstControlPoints = new Point[n];
         secondControlPoints = new Point[n];
         for (int i = 0; i < n; ++i)
         {
             // First control point
             firstControlPoints[i] = new Point(x[i], y[i]);
             // Second control point
             if (i < n - 1)
                 secondControlPoints[i] = new Point(2 * knots[i + 1].X - x[i + 1], 2 * knots[i + 1].Y - y[i + 1]);
             else
                 secondControlPoints[i] = new Point((knots[n].X + x[n - 1]) / 2, (knots[n].Y + y[n - 1]) / 2);
         }
    }

    /// <summary>
    /// Solves a tridiagonal system for one of coordinates (x or y) of first Bezier control points.
    /// </summary>
    /// <param name="rhs">Right hand side vector.</param>
    /// <returns>Solution vector.</returns>
    private static double[] GetFirstControlPoints(double[] rhs)
    {
         int n = rhs.Length;
         double[] x = new double[n]; // Solution vector.
         double[] tmp = new double[n]; // Temp workspace.

         double b = 2.0;
         x[0] = rhs[0] / b;
         for (int i = 1; i < n; i++) // Decomposition and forward substitution.
         {
             tmp[i] = 1 / b;
             b = (i < n - 1 ? 4.0 : 2.0) - tmp[i];
             x[i] = (rhs[i] - x[i - 1]) / b;
         }
         for (int i = 1; i < n; i++)
             x[n - i - 1] -= tmp[n - i] * x[n - i]; // Backsubstitution.
         return x;
    }
}
 *
 */

export type Point = { x: number; y: number };
const point = (x: number, y: number): Point => ({ x, y });

type Control = {
    first: Point[];
    second: Point[];
};

const getFirstControlPoints = (rhs: number[]): number[] => {
    const n = rhs.length;
    const x: number[] = new Array(n); // Solution vector.
    const tmp: number[] = new Array(n); // Temp workspace.

    let b = 2.0;
    x[0] = rhs[0] / b;
    for (
        let i = 1;
        i < n;
        i += 1 // Decomposition and forward substitution.
    ) {
        tmp[i] = 1 / b;
        b = (i < n - 1 ? 4.0 : 2.0) - tmp[i];
        x[i] = (rhs[i] - x[i - 1]) / b;
    }
    for (let i = 1; i < n; i += 1) {
        x[n - i - 1] -= tmp[n - i] * x[n - i]; // Backsubstitution.
    }
    return x;
};

export const getCurveControlPoints = (knots: Point[]): Control => {
    const n = knots.length - 1;
    if (n < 1) {
        return {
            first: [],
            second: [],
        };
    }

    // Calculate first Bezier control points
    // Right hand side vector
    const rhs = new Array(n);

    // Set right hand side X values
    for (let i = 1; i < n - 1; i += 1) {
        rhs[i] = 4 * knots[i].x + 2 * knots[i + 1].x;
    }
    rhs[0] = knots[0].x + 2 * knots[1].x;
    rhs[n - 1] = 3 * knots[n - 1].x;
    // Get first control points X-values
    const x = getFirstControlPoints(rhs);

    // Set right hand side Y values
    for (let i = 1; i < n - 1; i += 1) {
        rhs[i] = 4 * knots[i].y + 2 * knots[i + 1].y;
    }
    rhs[0] = knots[0].y + 2 * knots[1].y;
    rhs[n - 1] = 3 * knots[n - 1].y;
    // Get first control points Y-values
    const y = getFirstControlPoints(rhs);

    // Fill output arrays.
    const first: Point[] = new Array(n);
    const second: Point[] = new Array(n);
    for (let i = 0; i < n; i += 1) {
        // First control point
        first[i] = point(x[i], y[i]);
        // Second control point
        if (i < n - 1) {
            second[i] = point(
                2 * knots[i + 1].x - x[i + 1],
                2 * knots[i + 1].y - y[i + 1]
            );
        } else {
            second[i] = point(
                (knots[n].x + x[n - 1]) / 2,
                (knots[n].y + y[n - 1]) / 2
            );
        }
    }

    return { first, second };
};

export type CurveData = {
    x: number;
    y: number;
    cp1: Point;
    cp2: Point;
    first: boolean;
};

export const pointsToCurve = (points: [number, number][]): CurveData[] => {
    const { first, second } = getCurveControlPoints(
        points.map(([x, y]) => ({ x, y }))
    );
    const nullPoint = { x: 0, y: 0 };
    return points.map(([x, y], i) => ({
        x,
        y,
        cp1: i > 0 ? first[i - 1] : nullPoint,
        cp2: i > 0 ? second[i - 1] : nullPoint,
        first: i === 0,
    }));
};

export const drawCurve = (
    ctx: CanvasRenderingContext2D,
    curve: CurveData[]
) => {
    ctx.beginPath();
    curve.map(({ x, y, cp1, cp2, first }) => {
        if (first) {
            ctx.moveTo(x, y);
        } else {
            ctx.bezierCurveTo(cp1.x, cp1.y, cp2.x, cp2.y, x, y);
        }
    });
};

// const test = () => {
//     const scale = mul(10);
//     const points = [
//         scale(point(10, 10)),
//         scale(point(15, 20)),
//         scale(point(20, 20)),
//         scale(point(30, 30)),
//         scale(point(40, 25)),
//         scale(point(10, 10)),
//     ];

//     const { first, second } = getCurveControlPoints(points);
//     const canvas = document.createElement('canvas');
//     canvas.width = 1000;
//     canvas.height = 1000;
//     document.body.appendChild(canvas);
//     const ctx = canvas.getContext('2d')!;
//     ctx.fillStyle = 'white';
//     ctx.fillRect(0, 0, 1000, 1000);
//     ctx.beginPath();
//     ctx.strokeStyle = 'red';
//     ctx.lineWidth = 2;
//     ctx.moveTo(points[0].x, points[0].y);
//     points.slice(1, points.length - 1).map(({ x, y }, i) => {
//         const c1 = first[i];
//         const c2 = second[i];
//         console.log(c1.x, c1.y, c2.x, c2.y, x, y);
//         ctx.bezierCurveTo(c1.x, c1.y, c2.x, c2.y, x, y);
//     });
//     ctx.stroke();
//     points.map(({ x, y }) => {
//         ctx.fillStyle = 'blue';
//         ctx.fillRect(x, y, 5, 5);
//     });
// };
