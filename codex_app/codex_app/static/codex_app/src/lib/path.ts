import { index } from "./array.js";
import { attrs, emptyElement, events } from "./dom.js";
import {
	ANCHOR,
	BaseNode,
	BUTTON,
	CANVAS,
	DIV,
	IMG,
	INPUT,
	SPAN,
	TEXTAREA,
} from "./html.js";
import {
	CodexImage,
	findImage,
	findImageAnnotation,
	ImageAnnotation,
	renderImageAnnotation,
} from "./image.js";
import {
	alt,
	fromNullable,
	map,
	map2,
	none,
	orElse,
	pipe2,
	some,
} from "./option.js";
import { findParAnnotation, ParagraphAnnotation } from "./paragraph.js";
import { deleteIO, postIO } from "./remote.js";
import { assign, query, update, Viz0PathMode } from "./state.js";
import { findTerm, Term } from "./term.js";
import {
	createTransitional,
	findTransitionalText,
	TransitionalText,
} from "./transitional.js";

declare function termIlluURL(tid: string): string;

export type NodeBase = {
	id: number;
	tid: number;
};

export type NodeTerm = NodeBase & {
	readonly type: "te";
};

export type NodeImage = NodeBase & {
	readonly type: "im";
};

export type NodeTextAnnotation = NodeBase & {
	readonly type: "ta";
};

export type NodeImageAnnotation = NodeBase & {
	readonly type: "ia";
};

export type NodeTransitionalText = NodeBase & {
	readonly type: "tt";
};

export type PathNode =
	| NodeTerm
	| NodeImage
	| NodeTextAnnotation
	| NodeImageAnnotation
	| NodeTransitionalText;

export type PathNodeType = PathNode["type"];

export type Path = {
	id: number;
	name: string | null;
	closed: boolean;
	nodes: PathNode[];
	username: string;
};

export type PathCursor = Path & {
	index: number;
};

export const currentNode = (pc: PathCursor) => index(pc.index, pc.nodes);

export const getPaths = () =>
	query("paths")
		.concat()
		.sort((a, b) => a.id - b.id);

export const getOpenPath = () =>
	fromNullable(getPaths().find(({ closed }) => !closed));

export const createPath = () =>
	postIO<Path, {}>("/api/path/create/", {}).then((path) =>
		update("paths", (old) => old.concat(path)),
	);

export const closePath = (pathId: number) =>
	postIO<Path, {}>(`/api/path/close/${pathId}`, {}).then((path) =>
		update("paths", (old) =>
			old.filter(({ id }) => path.id !== id).concat(path),
		),
	);
export const openPath = (pathId: number) =>
	postIO<Path, {}>(`/api/path/open/${pathId}`, {}).then((path) =>
		update("paths", (old) =>
			old.filter(({ id }) => path.id !== id).concat(path),
		),
	);
export const namePath = (path: Path) =>
	postIO<Path, Path>(`/api/path/name/${path.id}`, path).then((path) =>
		update("paths", (old) =>
			old.filter(({ id }) => path.id !== id).concat(path),
		),
	);

const deleteNode = (pathId: number, nodeId: number) =>
	deleteIO<Path>(`/api/path/delete/${pathId}/${nodeId}`).then((path) =>
		update("paths", (old) =>
			old.filter(({ id }) => path.id !== id).concat(path),
		),
	);

const swapNode = (path: Path, node: PathNode, dir: "up" | "down") => {
	const sourceIndex = path.nodes.findIndex(({ id }) => id === node.id);
	const withTarget = (tnode: PathNode) =>
		postIO<Path, {}>(
			`/api/path/swap/${path.id}/${node.id}/${tnode.id}`,
			{},
		).then((path) =>
			update("paths", (old) =>
				old.filter(({ id }) => path.id !== id).concat(path),
			),
		);
	const inc = dir === "down" ? 1 : -1;
	map(withTarget)(index(sourceIndex + inc, path.nodes));
};

const downNode = (path: Path, node: PathNode) => swapNode(path, node, "down");

const upNode = (path: Path, node: PathNode) => swapNode(path, node, "up");

const createNodeWithPath = (
	nodeType: PathNodeType,
	pathId: number,
	targetId: number,
) =>
	postIO<Path, {}>(`/api/path/add/${nodeType}/${pathId}/${targetId}`, {}).then(
		(path) =>
			update("paths", (old) =>
				old.filter(({ id }) => path.id !== id).concat(path),
			),
	);

export const findPath = (pid: number) =>
	fromNullable(getPaths().find(({ id }) => id === pid));

export const createNode = (nodeType: PathNodeType, targetId: number) =>
	map(({ id }: Path) => createNodeWithPath(nodeType, id, targetId))(
		getOpenPath(),
	);

const renderCreate = (root: HTMLElement) => () => {
	const button = BUTTON("create-path", "create path");
	button.addEventListener("click", createPath);
	root.appendChild(button);
};

const renderNodeTerm = ({ tid }: NodeTerm) =>
	map2(
		map(({ uuid, name, statement }: Term) =>
			DIV(
				"path-node term",
				DIV(
					"term-head",
					DIV("", IMG("picto", termIlluURL(uuid))),
					DIV("label", ANCHOR("", `/viz0/term/${tid}.html`, name)),
				),
				DIV("description", statement),
			),
		),
		orElse(DIV("path-node term  empty")),
	)(findTerm(tid));

const renderNodeImage = (node: NodeImage) => DIV("path-node");

const renderNodeTextNote = (node: NodeTextAnnotation) =>
	DIV(
		"path-node par-annot",
		map((a: ParagraphAnnotation) => a.content)(findParAnnotation(node.tid)),
	);

const withImageAnnotation = (annotation: ImageAnnotation) => {
	const width = 400;
	const height = 400;

	const render = map((i: CodexImage) => {
		const canvas = CANVAS("image-annotation", width, height);
		const ctx = canvas.getContext("2d");
		if (ctx !== null) {
			renderImageAnnotation(i, ctx, annotation, [width, height]);
		}
		return canvas;
	});

	return orElse<BaseNode>(DIV("err", "error when loading image"))(
		render(findImage(annotation.imageId)),
	);
};

const renderNodetransitionalText = (node: NodeTransitionalText) =>
	DIV(
		"path-node par-text",
		map((a: TransitionalText) => a.content)(findTransitionalText(node.tid)),
	);

const renderNodeImageNote = (node: NodeImageAnnotation) =>
	DIV(
		"path-node image-annot",
		map(withImageAnnotation)(findImageAnnotation(node.tid)),
	);

const labelUp = "⇑";
const labelDown = "⇓";

const renderDown = (path: Path, node: PathNode) =>
	DIV(
		"up-down",
		SPAN("up disabled", labelUp),
		events(SPAN("down", labelDown), (add) =>
			add("click", () => downNode(path, node)),
		),
	);

const renderUpDown = (path: Path, node: PathNode) =>
	DIV(
		"up-down",
		events(SPAN("up", labelUp), (add) =>
			add("click", () => upNode(path, node)),
		),
		events(SPAN("down", labelDown), (add) =>
			add("click", () => downNode(path, node)),
		),
	);

const renderUp = (path: Path, node: PathNode) =>
	DIV(
		"up-down",
		events(SPAN("up", labelUp), (add) =>
			add("click", () => upNode(path, node)),
		),
		SPAN("down disabled", labelDown),
	);

const selectDir = (len: number, index: number) => {
	if (len > 1) {
		if (index === 0) {
			return some(renderDown);
		} else if (index === len - 1) {
			return some(renderUp);
		}
		return some(renderUpDown);
	}
	return none;
};

const wrapNode =
	(path: Path, node: PathNode, index: number) => (elem: HTMLElement) =>
		DIV(
			"node-wrapper",
			DIV(
				"node-tools",

				map((render: typeof renderDown) => render(path, node))(
					selectDir(path.nodes.length, index),
				),
				events(BUTTON("delete", "⤬"), (add) =>
					add("click", () => deleteNode(path.id, node.id)),
				),
			),
			elem,
		);

const renderNode = (node: PathNode) => {
	switch (node.type) {
		case "te":
			return renderNodeTerm(node);
		case "im":
			return renderNodeImage(node);
		case "ta":
			return renderNodeTextNote(node);
		case "ia":
			return renderNodeImageNote(node);
		case "tt":
			return renderNodetransitionalText(node);
	}
};

const renderWrappedNode = (path: Path) => (node: PathNode, index: number) =>
	wrapNode(path, node, index)(renderNode(node));

const renderTransTextInput = () => {
	const textArea = TEXTAREA("");
	return DIV(
		"trans-input",
		DIV("trans-label", "add a textual transition"),
		textArea,
		events(BUTTON("but", "Insert"), (on) =>
			on("click", () =>
				createTransitional(textArea.value).then((tt) =>
					createNode("tt", tt.id),
				),
			),
		),
	);
};

const renderPathName = (path: Path) => {
	let name = path.name;
	return attrs(
		DIV(
			"path-name",
			// H1("path-id", path.id),
			events(
				attrs(INPUT("input-name", "text"), (set) =>
					set("placeholder", path.name ?? "Give it a name"),
				),
				(on) =>
					on("change", (e) => (name = (e.target as HTMLInputElement).value)),
			),
			events(BUTTON("change", "save"), (on) =>
				on("click", () => {
					if (name !== null) {
						namePath({ ...path, name });
					}
				}),
			),
		),
		(set) => set("title", path.id),
	);
};

export const renderPath = (root: Element) => (path: Path) => {
	root.append(renderPathName(path));
	const renderAdd = map((tid: number) => {
		const button = BUTTON("add-node", "add current term");
		button.addEventListener("click", () =>
			createNodeWithPath("te", path.id, tid),
		);
		root.appendChild(button);
	});

	renderAdd(fromNullable(query("currentTerm")));

	root.append(
		DIV(
			"nodes",
			...path.nodes.map(renderWrappedNode(path)).concat(renderTransTextInput()),
		),
		events(BUTTON("close-path", "save path"), (on) =>
			on("click", () => closePath(path.id)),
		),
	);
	// root.append();
};

const renderListItem = (path: Path) =>
	DIV(
		"path-item",
		renderPathName(path),
		map(renderNode)(index(0, path.nodes)),
		events(BUTTON("load", "open path"), (on) =>
			on("click", () => {
				Promise.all(
					getPaths()
						.filter(({ id, closed }) => id !== path.id && !closed)
						.map(({ id }) => closePath(id)),
				)
					.then(() => openPath(path.id))
					.then(() => assign("viz0/path-mode", "single"))
					.catch((err) => console.log(err));
			}),
		),
	);

const renderList = (root: HTMLElement) => {
	root.append(DIV("path-list", ...getPaths().map(renderListItem)));
};

const renderModeSwitch = (current: Viz0PathMode) =>
	DIV(
		"mode-switch",
		current === "single"
			? events(BUTTON("", "list of paths"), (on) =>
					on("click", () => assign("viz0/path-mode", "list")),
			  )
			: events(BUTTON("", "single path"), (on) =>
					on("click", () => assign("viz0/path-mode", "single")),
			  ),
	);

export const renderPathBox = (root: HTMLElement) => {
	emptyElement(root);
	const mode = query("viz0/path-mode");
	root.append(renderModeSwitch(mode));

	if (mode === "single") {
		pipe2(getOpenPath(), map(renderPath(root)), alt(renderCreate(root)));
	} else {
		renderList(root);
	}
};
