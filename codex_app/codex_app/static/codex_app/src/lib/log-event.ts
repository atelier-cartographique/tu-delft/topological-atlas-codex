import { update } from "./state.js";

export type EventTerm = {
    readonly tag: "term";
    id: number;
};

export type EventImage = {
    readonly tag: "image";
    id: number;
};

export type EventImageAnnotation = {
    readonly tag: "image-annotation";
    id: number;
};

export type LogEvent = EventTerm | EventImage | EventImageAnnotation;

export const eventTerm = (id: number): EventTerm => ({ tag: "term", id });
export const eventImage = (id: number): EventImage => ({ tag: "image", id });
export const eventImageAnnotation = (id: number): EventImageAnnotation => ({
    tag: "image-annotation",
    id,
});

export const logEvent = (event: LogEvent) =>
    update("viz1/user-log", (log) => log.concat(event));
