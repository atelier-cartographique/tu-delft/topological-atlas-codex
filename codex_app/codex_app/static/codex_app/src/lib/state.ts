import type { Category } from "./category.js";
import type { CodexImage, ImageAnnotation } from "./image.js";
import type {
    ParagraphAnnotation,
    Highlight,
    Paragraph,
    ParSel,
    Qualifier,
} from "./paragraph.js";
import type { Term } from "./term.js";
import type { Tooltip } from "./tooltip.js";
import type { Path, PathCursor } from "./path.js";
import { TransitionalText } from "./transitional";
import type { Place } from "./place.js";
import type { LogEvent } from "./log-event.js";
import { LandingInteraction } from "../viz1/landing.js";

type Nullable<T> = T | null;

const nullable = <T>(a: Nullable<T>) => a;

export type Mode = "text" | "image" | "path";

export type Viz0PathMode = "single" | "list";

const state = {
    _version: -1,
    "app/user": nullable<number>(null),
    "app/csrf": nullable<string>(null),
    "app/mode": nullable<Mode>(null),
    tooltip: nullable<Tooltip>(null),
    selection: nullable<ParSel>(null),
    image: [] as CodexImage[],
    paragraphs: [] as Paragraph[],
    "annotations/paragraph": [] as ParagraphAnnotation[],
    "annotations/image": [] as ImageAnnotation[],
    terms: [] as Term[],
    categories: [] as Category[],
    places: [] as Place[],
    highlight: nullable<Highlight>(null),
    "image/select-list": [] as number[],
    "image/loaded": false,
    paths: [] as Path[],
    currentTerm: nullable<number>(null),
    qualifiers: [] as Qualifier[],
    images: [] as CodexImage[],
    // "highlight/terms": [] as number[],
    // "show/par": false,
    "viz1/landing": false,
    "viz1/term": nullable<number>(null),
    "viz1/path": nullable<PathCursor>(null),
    "viz1/user-log": [] as LogEvent[],
    // "viz1/frame/highlight": nullable<number>(null),
    "viz1/landing/interaction": nullable<LandingInteraction>(null),
    "viz1/landing/info": true,
    "viz0/user": nullable<string>(null),
    "viz0/path": nullable<number>(null),
    "viz0/path-mode": "single" as Viz0PathMode,
    transitionals: [] as TransitionalText[],
};

export type State = typeof state;
export type StateKey = keyof State;

const updateSym = Symbol("sym");
type UpdateSym = typeof updateSym;
let updatedKeys: StateKey[] = [];

export const init = (initState: Partial<State>) =>
    Object.keys(initState)
        .map<StateKey>((k) => k as StateKey)
        .forEach((k) => {
            const value = initState[k];
            if (value !== undefined) {
                assign(k, value, updateSym);
            }
        });

export const query = <K extends StateKey>(key: K): State[K] => state[key];

export const assign = <K extends StateKey>(
    key: K,
    value: State[K],
    quiet = nullable<UpdateSym>(null)
) => {
    state[key] = value;
    state._version = state._version + 1;
    console.log(`[${state._version}] assign`, key, value);
    if (quiet === null) {
        updatedKeys.push(key);
    }
};
export const update = <K extends StateKey>(
    key: K,
    u: (old: State[K]) => State[K]
) => assign(key, u(state[key]));

let interval: number | null = null;
const framer = (update: (keys: StateKey[]) => void) => {
    const FRAME_RATE = 16;
    let lastUpdateVersion = -1;
    let lastTimeUpdate = 0;
    let init = false;
    const frame = (animTimestamp: number) => {
        const stateVersion = state._version;
        if (init === false) {
            init = true;
            update(Object.keys(state) as StateKey[]);
            lastUpdateVersion = stateVersion;
            lastTimeUpdate = animTimestamp;
        } else if (
            lastUpdateVersion < stateVersion &&
            updatedKeys.length > 0 &&
            animTimestamp - lastTimeUpdate > FRAME_RATE
        ) {
            update(updatedKeys);
            lastUpdateVersion = stateVersion;
            lastTimeUpdate = animTimestamp;
            updatedKeys = [];
        }
        interval = window.requestAnimationFrame(frame);
    };
    return frame;
};

export const start = (update: (keys: StateKey[]) => void) => {
    if (interval !== null) {
        throw new Error("Framer already started");
    }
    interval = window.requestAnimationFrame(framer(update));
    state._version = 0;
    return Promise.resolve(0);
};
