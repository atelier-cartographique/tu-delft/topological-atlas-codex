import { hasSelection } from "./app.js";
import { ANCHOR, BUTTON, DIV, H3 } from "./html.js";
import { Language } from "./lang.js";
import { fromNullable, map } from "./option.js";
import { assign, query, update } from "./state.js";
import {
    getCurrentImage,
    CodexImage,
    ImageAnnotation,
    getImageSelection,
} from "./image.js";
import { postIO } from "./remote.js";

export type Category = {
    id: number;
    name: string;
    description: string;
    lang: Language;
};

export const findCategory = (catId: number) =>
    fromNullable(query("categories").find(({ id }) => id === catId));

// const postImageAnnotation = (data: Omit<ImageAnnotationCat, "id">) =>
//     postIO<ImageAnnotation, Omit<ImageAnnotation, "id">>(
//         "/api/image/annotation/",
//         data
//     );

// const select = (cat: Category) =>
//     map((image: CodexImage) => {
//         const triangles = getImageSelection();
//         postImageAnnotation({
//             tag: "category",
//             imageId: image.id,
//             categoryId: cat.id,
//             triangles,
//         })
//             .then((a) => update("annotations/image", (as) => as.concat(a)))
//             .catch((err) => console.log("post annotation error", err));
//         assign("image/select-list", []);
//     })(getCurrentImage());

// export const renderCatButton = (cat: Category) => {
//     const button = BUTTON("cat-select", cat.name);
//     if (hasSelection()) {
//         button.addEventListener("click", () => select(cat));
//     } else {
//         button.setAttribute("disabled", "true");
//     }
//     const node = DIV("cat-wrapper", button);
//     return node;
// };

// export const renderCategory = (cat: Category) => {
//     const button = BUTTON("cat-select cat-select-short", "✓");
//     if (hasSelection()) {
//         button.addEventListener("click", () => select(cat));
//     } else {
//         button.setAttribute("disabled", "true");
//     }
//     const anchor = ANCHOR("cat-link", `/category/${cat.id}.html`, cat.name);
//     anchor.setAttribute("target", "_blank");
//     const title = H3("cat-name", anchor, button);
//     const description = DIV("cat-description", cat.description);
//     const node = DIV("term", title, description);
//     return node;
// };
