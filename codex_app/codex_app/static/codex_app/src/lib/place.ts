import { CodexImage } from "./image.js";
import { bind, fromNullable } from "./option.js";
import { query } from "./state.js";
import { Point } from "geojson";

type BasePlace = {
    id: number;
    name: string;
    alias: string[];
    point: null;
};

export type PlaceWithPoint = {
    id: number;
    name: string;
    alias: string[];
    point: Point;
};

export type Place = BasePlace | PlaceWithPoint;

export const isPlaceWithPoint = (p: Place): p is PlaceWithPoint =>
    p.point !== null;

export const getImageLocation = (i: CodexImage) =>
    bind((pid: number) =>
        fromNullable(query("places").find(({ id }) => id === pid))
    )(fromNullable(i.location));

export const findPlaceByName = (placeName: string) => {
    const pn = placeName.toLowerCase();
    return fromNullable(
        query("places").find(
            ({ name, alias }) =>
                name.toLowerCase() === pn ||
                alias.map((a) => a.toLowerCase()).indexOf(pn) >= 0
        )
    );
};
export const findImageInPlace = (place: Place) =>
    fromNullable(query("images").find((i) => i.location === place.id));

export const findPlace = (pid: number) =>
    fromNullable(query("places").find(({ id }) => id === pid));

export const placesWithPoint = () => {
    const places: PlaceWithPoint[] = [];
    query("places").forEach((p) => {
        if (isPlaceWithPoint(p)) {
            places.push(p);
        }
    });
    return places;
};

export const placesExtent = (): [number, number, number, number] => {
    let minx = Number.MAX_VALUE;
    let miny = Number.MAX_VALUE;
    let maxx = Number.MIN_SAFE_INTEGER;
    let maxy = Number.MIN_SAFE_INTEGER;
    placesWithPoint().forEach(({ point }) => {
        const [x, y] = point.coordinates;
        minx = Math.min(minx, x);
        miny = Math.min(miny, y);
        maxx = Math.max(maxx, x);
        maxy = Math.max(maxy, y);
    });
    return [minx, miny, maxx, maxy];
};
