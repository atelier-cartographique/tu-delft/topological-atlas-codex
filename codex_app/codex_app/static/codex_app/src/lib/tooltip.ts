import { emptyElement, addClass, removeClass } from "./dom.js";
import { DIV } from "./html.js";

import { fromNullable, map } from "./option.js";
import { assign, query } from "./state.js";

export type Tooltip = {
	refElement: HTMLElement;
	render: (root: Node) => void;
};

const px = (n: number) => `${n}px`;

let inited = false;

export const renderTooltip = (root: HTMLElement) => {
	emptyElement(root);
	const { top, left } = root.getBoundingClientRect();

	if (!inited) {
		inited = true;
		root.addEventListener("click", () => {
			removeClass(root, "active");
			assign("tooltip", null);
		});
	}
	const withTooltip = map(({ refElement, render }: Tooltip) => {
		const refRect = refElement.getBoundingClientRect();
		const inner = DIV("tooltip-inner");
		addClass(root, "active");
		root.appendChild(inner);
		inner.style.position = "absolute";
		inner.style.top = px(refRect.bottom - top);
		inner.style.left = px(refRect.x - left);
		inner.style.width = px(Math.min(Math.max(refRect.width * 1.5, 400), 200));
		render(inner);
	});

	withTooltip(fromNullable(query("tooltip")));
};
