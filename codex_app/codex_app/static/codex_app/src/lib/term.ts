import { emptyElement } from "./dom.js";
import { ImageAnnotation, ImageAnnotationTerm } from "./image.js";

import { Language } from "./lang.js";
import { bind, fromNullable, map, orElse, pipe2 } from "./option.js";
import { ParagraphAnnotation } from "./paragraph.js";
import { postIO } from "./remote.js";
import { query } from "./state.js";

export type Term = {
    id: number;
    uuid: string;
    name: string;
    statement: string;
    lang: Language;
    categories: number[];
};

export const TermLocationUUID = "40b39814-4783-4db7-bb34-8bd14fbcede7";

export const postParagraphAnnotation = (
    data: Omit<ParagraphAnnotation, "id" | "qualities">
) =>
    postIO<ParagraphAnnotation, Omit<ParagraphAnnotation, "id" | "qualities">>(
        "/api/paragraph/annotation/",
        data
    );

export const findTerm = (id: number) =>
    fromNullable(query("terms").find((t) => t.id === id));

export const getTermsInCategory = (cid: number) =>
    query("terms").filter((t) => t.categories.indexOf(cid) >= 0);

export const postImageAnnotation = (data: Omit<ImageAnnotationTerm, "id">) =>
    postIO<ImageAnnotation, Omit<ImageAnnotation, "id">>(
        "/api/image/annotation/",
        data
    );

export const getViz1Term = () =>
    bind(findTerm)(fromNullable(query("viz1/term")));

type Interval = [number, number, number];

const sortInterval = (a: Interval, b: Interval) => a[0] - b[0];

const sortIntervals = (is: Interval[]) => is.sort(sortInterval);

const compileAnnotations = (content: string, rawIntervals: Interval[]) => {
    const start = 0;
    const end = content.length;
    const intervals = [
        ...new Set(
            sortIntervals(rawIntervals).reduce(
                (acc, i) => acc.concat(i[0], i[1]),
                [start, end]
            )
        ),
    ].sort((a, b) => a - b);

    // const inInterval = (left, right) =>
    //     rawIntervals.filter((a) => a[0] < right && a[1] > left).length > 0;

    const findInterval = (left: number, right: number) =>
        rawIntervals.find((a) => a[0] < right && a[1] > left);

    const blocks = [];
    for (let i = 1; i < intervals.length; i += 1) {
        const int = findInterval(intervals[i - 1], intervals[i]);
        const isCoded = int !== undefined;
        const annotation = isCoded ? int[2] : null;
        blocks.push({
            start: intervals[i - 1],
            end: intervals[i],
            content: content.slice(intervals[i - 1], intervals[i]),
            annotation,
        });
    }
    return blocks;
};

const withIntervals = (elem: Element) => (data: string) => {
    const intervals = JSON.parse(data);
    // const content = elem.textContent ?? "";
    // const content = JSON.parse('"' + elem.getAttribute('data-content') + '"')
    const content = pipe2(
        fromNullable(elem.getAttribute("data-content")),
        map((c) => JSON.parse(`"${c}"`)),
        orElse(elem.textContent ?? "")
    );
    const blocks = compileAnnotations(content, intervals);
    emptyElement(elem);
    blocks.forEach((block) => {
        const span = document.createElement("span");
        const text = document.createTextNode(block.content);
        if (block.annotation === null) {
            span.setAttribute("class", "block");
        } else {
            span.setAttribute("class", "block code");
            span.setAttribute("data-annotation", block.annotation.toString());
        }
        span.appendChild(text);
        elem.appendChild(span);
        /* 
        if (block.code !== null) {
            const illu = document.createElement('img')
            illu.setAttribute('class', 'term-illu')
            illu.setAttribute('src', termIlluURL(block.code))
            elem.appendChild(illu)
        } 
        */
    });
};

export const highlightCode = (elem: Element) => {
    map(withIntervals(elem))(fromNullable(elem.getAttribute("data-intervals")));
};
