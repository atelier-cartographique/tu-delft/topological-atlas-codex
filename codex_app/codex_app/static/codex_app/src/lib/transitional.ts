import { fromNullable } from "./option.js";
import { postIO } from "./remote.js";
import { query, update } from "./state.js";

export type TransitionalText = {
    id: number;
    content: string;
};

export const findTransitionalText = (id: number) =>
    fromNullable(query("transitionals").find((t) => t.id === id));

export const createTransitional = (content: string) =>
    postIO<TransitionalText, Omit<TransitionalText, "id">>(
        "/api/transitional/create/",
        { content }
    ).then((tt) => {
        update("transitionals", (old) =>
            old.filter(({ id }) => tt.id !== id).concat(tt)
        );
        return tt;
    });
