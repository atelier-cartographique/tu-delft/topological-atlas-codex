import { first } from "../lib/array.js";
import { findCategory } from "../lib/category.js";
import { attrs, emptyElement, events } from "../lib/dom.js";
import { DIV, SPAN } from "../lib/html.js";
import {
    CodexImage,
    findImage,
    getCurrentImage,
    ImageAnnotation,
    setImage,
} from "../lib/image.js";
import {
    bind,
    fromNullable,
    map,
    orElse,
    pipe2,
    toNullable,
} from "../lib/option.js";
import { ParagraphAnnotation } from "../lib/paragraph.js";
import { findImageInPlace, findPlaceByName, Place } from "../lib/place.js";
import { assign, query } from "../lib/state.js";
import { findTerm, Term, TermLocationUUID } from "../lib/term.js";

let inited = false;
const init = (root: HTMLElement) => {
    if (inited) {
        return;
    }
    inited = true;

    root.addEventListener("click", () => assign("show/par", false));
};

const ACTIVE_CLASS = "active";

const getImageForTerm = (term: Term, cid: number) => {
    let annot = query("annotations/image").find(
        (a) => a.tag === "term" && a.termId === term.id && cid !== a.imageId
    );
    // if (annot === undefined) {
    //     annot = query("annotations/image").find(
    //         (a) =>
    //             a.tag === "category" &&
    //             cid !== a.imageId &&
    //             term.categories.indexOf(a.categoryId) >= 0
    //     );
    // }
    return bind(({ imageId }: ImageAnnotation) => findImage(imageId))(
        fromNullable(annot)
    );
};

const withImageTarget = (element: HTMLElement) =>
    map((i: CodexImage) =>
        attrs(
            events(element, (add) => add("click", () => setImage(i))),
            (set) => set("class", "location")
        )
    );

const renderContent = (content: string, term: Term) => {
    let element = SPAN("", content);
    if (term.uuid === TermLocationUUID) {
        element = orElse(element)(
            withImageTarget(element)(
                bind(findImageInPlace)(findPlaceByName(content))
            )
        );
    }
    return element;
};

export const renderPar = (root: HTMLElement) => {
    init(root);
    emptyElement(root);
    root.classList.remove(ACTIVE_CLASS);
    const currentImage = toNullable(getCurrentImage());
    if (query("show/par") && currentImage !== null) {
        root.classList.add(ACTIVE_CLASS);
        const termIds = query("highlight/terms");

        const renderAnnotation = map((annot: ParagraphAnnotation) => {
            const renderOne = (
                prefix: string,
                { termId, content }: ParagraphAnnotation
            ) =>
                map((term: Term) =>
                    DIV(
                        `${prefix}-annot`,
                        events(DIV(`${prefix}-term`, term.name), (add) => {
                            add("click", () =>
                                map(setImage)(
                                    getImageForTerm(term, currentImage.id)
                                )
                            );
                        }),
                        DIV(`${prefix}-content`, renderContent(content, term))
                    )
                )(findTerm(termId));

            root.appendChild(
                orElse(DIV("term-not-found"))(renderOne("main", annot))
            );
            query("annotations/paragraph")
                .filter(
                    ({ parId, id }) => parId === annot.parId && id !== annot.id
                )
                .forEach((a) =>
                    root.appendChild(
                        orElse(DIV("term-not-found"))(renderOne("proximity", a))
                    )
                );
        });

        renderAnnotation(
            first(
                query("annotations/paragraph")
                    .filter(({ termId }) => termIds.indexOf(termId) >= 0)
                    .sort((a, b) => b.content.length - a.content.length)
            )
        );
    }
};
