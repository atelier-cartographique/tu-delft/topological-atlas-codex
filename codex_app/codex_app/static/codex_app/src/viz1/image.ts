import { findCategory } from "../lib/category.js";
import {
    getAnnotationsForImage,
    getCurrentImage,
    CodexImage,
    getGridScale,
    GRID_SIZE,
    ImageAnnotation,
    NumberedOps,
    Row,
    TRIANGLE_SIDE,
    selectedOpsRow,
    getPreviousImage,
    GRID_HEIGHT,
    GRID_WIDTH,
} from "../lib/image.js";
import { Language } from "../lib/lang.js";
import { eventTerm, logEvent } from "../lib/log-event.js";
import { alt, fromNullable, map, orElse, pipe2 } from "../lib/option.js";
import { assign } from "../lib/state.js";
import { findTerm, getTermsInCategory } from "../lib/term.js";
import { navigateTerm } from "./route.js";

type Size = { width: number; height: number };

type Point = { x: number; y: number };
const point = (x: number, y: number): Point => ({ x, y });

const mulS = (n: number, { x, y }: Point) => ({ x: x * n, y: y * n });
const divS = (n: number, { x, y }: Point) => ({ x: x / n, y: y / n });
const add = (p1: Point, p2: Point) => ({ x: p1.x + p2.x, y: p1.y + p2.y });

type Triangle = [Point, Point, Point];

const isTriangle = (ns: Point[]): ns is Triangle => ns.length === 3;

type HitZone = {
    triangles: Triangle[];
    annotation: ImageAnnotation;
};

let inited = false;

const canvas = {
    back: document.createElement("canvas"),
    middle: document.createElement("canvas"),
    fore: document.createElement("canvas"),
};

const context: { [k in CanvasKey]: CanvasRenderingContext2D | null } = {
    back: null,
    middle: null,
    fore: null,
};

type CanvasKey = keyof typeof canvas;

const getContext = (key: CanvasKey) => {
    let ctx = context[key];
    while (ctx === null) {
        ctx = canvas[key].getContext("2d");
        context[key] = ctx;
    }
    return ctx;
};

const initCanvas = (root: HTMLElement, key: CanvasKey) => {
    const { width, height } = root.getBoundingClientRect();
    canvas[key].width = width * 2;
    canvas[key].height = height * 2;
    canvas[key].setAttribute("class", `canvas-${key}`);
    root.appendChild(canvas[key]);
};

const getSize = (): Size => ({
    width: canvas.fore.width,
    height: canvas.fore.height,
});

type Line = [Point, Point];
const line = (start: Point, end: Point): Line => [start, end];

const pointToStr = ({ x, y }: Point) => `(${x}; ${y})`;
const lineToStr = ([start, end]: Line) =>
    `[${pointToStr(start)}, ${pointToStr(end)}]`;

const lineSet = () => {
    const _values: Line[] = [];
    const _rest: Line[] = [];

    const eqPoint = (a: Point, b: Point) =>
        Math.abs(a.x - b.x) < 0.1 && Math.abs(a.y - b.y) < 0.1;

    const eq = (l0: Line, l1: Line) =>
        (eqPoint(l0[0], l1[0]) && eqPoint(l0[1], l1[1])) ||
        (eqPoint(l0[0], l1[1]) && eqPoint(l0[1], l1[0]));

    const indexOf = (li: Line) => _values.findIndex((l) => eq(li, l));
    const has = (li: Line) => indexOf(li) >= 0;

    const put = (li: Line) => {
        const index = indexOf(li);
        if (index < 0) {
            _values.push(li);
            // console.log("put>", lineToStr(li));
        } else {
            _values.splice(index, 1);
            _rest.push(li);
            // console.log("no>", lineToStr(li));
        }
    };

    const values = () => _values;
    const rest = () => _rest;

    return {
        has,
        put,
        values,
        rest,
    };
};

const mergeTriangles = (ts: Triangle[]) => {
    const lines = lineSet();
    ts.forEach(([p1, p2, p3]) => {
        lines.put(line(p1, p2));
        lines.put(line(p2, p3));
        lines.put(line(p3, p1));
    });

    return lines;
};

const highlight = (hit: HitZone) => {
    const ctx = getContext("fore");
    const { width, height } = getSize();
    const set = mergeTriangles(hit.triangles);
    ctx.clearRect(0, 0, width, height);
    ctx.save();
    ctx.beginPath();
    ctx.strokeStyle = "white";
    ctx.lineWidth = 3;
    ctx.lineCap = "round";
    set.values().forEach(([start, end]) => {
        ctx.moveTo(start.x, start.y);
        ctx.lineTo(end.x, end.y);
    });
    ctx.stroke();
    // debug
    ctx.beginPath();
    ctx.strokeStyle = "#8dcaff";
    ctx.lineWidth = 1;
    ctx.lineCap = "round";
    set.rest().forEach(([start, end]) => {
        ctx.moveTo(start.x, start.y);
        ctx.lineTo(end.x, end.y);
    });
    ctx.stroke();
    ctx.restore();
};

const init = (root: HTMLElement) => {
    if (root.childElementCount === 0) {
        inited = true;
        initCanvas(root, "back");
        initCanvas(root, "middle");
        initCanvas(root, "fore");

        const back = getContext("back");
        back.fillRect(0, 0, canvas.back.width, canvas.back.height);

        const middle = getContext("middle");
        middle.clearRect(0, 0, canvas.middle.width, canvas.middle.height);

        const fore = getContext("fore");
        fore.clearRect(0, 0, canvas.fore.width, canvas.fore.height);

        let annotId: null | number = null;
        let cleared = false;

        canvas.fore.addEventListener("click", ({ offsetX, offsetY }) => {
            map((hit: HitZone) => {
                navigateTerm(hit.annotation.termId, true);
            })(testZones(offsetX, offsetY));
        });

        canvas.fore.addEventListener("mousemove", (e) => {
            const x = e.offsetX;
            const y = e.offsetY;
            pipe2(
                testZones(x, y),
                map((hit: HitZone) => {
                    if (annotId !== hit.annotation.id) {
                        // console.log("click", hit.annotation);
                        // const annot = hit.annotation;
                        // annotId = annot.id;
                        // cleared = false;
                        highlight(hit);
                        // return assign("highlight/terms", [annot.termId]);
                    }
                }),
                alt(() => {
                    if (!cleared) {
                        cleared = true;
                        annotId = null;
                        // assign("highlight/terms", []);
                        getContext("fore").clearRect(
                            0,
                            0,
                            canvas.fore.width,
                            canvas.fore.height
                        );
                    }
                })
            );
        });
        // canvas.fore.addEventListener("click", (e) => {
        //     const x = e.offsetX;
        //     const y = e.offsetY;

        //     map((hit: HitZone) => {
        //         console.log("click", hit.annotation);
        //         highlight(hit);
        //     })(testZones(x, y));
        // });
    }
};

const sign = ([p1, p2, p3]: Triangle) => {
    return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y) > 0;
};

const inTriangle =
    (pt: Point) =>
    ([p1, p2, p3]: Triangle) => {
        const d1 = sign([pt, p1, p2]);
        const d2 = sign([pt, p2, p3]);
        const d3 = sign([pt, p3, p1]);

        const result = (d1 && d2 && d3) || !(d1 || d2 || d3);
        // if (result) {
        //     console.log("HIT", pt);
        //     console.log("triangle", p1, p2, p3);
        //     console.log("signs", d1, d2, d3);
        // }
        return result;
    };

const centroid = ([p1, p2, p3]: Triangle): Point => ({
    x: (p1.x + p2.x + p3.x) / 3,
    y: (p1.y + p2.y + p3.y) / 3,
});

const gravity = (ts: Triangle[]) =>
    divS(
        ts.length,
        ts.map(centroid).reduce((acc, p) => add(acc, p))
    );

export const setClipping = (
    ctx: CanvasRenderingContext2D,
    annotations: ImageAnnotation[],
    gridXScale: number,
    gridYScale: number
) => {
    ctx.beginPath();
    const clippingRow = (y: number, startIndex: number) => {
        let r: Row<NumberedOps> = selectedOpsRow(y);
        for (let i = 0; i < GRID_SIZE; i += 1) {
            r = r.fn(startIndex + i, r.acc, r.x, r.y);
        }
        return r.acc;
    };

    const zones = annotations.map<HitZone>((annotation) => {
        const triangles: Triangle[] = [];

        for (let i = 0; i < GRID_SIZE; i += 1) {
            clippingRow(i * TRIANGLE_SIDE, i * GRID_SIZE).forEach(
                ({ n, ops }) => {
                    if (annotation.triangles.indexOf(n) >= 0) {
                        let points: Point[] = [];
                        for (const op of ops) {
                            switch (op.tag) {
                                case "M":
                                    ctx.moveTo(
                                        gridXScale * op.x,
                                        gridYScale * op.y
                                    );
                                    points.push(
                                        point(
                                            gridXScale * op.x,
                                            gridYScale * op.y
                                        )
                                    );
                                    break;
                                case "L":
                                    ctx.lineTo(
                                        gridXScale * op.x,
                                        gridYScale * op.y
                                    );
                                    points.push(
                                        point(
                                            gridXScale * op.x,
                                            gridYScale * op.y
                                        )
                                    );
                                    break;
                                case "Z":
                                    ctx.closePath();
                                    break;
                            }
                        }
                        const mat = ctx.getTransform();
                        const original = points.map((p) => {
                            const orig = mat.transformPoint(p);
                            return {
                                x: orig.x,
                                y: orig.y,
                            };
                        });
                        if (isTriangle(original)) {
                            triangles.push(original);
                        }
                    }
                }
            );
        }
        return { triangles, annotation };
    });
    // ctx.lineWidth = 6;
    // ctx.strokeStyle = "white";
    // ctx.fillStyle = "white";
    // ctx.fill();
    if (zones.length > 0) {
        ctx.clip();
    }
    return zones;
};

const catName = (cid: number) =>
    pipe2(
        findCategory(cid),
        map(({ name }) => name),
        orElse("~~~~")
    );

const termName = (tid: number) =>
    pipe2(
        findTerm(tid),
        map(({ name }) => name),
        orElse("~~~~")
    );

const DEFAULT_LANG: Language = "en";
export const termLang = (tid: number): Language =>
    pipe2(
        findTerm(tid),
        map(({ lang }) => lang),
        orElse<Language>(DEFAULT_LANG)
    );

export const termFont = (tid: number) => {
    switch (termLang(tid)) {
        case "ur":
            return "'noto'";
        default:
            return "'unbounded'";
    }
};

const catLang = (cid: number): Language =>
    pipe2(
        findCategory(cid),
        map(({ lang }) => lang),
        orElse<Language>(DEFAULT_LANG)
    );

const catFont = (cid: number) => {
    switch (catLang(cid)) {
        case "ur":
            return "'noto'";
        default:
            return "'unbounded'";
    }
};

export const label = (
    ctx: CanvasRenderingContext2D,
    annotations: ImageAnnotation[],
    gridXScale: number,
    gridYScale: number
) => {
    const clippingRow = (y: number, startIndex: number) => {
        let r: Row<NumberedOps> = selectedOpsRow(y);
        for (let i = 0; i < GRID_SIZE; i += 1) {
            r = r.fn(startIndex + i, r.acc, r.x, r.y);
        }
        return r.acc;
    };

    ctx.fillStyle = "rgba(255,255,255, 1)";
    //   ctx.fillStyle = "rgba(255,255,255, 0.9)";
    ctx.strokeStyle = "black";
    ctx.lineWidth = 2;

    annotations.map((annotation) => {
        const triangles: Triangle[] = [];

        for (let i = 0; i < GRID_SIZE; i += 1) {
            clippingRow(i * TRIANGLE_SIDE, i * GRID_SIZE).forEach(
                ({ n, ops }) => {
                    if (annotation.triangles.indexOf(n) >= 0) {
                        let points: Point[] = [];
                        for (const op of ops) {
                            switch (op.tag) {
                                case "M":
                                    points.push(
                                        point(
                                            gridXScale * op.x,
                                            gridYScale * op.y
                                        )
                                    );
                                    break;
                                case "L":
                                    points.push(
                                        point(
                                            gridXScale * op.x,
                                            gridYScale * op.y
                                        )
                                    );
                                    break;
                                case "Z":
                                    break;
                            }
                        }
                        if (isTriangle(points)) {
                            triangles.push(points);
                        }
                    }
                }
            );
        }
        if (triangles.length > 0) {
            const { x, y } = gravity(triangles);
            const name = termName(annotation.termId);
            ctx.font = `bold 200px ${termFont(annotation.termId)}`;

            const met = ctx.measureText(name);
            //   ctx.strokeText(
            //     name,
            //     x - met.width / 2,
            //     y + met.actualBoundingBoxAscent / 2
            //   );
            ctx.fillText(
                name,
                x - met.width / 2,
                y + met.actualBoundingBoxAscent / 2
            );
            // ctx.fillText(
            //     name,
            //     x - met.width / 2,
            //     y + met.actualBoundingBoxAscent / 2
            // );
        }
    });

    ctx.restore();
};

export const label2 = (
    ctx: CanvasRenderingContext2D,
    annotations: ImageAnnotation[],
    gridXScale: number,
    gridYScale: number
) => {
    const clippingRow = (y: number, startIndex: number) => {
        let r: Row<NumberedOps> = selectedOpsRow(y);
        for (let i = 0; i < GRID_SIZE; i += 1) {
            r = r.fn(startIndex + i, r.acc, r.x, r.y);
        }
        return r.acc;
    };
    ctx.save();
    ctx.font = "bold 130px sans";
    ctx.fillStyle = "white";
    annotations.map((annot) => {
        let minX = Number.MAX_VALUE;
        let minY = Number.MAX_VALUE;
        let maxX = Number.MIN_VALUE;
        let maxY = Number.MIN_VALUE;
        const updateExtent = (x: number, y: number) => {
            minX = Math.min(minX, x);
            minY = Math.min(minY, y);
            maxX = Math.max(maxX, x);
            maxY = Math.max(maxY, y);
        };
        for (let i = 0; i < GRID_SIZE; i += 1) {
            clippingRow(i * TRIANGLE_SIDE, i * GRID_SIZE).forEach(
                ({ n, ops }) => {
                    if (annot.triangles.indexOf(n) >= 0) {
                        for (const op of ops) {
                            switch (op.tag) {
                                case "M":
                                    // ctx.moveTo(gridXScale * op.x, gridYScale * op.y);
                                    updateExtent(
                                        gridXScale * op.x,
                                        gridYScale * op.y
                                    );
                                    break;
                                case "L":
                                    // ctx.lineTo(gridXScale * op.x, gridYScale * op.y);
                                    updateExtent(
                                        gridXScale * op.x,
                                        gridYScale * op.y
                                    );
                                    break;
                                case "Z":
                                    // ctx.closePath();
                                    break;
                            }
                        }
                    }
                }
            );
        }

        const dx = maxX - minX;
        const dy = maxY - minY;
        const centerX = minX + dx / 2;
        const centerY = minY + dy / 2;
        const name = termName(annot.termId);

        ctx.fillText(name, centerX, centerY);
    });

    ctx.restore();
};

const { clearZones, pushZones, testZones } = (() => {
    let zones: HitZone[] = [];

    const clearZones = () => (zones = []);

    const pushZones = (zs: HitZone[]) => (zones = zs);

    const testZones = (x: number, y: number) =>
        fromNullable(
            zones.find(
                ({ triangles }) =>
                    triangles.findIndex(inTriangle(mulS(2, point(x, y)))) >= 0
            )
        );

    return { clearZones, pushZones, testZones };
})();

const getCenteringMatrix = (
    width: number,
    height: number,
    imgWidth: number,
    imgHeight: number
) => {
    const scale = imgWidth > imgHeight ? width / imgWidth : height / imgHeight;
    const { tx, ty } = (() => {
        if (imgWidth > imgHeight) {
            return {
                tx: 0,
                ty: (height - imgHeight * scale) / 2,
            };
        }
        return {
            tx: (width - imgWidth * scale) / 2,
            ty: 0,
        };
    })();
    const base = new DOMMatrix();
    const translated = base.translate(tx, ty);
    const scaled = translated.scale(scale, scale);
    return scaled;
};

const FULLGRID_ANNOTATION: ImageAnnotation = {
    tag: "term",
    id: -1,
    imageId: -1,
    termId: -1,
    triangles: new Array(GRID_HEIGHT * GRID_WIDTH).fill(0).map((_, i) => i),
};

const gridify = (ctx: CanvasRenderingContext2D, triangles: Triangle[]) => {
    const set = mergeTriangles(triangles);
    ctx.save();
    ctx.beginPath();
    ctx.strokeStyle = "white";
    ctx.lineWidth = 3;
    ctx.lineCap = "round";
    set.values().forEach(([start, end]) => {
        ctx.moveTo(start.x, start.y);
        ctx.lineTo(end.x, end.y);
    });
    ctx.stroke();
    // debug
    ctx.beginPath();
    ctx.strokeStyle = "#8dcaff";
    ctx.lineWidth = 1;
    ctx.lineCap = "round";
    set.rest().forEach(([start, end]) => {
        ctx.moveTo(start.x, start.y);
        ctx.lineTo(end.x, end.y);
    });
    ctx.stroke();
    ctx.restore();
};

const getTriangles = (
    annotation: ImageAnnotation,
    gridXScale: number,
    gridYScale: number
) => {
    const clippingRow = (y: number, startIndex: number) => {
        let r: Row<NumberedOps> = selectedOpsRow(y);
        for (let i = 0; i < GRID_SIZE; i += 1) {
            r = r.fn(startIndex + i, r.acc, r.x, r.y);
        }
        return r.acc;
    };

    const triangles: Triangle[] = [];

    for (let i = 0; i < GRID_SIZE; i += 1) {
        clippingRow(i * TRIANGLE_SIDE, i * GRID_SIZE).forEach(({ n, ops }) => {
            if (annotation.triangles.indexOf(n) >= 0) {
                let points: Point[] = [];
                for (const op of ops) {
                    switch (op.tag) {
                        case "M":
                            points.push(
                                point(gridXScale * op.x, gridYScale * op.y)
                            );
                            break;
                        case "L":
                            points.push(
                                point(gridXScale * op.x, gridYScale * op.y)
                            );
                            break;
                        case "Z":
                            break;
                    }
                }
                if (isTriangle(points)) {
                    triangles.push(points);
                }
            }
        });
    }
    return triangles;
};

const withPreviousImage = (i: CodexImage) => {
    const ctx = getContext("middle");
    const { width, height } = getSize();
    ctx.save();
    const cm = getCenteringMatrix(width, height, i.file.width, i.file.height);
    ctx.setTransform(cm);
    const { xScale, yScale } = getGridScale(i.file.width, i.file.height);
    gridify(ctx, getTriangles(FULLGRID_ANNOTATION, xScale, yScale));
    ctx.restore();
};

const withImage = (i: CodexImage) => {
    // ctx.fillText(`:loading: ${i.name}`, 100, 100);
    const ctx = getContext("middle");
    const { width, height } = getSize();
    const img = new Image(i.file.width, i.file.height);
    clearZones();
    img.addEventListener(
        "load",
        () => {
            ctx.save();
            ctx.fillStyle = "rgba(0,0,0,0.4)";
            ctx.fillRect(0, 0, width, height);
            // ctx.drawImage(img, 0, 0);
            // ctx.fillStyle = "rgba(255,255,255,0.8)";
            // ctx.fillRect(0, 0, i.file.width, i.file.height);
            const cm = getCenteringMatrix(
                width,
                height,
                i.file.width,
                i.file.height
            );
            ctx.setTransform(cm);
            const annotations = getAnnotationsForImage(i.id);
            // if (annotations.length > 0) {
            const { xScale, yScale } = getGridScale(
                i.file.width,
                i.file.height
            );
            ctx.save();
            pushZones(setClipping(ctx, annotations, xScale, yScale));
            ctx.drawImage(img, 0, 0);
            ctx.restore();
            label(ctx, annotations, xScale, yScale);
            // }
            // ctx.fillStyle = "white";
            // for (let y = 0; y < height; y += 100) {
            //     ctx.fillText(y.toString(), 20, y);
            // }
            ctx.restore();
        },
        false
    );
    img.src = i.file.url;
};

const withoutImage = () => {
    const ctx = getContext("back");
    ctx.fillStyle = "grey";
    ctx.font = "bold 18px sans";
    ctx.fillText("missing image", 100, 100);
};

export const renderImage = (root: HTMLElement | null) => {
    if (root) {
        init(root);
        const { width, height } = getSize();
        getContext("fore").clearRect(0, 0, width, height);
        // map(withPreviousImage)(getPreviousImage());
        pipe2(getCurrentImage(), map(withImage), alt(withoutImage));
    }
};

export default renderImage;
