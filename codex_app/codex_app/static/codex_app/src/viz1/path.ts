import { emptyElement, events } from "../lib/dom.js";
import { ANCHOR, BaseNode, CANVAS, DIV, IMG, SPAN } from "../lib/html.js";
import {
  ImageAnnotation,
  CodexImage,
  renderImageAnnotation,
  findImage,
  findImageAnnotation,
  getCurrentImage,
  getAnnotationsForImage,
  setImage,
} from "../lib/image.js";
import {
  alt,
  bind,
  fromNullable,
  map,
  map2,
  orElse,
  orElseL,
  pipe2,
} from "../lib/option.js";
import { ParagraphAnnotation, findParAnnotation } from "../lib/paragraph.js";
import {
  currentNode,
  NodeImage,
  NodeImageAnnotation,
  NodeTerm,
  NodeTextAnnotation,
  NodeTransitionalText,
  Path,
  PathCursor,
  PathNode,
} from "../lib/path.js";
import { TransitionalText, findTransitionalText } from "../lib/transitional.js";
import { assign, query } from "../lib/state.js";
import { Term, findTerm, getViz1Term } from "../lib/term.js";
import { eventTerm, logEvent } from "../lib/log-event.js";
import { navigateHome } from "./route.js";

declare function termIlluURL(tid: string): string;

const selectPath = (pc: PathCursor) => assign("viz1/path", pc);

const incWrap = (i: number, len: number) => (i < len - 1 ? i + 1 : 0);

export const forward = (pc: PathCursor) =>
  assign("viz1/path", { ...pc, index: incWrap(pc.index, pc.nodes.length) });

export const setCurrentPath = (pc: Path, index: number) =>
  assign("viz1/path", { ...pc, index });

export const resetPath = () => assign("viz1/path", null);

export const getSelectPath = () => fromNullable(query("viz1/path"));

export const getCurrentNode = () => bind(currentNode)(getSelectPath());

const union = <T>(a: Set<T>, b: Set<T>) => {
  const u = new Set(a);
  b.forEach((v) => u.add(v));
  return u;
};

const termInPath = (tids: number[]) => (p: Path) => {
  for (const n of p.nodes) {
    if (n.type === "te" && tids.indexOf(n.tid) >= 0) {
      return true;
    }
  }
  return false;
};

const getImageForTerm = (tid: number) => {
  const annot = query("annotations/image").find(
    (a) => a.tag === "term" && a.termId === tid
  );
  return bind(({ imageId }: ImageAnnotation) => findImage(imageId))(
    fromNullable(annot)
  );
};

const renderNodeTerm = ({ tid }: NodeTerm) => {
  // const a = ANCHOR("", `/viz0/term/${tid}.html`);
  const className = "path-node term";
  const node = DIV(className);
  map(({ uuid, name, statement }: Term) => {
    node.append(
      // events(
      DIV(
        "_x",
        DIV(
          "term-head",
          DIV("", IMG("term-picto", termIlluURL(uuid))),
          SPAN("term-name", name)
        ),
        DIV("description", statement)
      )
      // (add) => add("click", (e) =>
      // 		map((image: CodexImage) => {
      // 			logEvent(eventTerm(tid));
      // 			setImage(image);
      // 		})(getImageForTerm(tid)),
      // 	),
      // ),
    );
  })(findTerm(tid));
  return node;
};

const renderNodeImage = (node: NodeImage) => DIV("path-node");

const renderNodeTextNote = (node: NodeTextAnnotation) =>
  DIV(
    "path-node par-annot",
    DIV(
      "window",
      map((a: ParagraphAnnotation) => a.content)(findParAnnotation(node.tid))
    )
  );

const withImageAnnotation = (annotation: ImageAnnotation) => {
  const width = 500;
  const height = 500;

  const render = map((i: CodexImage) => {
    const canvas = CANVAS("image-annotation", width, height);
    const ctx = canvas.getContext("2d");
    if (ctx !== null) {
      renderImageAnnotation(i, ctx, annotation, [width, height]);
    }
    return canvas;
  });

  return orElse<BaseNode>(DIV("err", "error when loading image"))(
    render(findImage(annotation.imageId))
  );
};

const renderNodeImageNote = (node: NodeImageAnnotation) =>
  DIV(
    "path-node image-annot",
    map(withImageAnnotation)(findImageAnnotation(node.tid))
  );

const renderNodetransitionalText = (node: NodeTransitionalText) =>
  DIV(
    "path-node par-text",
    map((a: TransitionalText) => a.content)(findTransitionalText(node.tid))
  );

const renderNode = (node: PathNode) => {
  switch (node.type) {
    case "te":
      return renderNodeTerm(node);
    case "im":
      return renderNodeImage(node);
    case "ta":
      return renderNodeTextNote(node);
    case "ia":
      return renderNodeImageNote(node);
    case "tt":
      return renderNodetransitionalText(node);
  }
};

const renderPathNavLink = (pc: PathCursor) =>
  events(
    DIV(
      "path",
      DIV("path-name", pc.name ?? pc.id)
      // renderNode(pc.nodes[pc.index])
    ),
    (on) => on("click", () => selectPath(pc))
  );

const termInPath2 = (tid: number, p: Path) => {
  for (let i = 0; i < p.nodes.length; i++) {
    const node = p.nodes[i];
    if (node.type === "te" && tid === node.tid) {
      return i;
    }
  }
  return -1;
};

const withCurrentPath = map((path: PathCursor) =>
  DIV(
    "path-cursor",
    renderNode(path.nodes[path.index]),
    events(DIV("cursor", "⤬"), (on) => on("click", resetPath)),
    events(DIV("cursor", "↬"), (on) => on("click", () => forward(path)))
  )
);

const renderBackIndex = () =>
  DIV(
    "to-index",
    events(DIV("label", "◢ map of paths"), (on) => on("click", navigateHome))
  );

const withCurrentPath2 = map((path: PathCursor) =>
  DIV(
    "path-x",
    renderBackIndex(),
    events(DIV("cursor", "⤬"), (on) => on("click", resetPath)),
    DIV("path-title", path.name || "-"),
    ...path.nodes.map(renderNode)
  )
);

export const renderPathNav = (root: HTMLElement) => {
  emptyElement(root);

  const allPaths = query("paths");
  const selectedPaths: PathCursor[] = [];
  const withImage = map((img: CodexImage) => {
    const annotations = getAnnotationsForImage(img.id);
    annotations.forEach((a) => {
      allPaths.forEach((path) => {
        const index = termInPath2(a.termId, path);
        if (index >= 0) {
          // renderPath(root, path, idx);
          // paths.splice(pi, 1);
          selectedPaths.push({ ...path, index });
        }
      });
    });
  });

  const withTerm = map((term: Term) => {
    allPaths.forEach((path) => {
      const index = termInPath2(term.id, path);
      if (index >= 0) {
        selectedPaths.push({ ...path, index });
      }
    });
  });

  withImage(getCurrentImage());
  withTerm(getViz1Term());

  const element = pipe2(
    getSelectPath(),
    withCurrentPath2,
    orElseL(() =>
      DIV("xxx", renderBackIndex(), ...selectedPaths.map(renderPathNavLink))
    )
  );

  root.append(element);
};
