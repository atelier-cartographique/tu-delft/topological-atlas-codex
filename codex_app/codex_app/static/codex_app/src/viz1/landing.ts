import { attrs, emptyElement, events } from "../lib/dom.js";
import { BaseNode, CANVAS, DIV, IMG, SPAN } from "../lib/html.js";
import { Eq } from "../lib/eq";
import {
    ImageAnnotation,
    CodexImage,
    renderImageAnnotation,
    findImage,
    findImageAnnotation,
    renderImageAnnotationInExtent,
    renderImageAnnotationInExtentFull,
} from "../lib/image.js";
import {
    alt,
    bind,
    fromNullable,
    map,
    map2,
    orElse,
    pipe2,
    some,
} from "../lib/option.js";
import { ParagraphAnnotation, findParAnnotation } from "../lib/paragraph.js";
import {
    NodeTerm,
    NodeImage,
    NodeTextAnnotation,
    NodeImageAnnotation,
    NodeTransitionalText,
    PathNode,
    getPaths,
    Path,
} from "../lib/path.js";
import { Term, findTerm } from "../lib/term.js";
import { TransitionalText, findTransitionalText } from "../lib/transitional.js";
import { fst, mapFirst, mapSecond, snd, tuple, Tuple } from "../lib/tuple.js";
import { iife } from "../lib/util.js";
import { navigateImage, navigateTerm } from "./route.js";
import { drawCurve, pointsToCurve } from "../lib/smooth-curve.js";
import { findPlace, getImageLocation, Place } from "../lib/place.js";
import { assign, query } from "../lib/state.js";
import { termFont } from "./image.js";

const px = (n: number) => `${n}px`;

type PathNodeRect = PathNode & {
    left: number;
    right: number;
    bottom: number;
    top: number;
};

const wrapDraw = (ctx: CanvasRenderingContext2D, f: () => void) => {
    ctx.save();
    f();
    ctx.restore();
};

export const renderLanding = (root: HTMLElement) => {
    emptyElement(root);
    // root.append();

    const view = DIV("view");
    root.append(view);
    const without = (path: Path, paths: Path[]) =>
        paths.filter(({ id }) => path.id !== id);

    let paths = getPaths();
    let rect = { left: 1000, right: 1000, bottom: 120, top: 120 };
    let dir: "left" | "right" | "up" | "down" = "right";

    const posNode = (node: PathNode) => {
        const elem = renderNode(node);
        root.append(elem);
        const { width, height } = elem.getBoundingClientRect();
        const { x, y } = iife(() => {
            switch (dir) {
                case "right":
                    return { x: rect.right, y: rect.top };
                case "left":
                    return { x: rect.left - width, y: rect.top };
                case "up":
                    return { x: rect.left, y: rect.top - height };
                case "down":
                    return { x: rect.left, y: rect.bottom };
            }
        });
        elem.style.left = px(x);
        elem.style.top = px(y);
        const { left, right, bottom, top } = elem.getBoundingClientRect();
        rect = { left, right, bottom, top };
        return elem;
    };

    const splitAt = (i: number, path: Path): Tuple<Path, Path> => {
        const leftNodes = path.nodes.slice(0, i);
        const rightNodes = path.nodes.slice(i + 1);
        return tuple(
            { ...path, nodes: leftNodes.reverse() },
            { ...path, nodes: rightNodes }
        );
    };

    const processNode =
        (path: Path, paths: Path[], depth: number) => (node: PathNode) => {
            if (depth > 3) {
                return;
            }

            const nodeElement = events(
                attrs(posNode(node), (set) => set("data-path", path.id)),
                (on) =>
                    on("click", () => {
                        view.querySelectorAll(".path-node").forEach((e) =>
                            e.classList.remove("selected")
                        );
                        view.querySelectorAll(
                            `[data-path="${path.id}"]`
                        ).forEach((e) => e.classList.add("selected"));
                        view.querySelectorAll(
                            `[data-intersects="${path.id}"]`
                        ).forEach((e) => e.classList.add("selected"));
                    })
            );
            view.append(nodeElement);
            findIntersectingPaths(node, without(path, paths)).map((t) => {
                const splitIndex = snd(t);
                const path = fst(t);
                nodeElement.classList.add("intersection");
                attrs(nodeElement, (set) => set("data-intersects", path.id));
                const parts = splitAt(splitIndex, path);
                const doLeft = mapFirst((path: Path) =>
                    path.nodes.map(
                        processNode(path, without(path, paths), (depth += 1))
                    )
                );
                const doRight = mapSecond((path: Path) =>
                    path.nodes.map(
                        processNode(path, without(path, paths), (depth += 1))
                    )
                );
                dir = iife(() => {
                    switch (dir) {
                        case "left":
                        case "right":
                            return "up";
                        case "up":
                        case "down":
                            return "left";
                    }
                });
                doLeft(parts);
                dir = iife(() => {
                    switch (dir) {
                        case "left":
                        case "right":
                            return "down";
                        case "up":
                        case "down":
                            return "right";
                    }
                });
                doRight(parts);
            });
        };

    paths.map((path, i) => {
        // setTimeout(() => {
        path.nodes.map(processNode(path, paths, 0));
        // const alreadyProcessed: number[] = [];
        // if (alreadyProcessed.findIndex((id) => id === path.id) < 0) {
        //     path.nodes.map((node) => {

        //     });
        // }
        dir = iife(() => {
            switch (dir) {
                case "left":
                case "right":
                    return "down";
                case "up":
                case "down":
                    return "right";
            }
        });
        // }, i * 600);
    });
};
const deg2rad = (d: number) => (d * Math.PI) / 180;

const polar2cartesian = (
    R: number,
    theta: number,
    center: [number, number]
) => {
    const x = R * Math.cos(deg2rad(theta)) + center[0];
    const y = R * Math.sin(deg2rad(theta)) + center[1];
    return [x, y];
};

type Extent = [number, number, number, number];

const extentCenter = ([x1, y1, x2, y2]: Extent): [number, number] => [
    x1 + (x2 - x1) / 2,
    y1 + (y2 - y1) / 2,
];

const extentWidth = ([x1, y1, x2, y2]: Extent) => x2 - x1;

const extentHeight = ([x1, y1, x2, y2]: Extent) => y2 - y1;

const scaleExtent = (s: number, e: Extent): Extent => {
    const c = extentCenter(e);
    const w = (s * extentWidth(e)) / 2;
    const h = (s * extentHeight(e)) / 2;
    return [c[0] - w, c[1] - h, c[0] + w, c[1] + h];
};

const transformExtent = (t: DOMMatrix, e: Extent): Extent => {
    const min = t.transformPoint(new DOMPoint(e[0], e[1]));
    const max = t.transformPoint(new DOMPoint(e[2], e[3]));
    return [min.x, min.y, max.x, max.y];
};

const intersects = (
    [ax1, ay1, ax2, ay2]: Extent,
    [bx1, by1, bx2, by2]: Extent
) => {
    // console.log(
    //     `${bx1} >= ${ax1} && ${bx2} <= ${ax2} && ${by1} >= ${ay1} && ${by2} <= ${ay2}`
    // );
    // return bx1 >= ax1 && bx2 <= ax2 && by1 >= ay1 && by2 <= ay2;
    // ((X  ,Y),( A,    B)) and ((X1,  Y1),(A1,  B1))
    // [ax1, ay1, ax2, ay2]      [bx1, by1, bx2, by2]
    // A<X1 or A1<X or B<Y1 or B1<Y
    return !(ax2 < bx1 || bx2 < ax1 || ay2 < by1 || by2 < ay1);
};

// console.log(
//     "intersects([0,0,10,10] , [5,5,15,15])",
//     intersects([0, 0, 10, 10], [5, 5, 15, 15])
// );

// console.log(
//     "intersects([0,0,10,10] , [15,15, 20, 20])",
//     intersects([0, 0, 10, 10], [15, 15, 20, 20])
// );

const HS = 24;
const extentAt = (x: number, y: number): Extent => [
    x - HS,
    y - HS,
    x + HS,
    y + HS,
];

// export const renderLanding2 = (root: HTMLElement) => {
//     emptyElement(root);
//     root.style.position = "absolute";
//     root.style.top = "0";
//     root.style.right = "0";
//     root.style.bottom = "0";
//     root.style.left = "0";
//     const paths = getPaths();
//     const nodeScore = Array.from(
//         paths
//             .reduce((acc, path) => {
//                 path.nodes.forEach((node) => {
//                     map2(
//                         map((val: (PathNode & { parent: number })[]) =>
//                             acc.set(
//                                 node.tid,
//                                 val.concat({ ...node, parent: path.id })
//                             )
//                         ),
//                         alt(() =>
//                             acc.set(node.tid, [{ ...node, parent: path.id }])
//                         )
//                     )(fromNullable(acc.get(node.tid)));
//                 });
//                 return acc;
//             }, new Map<number, (PathNode & { parent: number })[]>())
//             .entries()
//     ).sort((a, b) => b[1].length - a[1].length);
//     const { width, height } = root.getBoundingClientRect();
//     const canvas = CANVAS("landing-inner", width, height);
//     const withContext = map((ctx: CanvasRenderingContext2D) => {
//         ctx.translate(width / 2, height / 2);
//         ctx.scale(12, 12);
//         ctx.fillStyle = "#ffc1075e";
//         ctx.strokeStyle = "blue";
//         ctx.lineWidth = 0.05;
//         const drawPoint = (x: number, y: number) => {
//             ctx.beginPath();
//             ctx.arc(x, y, 0.1, 0, deg2rad(360));
//             ctx.fill();
//         };
//         const drawExtent = ([x1, y1, x2, y2]: Extent) => {
//             ctx.beginPath();
//             ctx.rect(x1, y1, x2 - x1, y2 - y1);
//             ctx.fill();
//             ctx.stroke();
//         };
//         const frames: { tid: number; extent: Extent }[] = [];
//         const notIntersects = (e: Extent): boolean =>
//             frames.every((f) => !intersects(e, f.extent));

//         const nextSlot = (
//             tid: number,
//             r: number,
//             t: number
//         ): [number, number] => {
//             if (frames.length === 0) {
//                 const [x, y] = polar2cartesian(r, t, [0, 0]);
//                 const extent = extentAt(x, y);
//                 frames.push({ tid, extent });
//                 return [r, t];
//             }
//             let curR = r;
//             let curT = t;
//             for (let i = 0; i < 10000; i++) {
//                 const [x, y] = polar2cartesian(curR, curT, [0, 0]);
//                 const extent = extentAt(x, y);
//                 // if (!bboxEq.equals(extent, lastExtent)) {
//                 if (notIntersects(extent)) {
//                     break;
//                 }
//                 curR += 0.05;
//                 // curT += 10;
//                 curT += (Math.random() - 1) * 20;
//             }
//             const [x, y] = polar2cartesian(curR, curT, [0, 0]);
//             const extent = extentAt(x, y);
//             frames.push({ tid, extent });
//             return [curR, curT];
//         };
//         // nodeScore.map(([tid, nodes], idx) => {
//         //     // const [x, y] = polar2cartesian(curR, curT, [0, 0]);
//         //     // drawPoint(x, y);
//         //     // curR += 0.05;
//         //     // curT += (Math.random() - 1) * 20;
//         // });
//         nodeScore.reduce(([r, t], [tid, nodes]) => nextSlot(tid, r, t), [
//             0, 0,
//         ] as [number, number]);

//         const drawBase = () =>
//             frames.map(({ extent }) => extent).forEach(drawExtent);

//         const pathMap = paths.reduce((acc, path) => {
//             const extents = path.nodes.map(
//                 (node) => frames.find((f) => f.tid === node.tid)!.extent
//             );
//             path.nodes.forEach(({ tid }) => {
//                 const key = tid.toString();
//                 const val = {
//                     pid: path.id,
//                     extents,
//                 };
//                 if (!(key in acc)) {
//                     acc[key] = [val];
//                 } else {
//                     acc[key].push(val);
//                 }
//             });
//             return acc;
//         }, {} as Record<string, { pid: number; extents: Extent[] }[]>);

//         const clearCanvas = () => {
//             const t = ctx.getTransform();
//             ctx.resetTransform();
//             ctx.clearRect(0, 0, width, height);
//             ctx.setTransform(t);
//         };
//         const drawTid = (tid: number) => {
//             const pm = pathMap[tid.toString()];
//             if (pm) {
//                 clearCanvas();
//                 // drawBase();

//                 ctx.save();
//                 ctx.strokeStyle = "black";
//                 ctx.lineWidth = ctx.lineWidth * 10;

//                 pm.forEach(({ pid, extents }) => {
//                     ctx.beginPath();
//                     extents.map(extentCenter).map((c, i) => {
//                         if (i === 0) {
//                             ctx.moveTo(c[0], c[1]);
//                         } else {
//                             ctx.lineTo(c[0], c[1]);
//                         }
//                     });
//                     ctx.stroke();
//                 });
//                 ctx.restore();

//                 ctx.save();
//                 ctx.strokeStyle = "red";
//                 ctx.fillStyle = "#ffc107";
//                 ctx.lineWidth = ctx.lineWidth * 2;
//                 pm.forEach(({ pid, extents }) => {
//                     extents.map(drawExtent);
//                 });
//                 ctx.restore();
//                 ctx.save();
//                 ctx.fillStyle = "black";
//                 ctx.font = "1pt sans-serif";
//                 pm.forEach(({ pid, extents }) => {
//                     extents.map((c) => {
//                         ctx.fillText(pid.toString(), c[0], c[3]);
//                     });
//                 });
//                 ctx.restore();
//             } else {
//                 console.error("arrrrg", tid);
//             }
//         };

//         // const tids = nodeScore.map(([tid, _]) => tid);
//         // let ctid = -1;
//         // const nextTid = () => {
//         //     ctid += 1;
//         //     if (ctid > tids.length) {
//         //         ctid = 0;
//         //     }
//         //     return tids[ctid];
//         // };
//         // const it = window.setInterval(() => {
//         //     const tid = nextTid();
//         //     console.log("tid", tid, new Date().toLocaleTimeString());
//         //     drawTid(tid);
//         // }, 1000);

//         canvas.addEventListener("click", (e) => {
//             const { x, y } = ctx
//                 .getTransform()
//                 .inverse()
//                 .transformPoint({ x: e.clientX, y: e.clientY });
//             for (let i = 0; i < frames.length; i++) {
//                 const frame = frames[i];
//                 if (intersects(frame.extent, [x, y, x, y])) {
//                     console.log("Got tid", frame.tid, x, y);
//                     drawTid(frame.tid);
//                     return;
//                 }
//             }
//             clearCanvas();
//             drawBase();
//         });

//         drawBase();
//     });

//     withContext(fromNullable(canvas.getContext("2d")));
//     root.append(canvas);
// };

///////////////// LANDING 3

const colorDict = {
    "dark-turquoise": "#119a90",
    "grey-turquoise": "#74b4ad",
    pink: "#ffaee6",
    "old-pink": "#e38698",
    yellow: "#ffffc7",
    green: "#7fea9a",
    "grey-blue": "#7e98bb",
    turquoise: "#5df0e6",
    "dark-green": "#a5bbb3",
    "dark-pink": "#b43d5d",
    "light-turquoise": "#cbeae4",
    "light-pink": "#fff9fa",
    violet: "#5d1bb3",
    "hover-green": "#68ff8e",
    white: "white",
};

const colorIterator = () => {
    const colors = Object.values(colorDict);
    let cindex = -1;
    return () => {
        cindex += 1;
        if (cindex > colors.length - 1) {
            cindex = 0;
        }
        return colors[cindex];
    };
};
const pickColor = colorIterator();

const drawTextInExtent = (
    ctx: CanvasRenderingContext2D,
    extent: Extent,
    text: string,
    stop = true
) => {
    const { fontBoundingBoxAscent, fontBoundingBoxDescent } =
        ctx.measureText(text);
    const lineHeight = 1 * (fontBoundingBoxAscent + fontBoundingBoxDescent);

    const words = text.split(" ");
    const spaceMeasure = ctx.measureText(" ");
    let wordIndex = 0;
    const width = extent[2] - extent[0];
    const base = extent[0];

    for (let i = 0; i < 20; i++) {
        const y = extent[1] + (fontBoundingBoxAscent + i * lineHeight);
        let x = 0;
        let startOfLine = true;
        while (wordIndex < words.length) {
            const w = words[wordIndex];
            const wm = ctx.measureText(w);
            if (!startOfLine && x + wm.width > width) {
                // ctx.fillRect(
                //     base + x,
                //     y - fontBoundingBoxAscent,
                //     spaceMeasure.width,
                //     lineHeight
                // );
                // console.log(
                //     "text/break",
                //     `w = ${w}, x = ${x} , wm.width = ${wm.width}, width = ${width}`
                // );
                break;
            } else {
                ctx.fillText(w, base + x, y);
                x += wm.width + spaceMeasure.width;
                wordIndex += 1;
            }
            startOfLine = false;
        }
        if (stop && y + lineHeight > extent[3]) {
            break;
        }
    }
};

const drawNodeTerm = (
    ctx: CanvasRenderingContext2D,
    extent: Extent,
    { tid }: NodeTerm
) => {
    map(({ id, name }: Term) => {
        // ctx.fillStyle = pickColor();
        ctx.fillStyle = "white";
        // ctx.strokeStyle = "white";
        // ctx.lineWidth = 2;
        // ctx.strokeRect(
        //     extent[0],
        //     extent[1],
        //     extentWidth(extent),
        //     extentHeight(extent)
        // );
        ctx.font = `bold 13pt ${termFont(id)}`;
        drawTextInExtent(ctx, extent, name);
    })(findTerm(tid));
};

const drawNodeImage = (
    ctx: CanvasRenderingContext2D,
    extent: Extent,
    { tid }: NodeImage
) => {
    // TODO?
};

const drawNodeTextNote = (
    ctx: CanvasRenderingContext2D,
    extent: Extent,
    node: NodeTextAnnotation
) =>
    map((a: ParagraphAnnotation) => {
        // const e = scaleExtent(0.5, extent);
        // const [x, y] = extentCenter(e);
        // const width = extentWidth(e);
        // const height = extentHeight(e);
        // ctx.beginPath();
        // ctx.fillStyle = "#ff7b00ec";
        // ctx.save();
        // console.log("extent", extent);
        // ctx.strokeStyle = pickColor();
        // ctx.fillStyle = pickColor();
        // ctx.lineWidth = 1;
        // ctx.fillRect(...extent);
        // ctx.strokeRect(...extent);
        // ctx.beginPath();
        // ctx.strokeStyle = "black";
        // ctx.moveTo(extent[0], extent[1]);
        // ctx.lineTo(extent[2], extent[3]);
        // ctx.stroke();
        // ctx.restore();
        // // ctx.arc(x, y, extentWidth(e) / 2, 0, deg2rad(360));
        // ctx.beginPath();
        // ctx.moveTo(x, e[1]);
        // ctx.lineTo(e[2], y);
        // ctx.lineTo(x, e[3]);
        // ctx.lineTo(e[0], y);
        // // ctx.stroke();
        // ctx.fill();

        ctx.font = `normal 5pt sans-serif`;
        drawTextInExtent(ctx, extent, a.content);
    })(findParAnnotation(node.tid));

const PictoCache: { [key: string]: HTMLImageElement } = {};
const drawImageAnnotation =
    (ctx: CanvasRenderingContext2D, extent: Extent) =>
    (annotation: ImageAnnotation) => {
        // ctx.strokeStyle = "white";
        // ctx.lineWidth = 1;
        // ctx.strokeRect(
        //     extent[0],
        //     extent[1],
        //     extentWidth(extent),
        //     extentHeight(extent)
        // );

        const render = map((i: CodexImage) =>
            renderImageAnnotationInExtentFull(i, ctx, annotation, extent)
        );
        render(findImage(annotation.imageId));
        return;
        const draw = (picto: HTMLImageElement) => {
            wrapDraw(ctx, () => {
                ctx.drawImage(
                    picto,
                    0,
                    0,
                    picto.naturalWidth,
                    picto.naturalHeight,
                    extent[0],
                    extent[1],
                    extentWidth(extent),
                    extentHeight(extent)
                );

                const renderLocation = map((loc: Place) => {
                    ctx.fillStyle = "black";
                    ctx.strokeStyle = "white";
                    ctx.lineWidth = 4;
                    ctx.font = "10pt sans-serif";
                    const {
                        width,
                        fontBoundingBoxAscent,
                        fontBoundingBoxDescent,
                    } = ctx.measureText(loc.name);
                    const x = extent[0] + (extentWidth(extent) - width) / 2;
                    const y =
                        extent[3] -
                        (fontBoundingBoxAscent + fontBoundingBoxDescent);
                    ctx.strokeText(loc.name, x, y);
                    ctx.fillText(loc.name, x, y);
                });
                const withLocation = bind(getImageLocation);
                map2(
                    withLocation,
                    renderLocation
                )(findImage(annotation.imageId));
            });
        };

        const key = annotation.id.toString();
        if (!(key in PictoCache)) {
            const picto = new Image(extentWidth(extent), extentHeight(extent));
            picto.addEventListener(
                "load",
                () => {
                    PictoCache[key] = picto;
                    draw(picto);
                },
                false
            );
            picto.src = `/api/image/annotation/picto/${
                annotation.id
            }?color=${encodeURIComponent(pickColor())}`;
        } else {
            draw(PictoCache[key]);
        }
    };

const drawNodeImageNote = (
    ctx: CanvasRenderingContext2D,
    extent: Extent,
    node: NodeImageAnnotation
) => map(drawImageAnnotation(ctx, extent))(findImageAnnotation(node.tid));

const drawNodetransitionalText = (
    ctx: CanvasRenderingContext2D,
    extent: Extent,
    node: NodeTransitionalText
) =>
    map((a: TransitionalText) => {
        ctx.fillStyle = pickColor();
        // ctx.fillRect(
        //     extent[0],
        //     extent[1],
        //     extentWidth(extent),
        //     extentHeight(extent)
        // );
        ctx.fillStyle = "white";
        ctx.font = "bold 11pt sans-serif";
        drawTextInExtent(ctx, extent, a.content, false);
    })(findTransitionalText(node.tid));

const drawNode = (
    ctx: CanvasRenderingContext2D,
    extent: Extent,
    node: PathNode
) => {
    switch (node.type) {
        case "te":
            return drawNodeTerm(ctx, extent, node);
        case "im":
            return drawNodeImage(ctx, extent, node);
        case "ta":
            return drawNodeTextNote(ctx, extent, node);
        case "ia":
            return drawNodeImageNote(ctx, extent, node);
        case "tt":
            return drawNodetransitionalText(ctx, extent, node);
    }
};

export type LandingInteraction = {
    frameIndex: number;
    extent: Extent;
    node: PathNode;
};

const interactionBox = DIV("interaction");
interactionBox.style.position = "absolute";
interactionBox.style.pointerEvents = "none";

const posElem = (e: HTMLElement, [minx, miny, maxx, maxy]: Extent) => {
    interactionBox.style.top = px(miny);
    interactionBox.style.left = px(minx);
    interactionBox.style.width = px(maxx - minx);
    interactionBox.style.height = px(maxy - miny);
};

export const renderLandingInteraction = (root: HTMLElement) => {
    // console.log(query("viz1/landing/interaction"));
    if (interactionBox.parentElement === null) {
        root.append(interactionBox);
    }
    emptyElement(interactionBox);
    const interaction = query("viz1/landing/interaction");
    if (interaction !== null) {
        posElem(interactionBox, interaction.extent);
        const node = interaction.node;
        if (node.type === "te" || node.type === "ia") {
            interactionBox.append(
                events(
                    attrs(DIV(`landing-link ${node.type}` /*, "↬" */), (set) =>
                        set("style", "pointer-events:auto")
                    ),
                    (on) =>
                        on("click", () => {
                            switch (node.type) {
                                case "te":
                                    return navigateTerm(node.tid, false);
                                case "ia":
                                    return map((a: ImageAnnotation) =>
                                        navigateImage(a.imageId, false)
                                    )(findImageAnnotation(node.tid));
                            }
                        })
                )
            );
        }
    } else {
        posElem(interactionBox, [0, 0, 0, 0]);
    }
};

export const renderLanding3 = (root: HTMLElement) => {
    emptyElement(root);
    root.style.position = "absolute";
    root.style.top = "0";
    root.style.right = "0";
    root.style.bottom = "0";
    root.style.left = "0";
    const paths = getPaths();
    const interestingNodes = (path: Path) => path.nodes;
    // path.nodes.filter(
    //     ({ type }) =>
    //         // type === "ia" ||
    //         type === "tt"
    // );

    const nodeScore = Array.from(
        paths
            .reduce((acc, path) => {
                interestingNodes(path).forEach((node) => {
                    map2(
                        map((val: (PathNode & { parent: number })[]) =>
                            acc.set(
                                node.tid,
                                val.concat({ ...node, parent: path.id })
                            )
                        ),
                        alt(() =>
                            acc.set(node.tid, [{ ...node, parent: path.id }])
                        )
                    )(fromNullable(acc.get(node.tid)));
                });
                return acc;
            }, new Map<number, (PathNode & { parent: number })[]>())
            .entries()
    ).sort((a, b) => b[1].length - a[1].length);
    const { width, height } = root.getBoundingClientRect();
    const canvas = CANVAS("landing-inner", width, height);
    const withContext = map((ctx: CanvasRenderingContext2D) => {
        ctx.fillStyle = pickColor();
        // ctx.translate(width / 2, height / 2);
        const scale = 1.5;
        ctx.scale(scale, scale);
        ctx.fillStyle = "#ffc1075e";
        ctx.strokeStyle = "blue";
        ctx.lineWidth = 0.05;
        ctx.font = "6pt sans-serif";
        const drawPoint = (x: number, y: number) => {
            ctx.beginPath();
            ctx.arc(x, y, 0.1, 0, deg2rad(360));
            ctx.fill();
        };
        const drawExtent = ([x1, y1, x2, y2]: Extent) => {
            ctx.beginPath();
            ctx.rect(x1, y1, x2 - x1, y2 - y1);
            ctx.fill();
            ctx.stroke();
        };
        const frames: { tid: number; extent: Extent; node: PathNode }[] = [];
        const notIntersects = (e: Extent): boolean =>
            frames.every((f) => !intersects(e, f.extent));
        const notIntersectsAt = (e: Extent, i: number): boolean =>
            frames.slice(i).every((f) => !intersects(e, f.extent));

        const nodeExtent = (x: number, y: number, node: PathNode): Extent => {
            const termSize = [66, 24];
            const imageSize = [0, 0];
            const textAnnotSize = [24, 24];
            const imageAnnotSize = [66 * 2, 66 * 2];
            const transiSize = [88, 66];
            switch (node.type) {
                case "te":
                    return [
                        x - termSize[0],
                        y - termSize[1],
                        x + termSize[0],
                        y + termSize[1],
                    ];
                case "im":
                    return [
                        x - imageSize[0],
                        y - imageSize[1],
                        x + imageSize[0],
                        y + imageSize[1],
                    ];
                case "ta":
                    return [
                        x - textAnnotSize[0],
                        y - textAnnotSize[1],
                        x + textAnnotSize[0],
                        y + textAnnotSize[1],
                    ];
                case "ia":
                    return [
                        x - imageAnnotSize[0],
                        y - imageAnnotSize[1],
                        x + imageAnnotSize[0],
                        y + imageAnnotSize[1],
                    ];
                case "tt":
                    return [
                        x - transiSize[0],
                        y - transiSize[1],
                        x + transiSize[0],
                        y + transiSize[1],
                    ];
            }
        };
        const transform = ctx.getTransform();
        const inCanvas = (ix: number, iy: number) => {
            const { x, y } = transform.transformPoint({ x: ix, y: iy });
            return x > 0 && x < width && y > 0 && y < height;
        };
        // const maxR = height / 4;
        // const nextSlot = (
        //     tid: number,
        //     r: number,
        //     t: number,
        //     node: PathNode
        // ): [number, number] => {
        //     let curR = r;
        //     let curT = t;
        //     if (frames.length === 0) {
        //         const [x, y] = polar2cartesian(r, t, [0, 0]);
        //         const extent = nodeExtent(x, y, node);
        //         frames.push({ tid, extent, node });
        //         return [r, t];
        //     }
        //     // let scale = Math.min(maxR / curR, 1.0);
        //     for (let i = 0; i < 2000; i++) {
        //         const [x, y] = polar2cartesian(curR, curT, [0, 0]);
        //         // scale = Math.min(maxR / curR, 1.0);
        //         const extent = nodeExtent(x, y, node);
        //         // if (!bboxEq.equals(extent, lastExtent)) {
        //         if (notIntersects(extent) && inCanvas(x, y)) {
        //             break;
        //         }
        //         curR += 0.05;
        //         curT += 1;
        //         // curT += Math.random() - 1;
        //     }
        //     const [x, y] = polar2cartesian(curR, curT, [0, 0]);
        //     const extent = nodeExtent(x, y, node);
        //     frames.push({ tid, extent, node });
        //     return [curR, curT];
        // };
        const sw = width / scale;
        const sh = height / scale;
        const lineNumber = Math.ceil(nodeScore.length / 10);
        const linePos = (column: number, line: number): [number, number] => [
            (sw / lineNumber) * column,
            sh - (sh / lineNumber) * line,
        ];
        let currentframestart = 0;
        const nextSlot2 = (
            tid: number,
            column: number,
            line: number,
            node: PathNode
        ): [number, number] => {
            if (frames.length === 0) {
                const [x, y] = linePos(column, line);
                const extent = nodeExtent(x, y, node);
                frames.push({ tid, extent, node });
                return [column, line];
            }
            let curLine = line;
            let curColumn = column;
            for (let i = 0; i < 10000; i++) {
                const [x, y] = linePos(curColumn, curLine);
                const extent = nodeExtent(x, y, node);
                // if (!bboxEq.equals(extent, lastExtent)) {
                if (notIntersectsAt(extent, currentframestart)) {
                    break;
                }
                if (curColumn + 1 >= lineNumber) {
                    curColumn = 0;
                    curLine += 1;
                    currentframestart = frames.length - 1;
                    break;
                } else {
                    curColumn += 1;
                }
            }
            const [x, y] = linePos(curColumn, curLine);
            const extent = nodeExtent(x, y, node);
            frames.push({ tid, extent, node });
            return [curColumn, curLine];
        };
        // nodeScore.map(([tid, nodes], idx) => {
        //     // const [x, y] = polar2cartesian(curR, curT, [0, 0]);
        //     // drawPoint(x, y);
        //     // curR += 0.05;
        //     // curT += (Math.random() - 1) * 20;
        // });
        nodeScore.reduce(
            // ([r, t], [tid, nodes]) => nextSlot(tid, r, t, nodes[0]),
            ([r, t], [tid, nodes]) => nextSlot2(tid, r, t, nodes[0]),
            [0, 0] as [number, number]
        );

        // const isHighFrame = (idx: number) =>
        //     pipe2(
        //         fromNullable(query("viz1/frame/highlight")),
        //         map((fid) => fid === idx),
        //         orElse(false)
        //     );

        const drawBase = () =>
            frames
                .slice()
                .reverse()
                .map(({ extent, node }, idx) => {
                    // drawExtent(extent);
                    wrapDraw(ctx, () => {
                        ctx.fillStyle = "#ff7b00ec";
                        drawNode(ctx, extent, node);
                        // if (isHighFrame(idx)) {
                        //     const [x1, y1, x2, y2] = extent;
                        //     ctx.strokeStyle = "white";
                        //     ctx.lineWidth = 2;
                        //     ctx.beginPath();
                        //     ctx.rect(x1, y1, x2 - x1, y2 - y1);
                        //     ctx.stroke();
                        // }
                    });
                });

        const pathMap = paths.reduce((acc, path) => {
            const extents = interestingNodes(path).map(
                (node) => frames.find((f) => f.tid === node.tid)!.extent
            );
            interestingNodes(path).forEach(({ tid }) => {
                const key = tid.toString();
                const val = {
                    pid: path.id,
                    extents,
                };
                if (!(key in acc)) {
                    acc[key] = [val];
                } else {
                    acc[key].push(val);
                }
            });
            return acc;
        }, {} as Record<string, { pid: number; extents: Extent[] }[]>);

        const clearCanvas = () => {
            wrapDraw(ctx, () => {
                const t = ctx.getTransform();
                ctx.resetTransform();
                // ctx.fillStyle = "black";
                // ctx.fillRect(0, 0, width, height);
                ctx.clearRect(0, 0, width, height);
                ctx.setTransform(t);
            });
        };

        const DRAW_CURVE = true;
        const drawTid = (tid: number) => {
            const pm = pathMap[tid.toString()];
            if (pm) {
                clearCanvas();
                // drawBase();

                wrapDraw(ctx, () => {
                    ctx.lineWidth = ctx.lineWidth * 60;
                    ctx.lineJoin = "round";
                    ctx.lineCap = "round";

                    pm.forEach(({ pid, extents }) => {
                        const c = pickColor();
                        ctx.strokeStyle = c;
                        if (DRAW_CURVE) {
                            const curve = pointsToCurve(
                                extents.map(extentCenter)
                            );
                            drawCurve(ctx, curve);
                        } else {
                            ctx.beginPath();
                            extents.map(extentCenter).map((c, i) => {
                                if (i === 0) {
                                    ctx.moveTo(c[0], c[1]);
                                } else {
                                    ctx.lineTo(c[0], c[1]);
                                }
                            });
                        }
                        ctx.stroke();
                    });
                });
                drawBase();
                // ctx.save();
                // ctx.strokeStyle = "red";
                // ctx.fillStyle = "#ffc107";
                // ctx.lineWidth = ctx.lineWidth * 2;
                // pm.forEach(({ pid, extents }) => {
                //     extents.map(drawExtent);
                // });
                // ctx.restore();
                // ctx.save();
                // ctx.fillStyle = "black";
                // ctx.font = "1pt sans-serif";
                // pm.forEach(({ pid, extents }) => {
                //     extents.map((c) => {
                //         ctx.fillText(pid.toString(), c[0], c[3]);
                //     });
                // });
                // ctx.restore();
            } else {
                console.error("arrrrg", tid);
            }
        };

        // const tids = nodeScore.map(([tid, _]) => tid);
        // let ctid = -1;
        // const nextTid = () => {
        //     ctid += 1;
        //     if (ctid > tids.length) {
        //         ctid = 0;
        //     }
        //     return tids[ctid];
        // };
        // const it = window.setInterval(() => {
        //     const tid = nextTid();
        //     console.log("tid", tid, new Date().toLocaleTimeString());
        //     drawTid(tid);
        // }, 1000);

        canvas.addEventListener("mousemove", (e) => {
            const t = ctx.getTransform();
            const inv = t.inverse();
            const { x, y } = inv.transformPoint({ x: e.clientX, y: e.clientY });
            const interaction = query("viz1/landing/interaction");
            for (let i = 0; i < frames.length; i++) {
                const frame = frames[i];
                if (intersects(frame.extent, [x, y, x, y])) {
                    if (interaction === null || interaction.frameIndex !== i) {
                        const nodes = nodeScore;
                        assign("viz1/landing/interaction", {
                            frameIndex: i,
                            extent: transformExtent(t, frame.extent),
                            node: frame.node,
                        });
                    }
                    return;
                }
            }
            if (interaction !== null) {
                assign("viz1/landing/interaction", null);
            }
        });
        // canvas.addEventListener("mousemove", (e) => {
        //     const { x, y } = ctx
        //         .getTransform()
        //         .inverse()
        //         .transformPoint({ x: e.clientX, y: e.clientY });
        //     for (let i = 0; i < frames.length; i++) {
        //         const frame = frames[i];
        //         if (intersects(frame.extent, [x, y, x, y])) {
        //             if (query("viz1/frame/highlight") !== i) {
        //                 assign("viz1/frame/highlight", i);
        //                 clearCanvas();
        //                 drawBase();
        //             }
        //             return;
        //         }
        //     }
        //     if (query("viz1/frame/highlight") !== null) {
        //         assign("viz1/frame/highlight", null);
        //         clearCanvas();
        //         drawBase();
        //     }
        // });

        canvas.addEventListener("click", (e) => {
            const { x, y } = ctx
                .getTransform()
                .inverse()
                .transformPoint({ x: e.clientX, y: e.clientY });
            for (let i = 0; i < frames.length; i++) {
                const frame = frames[i];
                if (intersects(frame.extent, [x, y, x, y])) {
                    // console.log("Got tid", frame.tid, x, y);
                    drawTid(frame.tid);
                    return;
                }
            }
            clearCanvas();
            drawBase();
        });

        drawBase();
    });

    withContext(fromNullable(canvas.getContext("2d")));
    root.append(canvas);
};

const findIntersectingPaths = (node: PathNode, paths: Path[]) => {
    const result: Tuple<Path, number>[] = [];
    paths.forEach((path) => {
        const i = path.nodes.findIndex(
            ({ type, tid }) => tid === node.tid && type === node.type
        );
        if (i >= 0) {
            result.push(tuple(path, i));
        }
    });
    return result;
};

const renderNodeTerm = ({ tid }: NodeTerm) => {
    const className = "path-node term";
    const node = DIV(className);
    map(({ name, statement }: Term) => {
        node.append(
            // events(
            DIV(
                "_x",
                SPAN("term-name", name),
                SPAN("term-description", statement),
                events(DIV("link", "↬"), (on) =>
                    on("click", () => navigateTerm(tid, true))
                )
            )
        );
    })(findTerm(tid));
    return node;
};

const renderNodeImage = (node: NodeImage) => DIV("path-node");

const renderNodeTextNote = (node: NodeTextAnnotation) =>
    DIV(
        "path-node par-annot",
        map((a: ParagraphAnnotation) => a.content)(findParAnnotation(node.tid))
    );

const withImageAnnotation = (annotation: ImageAnnotation) => {
    const width = 200;
    const height = 200;

    const render = map((i: CodexImage) => {
        const canvas = CANVAS("image-annotation-canvas", width, height);
        const ctx = canvas.getContext("2d");
        if (ctx !== null) {
            renderImageAnnotation(i, ctx, annotation, [width, height]);
        }
        return DIV(
            "image-annotation",
            canvas,
            events(DIV("link", "↬"), (on) =>
                on("click", () => navigateImage(i.id, true))
            )
        );
    });

    return orElse<BaseNode>(DIV("err", "error when loading image"))(
        render(findImage(annotation.imageId))
    );
};

const renderNodeImageNote = (node: NodeImageAnnotation) =>
    DIV(
        "path-node image-annot",
        map(withImageAnnotation)(findImageAnnotation(node.tid))
    );

const renderNodetransitionalText = (node: NodeTransitionalText) =>
    DIV(
        "path-node par-text",
        map((a: TransitionalText) => a.content)(findTransitionalText(node.tid))
    );

const renderNode = (node: PathNode) => {
    switch (node.type) {
        case "te":
            return renderNodeTerm(node);
        case "im":
            return renderNodeImage(node);
        case "ta":
            return renderNodeTextNote(node);
        case "ia":
            return renderNodeImageNote(node);
        case "tt":
            return renderNodetransitionalText(node);
    }
};
