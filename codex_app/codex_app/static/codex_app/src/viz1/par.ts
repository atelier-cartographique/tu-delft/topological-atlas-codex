import { attrs, emptyElement, events } from "../lib/dom.js";
import { ANCHOR, CANVAS, DIV, H1, SPAN } from "../lib/html.js";
import {
    CodexImage,
    ImageAnnotation,
    findImage,
    renderImageAnnotation,
} from "../lib/image.js";
import { eventImage, eventTerm, logEvent } from "../lib/log-event.js";
import { bind, fromNullable, map } from "../lib/option.js";
import { ParagraphAnnotation } from "../lib/paragraph.js";
import { query } from "../lib/state.js";
import { Term, findTerm, getViz1Term } from "../lib/term.js";
import { termFont } from "./image.js";
import { navigateImage, navigateTerm } from "./route.js";

let inited = false;
const init = (root: HTMLElement) => {
    if (inited) {
        return;
    }
    inited = true;

    // root.addEventListener("click", () => assign("show/par", false));
};

const ACTIVE_CLASS = "active";

const getImageForTerm = (term: Term, cid: number) => {
    let annot = query("annotations/image").find(
        (a) => a.tag === "term" && a.termId === term.id && cid !== a.imageId
    );
    // if (annot === undefined) {
    //     annot = query("annotations/image").find(
    //         (a) =>
    //             a.tag === "category" &&
    //             cid !== a.imageId &&
    //             term.categories.indexOf(a.categoryId) >= 0
    //     );
    // }
    return bind(({ imageId }: ImageAnnotation) => findImage(imageId))(
        fromNullable(annot)
    );
};

// const withImageTarget = (element: HTMLElement) =>
//     map((i: CodexImage) =>
//         attrs(
//             events(element, (add) => add("click", () => setImage(i))),
//             (set) => set("class", "location")
//         )
//     );

// const renderContent = (content: string, term: Term) => {
//     let element = SPAN("", content);
//     if (term.uuid === TermLocationUUID) {
//         element = orElse(element)(
//             withImageTarget(element)(
//                 bind(findImageInPlace)(findPlaceByName(content))
//             )
//         );
//     }
//     return element;
// };

const uniq =
    <T>(eq: (a: T, b: T) => boolean) =>
    (xs: T[]) => {
        const result: T[] = [];
        for (const x of xs) {
            if (result.findIndex((r) => eq(r, x)) < 0) {
                result.push(x);
            }
        }
        return result;
    };

const sameTermAnnot = uniq<ParagraphAnnotation>(
    (a, b) => a.termId === b.termId
);

const renderAnnotContent = (
    annot: ParagraphAnnotation
    // currentImage: CodexImage
) =>
    DIV(
        "annot",
        DIV("content", annot.content),
        DIV(
            "proximity",
            ...sameTermAnnot(
                query("annotations/paragraph").filter(
                    ({ parId, id }) => parId === annot.parId && id !== annot.id
                )
            ).map(({ termId, content }) =>
                map((term: Term) =>
                    attrs(
                        events(DIV("term", term.name), (add) => {
                            add("click", () => {
                                navigateTerm(term.id, true);
                            });
                        }),
                        (set) => set("title", content)
                    )
                )(findTerm(termId))
            )
        )
    );

const renderImages = (root: Element) => (tid: number) => {
    const imageBox = DIV("image-box");
    query("annotations/image")
        .filter((a) => a.tag === "term" && a.termId === tid)
        .map((annotation) => {
            const width = 400;
            const height = 400;
            const render = map((i: CodexImage) => {
                const canvas = events(
                    CANVAS("image-annotation", width, height),
                    (add) =>
                        add("click", () => {
                            navigateImage(i.id, true);
                        })
                );
                const ctx = canvas.getContext("2d");
                if (ctx !== null) {
                    renderImageAnnotation(i, ctx, annotation, [width, height]);

                    imageBox.append(canvas);
                }
            });
            render(findImage(annotation.imageId));
        });
    return imageBox;
};

export const renderPar = (root: HTMLElement) => {
    init(root);
    emptyElement(root);
    root.classList.remove(ACTIVE_CLASS);
    const withTerm = (term: Term) => {
        root.classList.add(ACTIVE_CLASS);
        root.append(
            H1(
                "title",
                attrs(SPAN("term-link", term.name), (set) =>
                    set("style", `font: bold 1.5em ${termFont(term.id)}`)
                )
            )
        );
        const imgBox = renderImages(root)(term.id);

        const sameTerm = query("annotations/paragraph")
            .filter(({ termId }) => term.id === termId)
            .sort((a, b) => b.content.length - a.content.length);

        const annotBox = DIV("annot-box");
        sameTerm.forEach((annotation) => {
            annotBox.append(renderAnnotContent(annotation));
        });
        const parBox = DIV("par-box");
        parBox.append(imgBox, annotBox);
        root.append(parBox);
    };

    map(withTerm)(getViz1Term());
};
