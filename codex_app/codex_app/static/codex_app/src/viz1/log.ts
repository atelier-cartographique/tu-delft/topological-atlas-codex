import { emptyElement, events } from "../lib/dom.js";
import { DIV } from "../lib/html.js";
import { findImage, ImageAnnotation, setImage } from "../lib/image.js";
import { LogEvent } from "../lib/log-event.js";
import { bind, fromNullable, map } from "../lib/option.js";
import { query } from "../lib/state.js";
import { findTerm, Term } from "../lib/term.js";
import { navigateImage, navigateTerm } from "./route.js";

const getImageForTerm = (term: Term) => {
    let annot = query("annotations/image").find(
        (a) => a.tag === "term" && a.termId === term.id
    );
    // if (annot === undefined) {
    //     annot = query("annotations/image").find(
    //         (a) =>
    //             a.tag === "category" &&
    //             term.categories.indexOf(a.categoryId) >= 0
    //     );
    // }
    return bind(({ imageId }: ImageAnnotation) => findImage(imageId))(
        fromNullable(annot)
    );
};

const renderEvent = (event: LogEvent) => {
    switch (event.tag) {
        case "image":
            return events(DIV("event event-image"), (add) =>
                add("click", () => navigateImage(event.id, false))
            );
        case "term":
            return events(DIV("event event-term"), (add) =>
                add("click", () => navigateTerm(event.id, false))
            );
        case "image-annotation":
            return DIV("event event-image-annotation");
    }
};

export const renderLog = (root: HTMLElement) => {
    emptyElement(root);
    query("viz1/user-log").map((event) => root.append(renderEvent(event)));
};
