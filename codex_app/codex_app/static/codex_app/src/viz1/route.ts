import { index, first } from "../lib/array.js";
import { findImage, getImageList, setImage } from "../lib/image.js";
import { eventImage, eventTerm, logEvent } from "../lib/log-event.js";
import {
    bind,
    fromNullable,
    map,
    map2,
    merge,
    orElse,
    orElseL,
    pipe2,
} from "../lib/option.js";
import { findPath } from "../lib/path.js";
import { Router } from "../lib/router.js";
import { assign, query } from "../lib/state.js";
import { findTerm } from "../lib/term.js";
import { fst, snd, tuple } from "../lib/tuple.js";
import { tryNumber } from "../lib/util.js";
import { resetPath, setCurrentPath } from "./path.js";

export const landing = () => query("viz1/landing");
export const setLanding = (b: boolean) => assign("viz1/landing", b);

const { home, route, navigate, back } = Router("viz1");

home("index.html", () => setLanding(true));

const firstId = map2(bind(first), bind(tryNumber));

const imageAt = map2(bind(findImage), map(setImage));

route("image", imageAt, firstId);

const termAt = map2(
    bind(findTerm),
    map(({ id }) => {
        assign("viz1/term", id);
    })
);

route("term", termAt, firstId);

const nodeEx = (parts: string[]) =>
    merge(bind(tryNumber)(index(0, parts)), bind(tryNumber)(index(1, parts)));

route(
    "node",
    map((t) => {
        pipe2(
            findPath(fst(t)),
            bind((path) =>
                map((i: number) => tuple(path, i))(
                    fromNullable(
                        path.nodes.findIndex(({ id }) => id === snd(t))
                    )
                )
            ),
            map((pi) => setCurrentPath(fst(pi), snd(pi)))
        );
    }),
    nodeEx
);

route(
    "path",
    map2(
        bind(findPath),
        map((path) => setCurrentPath(path, 0))
    ),
    firstId
);

export const navigateHome = () => navigate("index.html", []);

export const navigateImage = (id: number, log: boolean) => {
    if (log) {
        logEvent(eventImage(id));
    }
    setLanding(false);
    resetPath();
    navigate("image", [id]);
};

export const navigateTerm = (id: number, log: boolean) => {
    if (log) {
        logEvent(eventTerm(id));
    }
    setLanding(false);
    resetPath();
    navigate("term", [id]);
};

export const navigatePath = (id: number) => {
    setLanding(false);
    navigate("path", [id]);
};

export const initial = () => {
    const args = document.location.pathname
        .split("/")
        .filter((x) => x.length > 0);
    map2(
        map((prefix: string) => navigate(prefix, args.slice(2))),
        orElseL(navigateHome)
    )(index(1, args));
};
