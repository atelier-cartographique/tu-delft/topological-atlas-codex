import { init, query, State, update } from "../lib/state.js";
import { mount, app } from "../lib/app.js";
import { renderTooltip } from "../lib/tooltip.js";
import { renderCodex } from "./codex.js";
import { renderText } from "./text.js";

export const main = (state: Partial<State>) => {
    init(state);
    const text = document.getElementById("text");
    const codex = document.getElementById("codex");
    const tooltip = document.getElementById("tooltip-box");
    mount(text, renderText, "terms", "annotations/paragraph", "highlight");
    mount(codex, renderCodex, "terms", "selection", "annotations/paragraph");
    mount(tooltip, renderTooltip, "tooltip");
    app().then(() => update("_version", (v) => v + 1));
};
