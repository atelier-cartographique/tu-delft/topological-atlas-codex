import { renderParagraph } from "../lib/paragraph.js";
import { emptyElement } from "../lib/dom.js";
import { query } from "../lib/state.js";

export const renderText = (root: HTMLElement) => {
    emptyElement(root);
    query("paragraphs").forEach((par) => {
        const parNode = renderParagraph(par);
        root.appendChild(parNode);
    });
};
