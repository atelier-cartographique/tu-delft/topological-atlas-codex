import { hasSelection } from "../lib/app.js";
import { emptyElement } from "../lib/dom.js";
import { BUTTON, ANCHOR, H3, DIV } from "../lib/html.js";
import { map, fromNullable } from "../lib/option.js";
import { ParSel } from "../lib/paragraph.js";
import { update, assign, query } from "../lib/state.js";
import { findTerm, postParagraphAnnotation, Term } from "../lib/term.js";

const select = (term: Term) =>
    map((sel: ParSel) => {
        postParagraphAnnotation({ ...sel, termId: term.id, textId: -1 })
            .then((a) => update("annotations/paragraph", (as) => as.concat(a)))
            .catch((err) => console.log("post annotation error", err));
        assign("selection", null);
    })(fromNullable(query("selection")));

const renderTerm = (term: Term) => {
    const button = BUTTON("term-select term-select-short", "✓");
    if (hasSelection()) {
        button.addEventListener("click", () => select(term));
    } else {
        button.setAttribute("disabled", "true");
    }
    const anchor = ANCHOR("term-link", `/term/${term.id}.html`, term.name);
    anchor.setAttribute("target", "_blank");
    const title = H3("term-name", anchor, button);
    const statement = DIV("term-statement", term.statement);
    const node = DIV(`term`, title, statement);
    return node;
};

const renderTermButton = (term: Term) => {
    const button = BUTTON("term-select", term.name);
    if (hasSelection()) {
        button.addEventListener("click", () => select(term));
    } else {
        button.setAttribute("disabled", "true");
    }
    const node = DIV(`term-wrapper`, button);
    return node;
};

export const renderCodex = (root: HTMLElement) => {
    emptyElement(root);
    const buttons = DIV("buttons");
    const terms = DIV("terms");
    query("annotations/paragraph")
        .reduce<{ t: number; c: number }[]>((acc, { termId }) => {
            const tup = acc.find(({ t, c }) => t === termId);
            if (tup === undefined) {
                return acc.concat({ t: termId, c: 1 });
            }
            tup.c += 1;
            return acc;
        }, [])
        .sort((a, b) => b.c - a.c)
        .forEach(({ t }) =>
            map((t: Term) => buttons.appendChild(renderTermButton(t)))(
                findTerm(t)
            )
        );
    query("terms")
        .sort((a, b) => a.name.localeCompare(b.name))
        .forEach((t) => {
            terms.appendChild(renderTerm(t));
        });

    root.appendChild(buttons);
    root.appendChild(terms);
};
