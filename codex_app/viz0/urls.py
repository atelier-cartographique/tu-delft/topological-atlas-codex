"""codex_app URL Configuration
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings

from .views import (
    get_narrative,
    get_path_list,
    get_term,
    get_index,
)


def prefix(p):
    return f"viz0.{p}"


urlpatterns = [
    path(
        "index.html",
        get_index,
        name=prefix("index"),
    ),
    path(
        "term/<int:id>.html",
        get_term,
        name=prefix("get_term"),
    ),
    path(
        "narrative/<int:id>.html",
        get_narrative,
        name=prefix("get_narrative"),
    ),
    path(
        "paths/",
        get_path_list,
        name=prefix("get_path_list"),
    ),
]
