from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from codex.models import (
    ParAnnotation,
    ImageAnnotation,
    Term,
    TermCatCat,
    Path2,
    Image,
    Qualifier,
)
from codex.serializers import (
    serialize_image,
    serialize_image_annotation,
    serialize_paragraph_annotation,
    serialize_path,
    serialize_term,
    serialize_transitional_text,
)
from json import dumps


def make_paragrahps(term):
    paragraphs = {}
    for a in term.annotation.all():
        pid = a.paragraph.id
        if pid not in paragraphs:
            paragraphs[pid] = 1
        else:
            paragraphs[pid] += 1
    return paragraphs


def merge_content(old_content, in_oasis, in_shape_shifting, in_hyper_reality):
    c, o, s, h = old_content
    return c, o or in_oasis, s or in_shape_shifting, h or in_hyper_reality


OASIS_ID = "b154704a-ea62-4287-9cc2-8bc7a66e2092"
SHAPE_SHIFTING_ID = "c4101f80-4570-4129-a2a5-a417e11adfe0"
HYPER_REALITY_ID = "c35f8580-4d64-4ea1-84ff-f73569df96b1"


@login_required
def get_index(request):
    term_oasis = Term.objects.get(identifier=OASIS_ID)
    term_shape_shifting = Term.objects.get(identifier=SHAPE_SHIFTING_ID)
    term_hyper_reality = Term.objects.get(identifier=HYPER_REALITY_ID)
    oasis = make_paragrahps(term_oasis)
    shape_shifting = make_paragrahps(term_shape_shifting)
    hyper_reality = make_paragrahps(term_hyper_reality)
    locations = Term.objects.get(identifier="40b39814-4783-4db7-bb34-8bd14fbcede7")

    annotations = {}
    for annot in locations.annotation.all():
        par = annot.paragraph
        text = par.text
        in_oasis = par.id in oasis
        in_shape_shifting = par.id in shape_shifting
        in_hyper_reality = par.id in hyper_reality

        if text.id not in annotations:
            annotations[text.id] = {
                "text": text,
                "content": [],
            }

        if (
            len(annotations[text.id]["content"]) > 0
            and annotations[text.id]["content"][-1][0] == annot.content
        ):
            annotations[text.id]["content"][-1] = merge_content(
                annotations[text.id]["content"][-1],
                in_oasis,
                in_shape_shifting,
                in_hyper_reality,
            )
        else:
            annotations[text.id]["content"].append(
                (
                    annot.content,
                    in_oasis,
                    in_shape_shifting,
                    in_hyper_reality,
                )
            )

    context = {
        "term": locations,
        "annotations": list(annotations.values()),
        "OASIS_ID": term_oasis.id,
        "SHAPE_SHIFTING_ID": term_shape_shifting.id,
        "HYPER_REALITY_ID": term_hyper_reality.id,
    }
    return render(request, "viz0/index.html", context)


def append_texts_for_term(term, relevant, texts):
    for a in term.annotation.select_related("paragraph__text").filter(
        quality__qualifier=relevant
    ):
        text = a.paragraph.text
        if text.id not in texts:
            texts[text.id] = {"text": text, "paragraphs": [], "count": 0}
        if a.paragraph.id not in [p["par"].id for p in texts[text.id]["paragraphs"]]:
            others = ParAnnotation.objects.filter(~Q(term=term), paragraph=a.paragraph)
            terms = list(set([o.term for o in others]))

            texts[text.id]["paragraphs"].append(
                {"par": a.paragraph, "annotations": [a], "terms": terms}
            )
            texts[text.id]["paragraphs"] = sorted(
                texts[text.id]["paragraphs"], key=lambda x: x["par"].order
            )
        else:
            for p in texts[text.id]["paragraphs"]:
                if p["par"].id == a.paragraph.id:
                    p["annotations"].append(a)
                    others = ParAnnotation.objects.filter(
                        ~Q(term=term), paragraph=a.paragraph
                    )
                    p["terms"] = list(
                        set(p["terms"]).union(set([o.term for o in others]))
                    )

        texts[text.id]["count"] += 1


def flatten(xs):
    for l0 in xs:
        for l1 in l0:
            yield l1


@login_required
def get_term(request, id):
    term = get_object_or_404(Term, pk=id)
    texts = {}
    relevant = Qualifier.objects.first()
    append_texts_for_term(term, relevant, texts)
    terms = Term.objects.filter(is_relation=False)
    paths = Path2.objects.filter(user=request.user)
    path_annotations = ParAnnotation.objects.filter(
        id__in=[n.tid for ls in [list(p.text_annotations()) for p in paths] for n in ls]
    )
    image_annotations = [
        serialize_image_annotation(i) for i in ImageAnnotation.objects.all()
    ]
    images = [serialize_image(i) for i in Image.objects.all()]
    transitionals = [
        serialize_transitional_text(n.get_target())
        for n in flatten([path.transitional_texts() for path in paths])
    ]
    context = {
        "term": term,
        "terms": dumps([serialize_term(t) for t in terms], indent=2),
        "paths": dumps([serialize_path(p) for p in paths], indent=2),
        "transitionals": dumps(transitionals, indent=2),
        "annotations": dumps(
            [serialize_paragraph_annotation(a) for a in term.annotation.filter()]
            + [serialize_paragraph_annotation(a) for a in path_annotations],
            indent=2,
        ),
        "texts": list(texts.values()),
        "images": dumps(images),
        "image_annotations": dumps(image_annotations),
    }
    return render(request, "viz0/term.html", context)


def append_texts_for_narrative(term, texts):
    for a in term.annotation.select_related("paragraph__text").all():
        text = a.paragraph.text
        if text.id not in texts:
            texts[text.id] = {"text": text, "paragraphs": [], "count": 0}

        if a.paragraph.id not in [p["par"].id for p in texts[text.id]["paragraphs"]]:
            others = ParAnnotation.objects.filter(~Q(term=term), paragraph=a.paragraph)
            terms = list(set([o.term for o in others]))
            others_per_term = {t: [] for t in terms}
            for o in others:
                others_per_term[o.term].append(o)

            texts[text.id]["paragraphs"].append(
                {"par": a.paragraph, "annotations": [a], "terms": others_per_term}
            )
            texts[text.id]["paragraphs"] = sorted(
                texts[text.id]["paragraphs"], key=lambda x: x["par"].order
            )
        else:
            for p in texts[text.id]["paragraphs"]:
                if p["par"].id == a.paragraph.id:
                    p["annotations"].append(a)
                    others = ParAnnotation.objects.filter(
                        ~Q(term=term), paragraph=a.paragraph
                    )
                    others_per_term = p["terms"]
                    for o in others:
                        if o.term not in others_per_term:
                            others_per_term[o.term] = []
                        others_per_term[o.term].append(o)
                    p["terms"] = others_per_term

        texts[text.id]["count"] += 1


@login_required
def get_narrative(request, id):
    term = get_object_or_404(Term, pk=id)
    texts = {}
    append_texts_for_narrative(term, texts)

    image_annotations = []

    for cat in [cc.category for cc in TermCatCat.objects.filter(term=term)]:
        image_annotations.extend(list(cat.images.all()))

    # for i in term.images.all():
    #     if i.image.id not in images:
    #         images[i.image.id] = {"image": i.image, "annotations": []}
    #     images[i.image.id]["annotations"].append(i)

    context = {
        "term": term,
        "texts": texts.values(),
        "image_annotations": image_annotations,
    }
    return render(request, "viz0/narrative.html", context)


@login_required
def get_path_list(request):
    paths = Path2.objects.all()

    par_annotations = ParAnnotation.objects.filter(
        id__in=[n.tid for ls in [list(p.text_annotations()) for p in paths] for n in ls]
    )
    # context = {
    #     "objects": paths,
    #     "paths": dumps([serialize_path(p) for p in paths], indent=2),
    #     "terms": dumps([serialize_term(t) for t in terms], indent=2),
    #     "par_annotations": dumps(
    #         [serialize_paragraph_annotation(a) for a in par_annotations], indent=2
    #     ),
    # }

    terms = Term.objects.filter(is_relation=False)

    image_annotations = [
        serialize_image_annotation(i) for i in ImageAnnotation.objects.all()
    ]
    images = [serialize_image(i) for i in Image.objects.all()]
    transitionals = [
        serialize_transitional_text(n.get_target())
        for n in flatten([path.transitional_texts() for path in paths])
    ]
    context = {
        "terms": dumps([serialize_term(t) for t in terms], indent=2),
        "paths": dumps([serialize_path(p) for p in paths], indent=2),
        "transitionals": dumps(transitionals, indent=2),
        "annotations": dumps(
            [serialize_paragraph_annotation(a) for a in par_annotations], indent=2
        ),
        "images": dumps(images),
        "image_annotations": dumps(image_annotations),
    }

    return render(request, "viz0/path-list.html", context)
