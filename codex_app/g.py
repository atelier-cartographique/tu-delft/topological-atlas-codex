from django.db.models import Q
from codex.models.codex import *

TERM_LOCATION = Term.objects.get(identifier="40b39814-4783-4db7-bb34-8bd14fbcede7")
LOC_ANNOTATIONS = list(ParAnnotation.objects.filter(term=TERM_LOCATION))


def process_place(place, rels):
    print('Processing', str(place))
    for i in place.image_set.all():
        rels.append((str(place), str(i)))
    for i in place.image_set.all():
        for a in i.imageannotation_set.all():
            if a.term is not None:
                rels.append((str(i), str(a.term)))
            else:
                for tcc in a.category.termcatcat_set.all():
                    rels.append((str(i), str(tcc.term)))
    annots = []
    for a in LOC_ANNOTATIONS:
        an = a.content.lower()
        if place.name.lower() == an:
            annots.append(a)
            continue
        for al in place.alias.all():
            if al.name.lower() == an:
                annots.append(a)
                break
    for a in annots:
        others = ParAnnotation.objects.filter(~Q(id=a.id), paragraph=a.paragraph)
        for o in others:
            rels.append((a.content, str(o.term)))

def write_dot(rels, fp='output.dot'):
    with open(fp, 'w') as output:
        output.write('digraph topo {\n')
        for left, right in set(rels):
            output.write(f'"{left}" -> "{right}";\n')
        output.write('\n}')

relations = []
for place in Place.objects.all():
    process_place(place, relations)

write_dot(relations, 'tout.dot')