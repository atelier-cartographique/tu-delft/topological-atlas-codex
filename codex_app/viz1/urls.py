"""codex_app URL Configuration
"""
from django.urls import path, re_path

from .views import (
    get_index,
)


def prefix(p):
    return f"viz1.{p}"


urlpatterns = [
    re_path(
        ".*",
        get_index,
        name=prefix("index"),
    ),
]
