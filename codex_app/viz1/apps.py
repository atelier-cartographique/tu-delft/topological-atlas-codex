from django.apps import AppConfig


class Viz1Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'viz1'
