from json import loads
from django.http import FileResponse, JsonResponse, HttpResponseForbidden
from django.shortcuts import get_object_or_404
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_page

from codex.models import (
    ImageAnnotation,
    Term,
    Agreement,
    Path2,
    PathNode,
    TransitionalText,
    ParAnnotation,
)
from codex.serializers import (
    deserialize_annotation_quality,
    deserialize_image_annotation,
    deserialize_paragraph_annotation,
    serialize_annotation_quality,
    serialize_image_annotation,
    serialize_paragraph_annotation,
    serialize_path,
    serialize_transitional_text,
)
from codex.triangle import clip_with_annotation, picto_with_annotation


def serialize_agreement(agreement: Agreement):
    return {
        "id": agreement.id,
        "party": agreement.party,
    }


def serialize_term(term: Term):
    return {
        "id": term.id,
        "identifier": term.identifier,
        "name": term.name,
        "statement": term.statement,
        "parent": term.parent,
        "parties": [serialize_agreement(a) for a in term.agreement_set.all()],
    }


@login_required
def get_distinct_terms(_request):
    terms = {}
    for term in Term.objects.order_by("-created_on"):
        if term.identifier not in terms:
            terms[term.identifier] = term
    data = [serialize_term(t) for t in terms.values()]
    return JsonResponse(data, safe=False)


@login_required
def get_img_annotation_image(request, id):
    a = get_object_or_404(ImageAnnotation, pk=id)
    streamed_image = clip_with_annotation(a)
    return FileResponse(streamed_image)


def get_img_annotation_picto(request, id):
    a = get_object_or_404(ImageAnnotation, pk=id)
    color = request.GET.get("color", "#7fea9a")
    streamed_image = picto_with_annotation(a, color)
    return FileResponse(streamed_image)


@login_required
@require_http_methods(["POST"])
def post_par_annotation(request):
    data = loads(request.body.decode("utf-8"))
    data["user"] = request.user
    instance = deserialize_paragraph_annotation(data)
    instance.save()
    return JsonResponse(serialize_paragraph_annotation(instance), status=201)


@login_required
@require_http_methods(["DELETE"])
def delete_par_annotation(request, id):
    user = request.user
    instance = ParAnnotation.objects.get(pk=id)
    if instance.user != user:
        return HttpResponseForbidden()
    instance.delete()
    return JsonResponse({"id": id}, status=200)


@login_required
@require_http_methods(["POST"])
def post_annotation_quality(request):
    data = loads(request.body.decode("utf-8"))
    data["user"] = request.user
    instance = deserialize_annotation_quality(data)
    instance.save()
    return JsonResponse(serialize_annotation_quality(instance), status=201)


@login_required
@require_http_methods(["POST"])
def post_img_annotation(request):
    data = loads(request.body.decode("utf-8"))
    data["user"] = request.user
    instance = deserialize_image_annotation(data)
    instance.save()
    return JsonResponse(serialize_image_annotation(instance), status=201)


@login_required
@require_http_methods(["POST"])
def create_path(request):
    instance = Path2.objects.create(user=request.user)
    return JsonResponse(serialize_path(instance), status=201)


@login_required
@require_http_methods(["POST"])
def close_path(request, id):
    instance = Path2.objects.get(pk=id)
    if request.user != instance.user:
        return HttpResponseForbidden()
    instance.closed = True
    instance.save()
    return JsonResponse(serialize_path(instance))


@login_required
@require_http_methods(["POST"])
def open_path(request, id):
    instance = Path2.objects.get(pk=id)
    if request.user != instance.user:
        return HttpResponseForbidden()
    instance.closed = False
    instance.save()
    return JsonResponse(serialize_path(instance))


@login_required
@require_http_methods(["POST"])
def name_path(request, id):
    data = loads(request.body.decode("utf-8"))
    instance = Path2.objects.get(pk=id)
    if request.user != instance.user:
        return HttpResponseForbidden()
    name = data.pop("name")
    instance.name = name
    instance.save()
    return JsonResponse(serialize_path(instance))


@login_required
@require_http_methods(["POST"])
def path_add_node(request, node_type, pid, tid):
    instance = Path2.objects.get(pk=pid)
    if request.user != instance.user:
        return HttpResponseForbidden()
    target = PathNode.get_target_from(node_type, tid)
    instance.add_node(node_type, target)
    return JsonResponse(serialize_path(instance))


@login_required
@require_http_methods(["POST"])
def path_swap_nodes(request, pid, nid1, nid2):
    instance = Path2.objects.get(pk=pid)
    if request.user != instance.user:
        return HttpResponseForbidden()
    instance.swap_nodes(nid1, nid2)
    return JsonResponse(serialize_path(instance))


@login_required
@require_http_methods(["DELETE"])
def path_delete_node(request, pid, nid):
    instance = Path2.objects.get(pk=pid)
    if request.user != instance.user:
        return HttpResponseForbidden()
    instance.delete_node(nid)
    return JsonResponse(serialize_path(instance))


@login_required
@require_http_methods(["POST"])
def create_transitional_text(request):
    data = loads(request.body.decode("utf-8"))
    content = str(data.pop("content"))
    tt = TransitionalText.objects.create(content=content)
    return JsonResponse(serialize_transitional_text(tt))


@login_required
def get_user_path_list(request):
    instances = Path2.objects.filter(user=request.user)
    data = [serialize_path(instance) for instance in instances]
    return JsonResponse(data)
