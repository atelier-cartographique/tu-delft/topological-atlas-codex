# Generated by Django 4.0 on 2022-04-26 09:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('codex', '0014_interviewrole_person_place_text_comment_text_date_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='image',
            name='comment',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='image',
            name='location',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='codex.place'),
        ),
    ]
