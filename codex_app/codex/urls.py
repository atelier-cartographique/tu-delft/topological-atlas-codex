from django.urls import path

from codex.views import (
    close_path,
    name_path,
    open_path,
    create_path,
    create_transitional_text,
    delete_par_annotation,
    get_distinct_terms,
    get_img_annotation_picto,
    get_user_path_list,
    path_add_node,
    path_delete_node,
    path_swap_nodes,
    post_annotation_quality,
    post_img_annotation,
    post_par_annotation,
    get_img_annotation_image,
)

urlpatterns = [
    path("terms/", get_distinct_terms),
    path(
        "image/annotation/image/<int:id>",
        get_img_annotation_image,
        name="get_img_annotation_image",
    ),
    path(
        "image/annotation/picto/<int:id>",
        get_img_annotation_picto,
        name="get_img_annotation_picto",
    ),
    path("paragraph/annotation/", post_par_annotation),
    path("paragraph/annotation/<int:id>", delete_par_annotation),
    path("image/annotation/", post_img_annotation),
    path("annotation/quality/", post_annotation_quality),
    # path
    path("path/user/", get_user_path_list),
    path("path/create/", create_path),
    path("path/close/<int:id>", close_path),
    path("path/open/<int:id>", open_path),
    path("path/name/<int:id>", name_path),
    path("path/delete/<int:pid>/<int:nid>", path_delete_node),
    path("path/swap/<int:pid>/<int:nid1>/<int:nid2>", path_swap_nodes),
    path("path/add/<node_type>/<int:pid>/<int:tid>", path_add_node),
    path("transitional/create/", create_transitional_text),
]
