from unicodedata import category
import uuid
from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from datetime import datetime
from django.db.models import CheckConstraint, Q, F
from django.apps import apps
from sorl.thumbnail import ImageField

# should there be scopes for terms ?

LANGUAGES = getattr(settings, "LANGUAGES")


class NamedMixin:
    def __str__(self) -> str:
        return self.name.replace("\n", " ")


class TermCategory(NamedMixin, models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(
        User, on_delete=models.PROTECT, related_name="+", null=True, blank=True
    )
    name = models.CharField(max_length=256)
    description = models.TextField()
    lang = models.CharField(max_length=12, choices=LANGUAGES)


class TermCurrentManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status=Term.CURRENT)


def _update_term_links(term):
    for rel in Relationship.objects.all():
        rel._update_version(term)
    for pa in ParAnnotation.objects.all():
        pa._update_version(term)
    for ia in ImageAnnotation.objects.all():
        ia._update_version(term)
    for cc in TermCatCat.objects.all():
        cc._update_version(term)
    PathNode = apps.get_model(app_label="codex", model_name="PathNode")
    for node in PathNode.objects.filter(type=PathNode.TERM):
        node._update_version(term)


class Term(NamedMixin, models.Model):
    DELETED = "del"
    REVISED = "rev"
    CURRENT = "cur"
    CREATED = "wip"

    STATUS = (
        (DELETED, "Deleted"),
        (REVISED, "Revised"),
        (CURRENT, "Current"),
        (CREATED, "In progress (internal)"),
    )

    objects = TermCurrentManager()
    versions = models.Manager()

    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(
        User, on_delete=models.PROTECT, related_name="+", null=True, blank=True
    )

    name = models.CharField(max_length=256)
    statement = models.TextField()
    lang = models.CharField(max_length=12, choices=LANGUAGES)
    is_relation = models.BooleanField()

    identifier = models.UUIDField(default=uuid.uuid4, editable=False)
    created_on = models.DateTimeField(auto_now_add=True, editable=False)
    eol = models.DateTimeField(editable=False, null=True, blank=True)
    status = models.CharField(
        max_length=3, choices=STATUS, default=CREATED, editable=False
    )

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        print(f"term#save id={self.id}, status={self.status}")
        if self.status == Term.CREATED:
            self.status = Term.CURRENT
            return super().save(
                force_insert=force_insert,
                force_update=force_update,
                using=using,
                update_fields=update_fields,
            )
        elif self.status == Term.CURRENT:
            new_version = Term.objects.create(
                user=self.user,
                name=self.name,
                statement=self.statement,
                lang=self.lang,
                is_relation=self.is_relation,
                identifier=self.identifier,
                created_on=self.created_on,
                eol=None,
                status=Term.CREATED,
            )

            self.status = Term.REVISED
            self.eol = datetime.now()
            super().save(
                force_insert=False,
                force_update=True,
                using=using,
                update_fields=["status", "eol"],
            )

            _update_term_links(new_version)

        else:
            raise Exception("Revised or deleted term cannot possibly saved again")

    def delete(self, using=None, keep_parents=False):
        self.status = Term.DELETED
        self.save_base(force_insert=False, force_update=True, update_fields=["status"])

    def use_count(self):
        return self.annotation.count()

    @property
    def identifier_str(self):
        return str(self.identifier)


class TermCatCat(models.Model):
    id = models.AutoField(primary_key=True)
    term = models.ForeignKey(Term, on_delete=models.CASCADE)
    category = models.ForeignKey(TermCategory, on_delete=models.CASCADE)

    def _update_version(self, term):
        if self.term.identifier == term.identifier:
            self.term = term
            self.save()


class RelationshipKindManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_relation=True, status=Term.CURRENT)


class RelationshipKind(Term):
    class Meta:
        proxy = True

    objects = RelationshipKindManager()


class Relationship(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.PROTECT, related_name="+", null=True, blank=True
    )
    source = models.ForeignKey(Term, related_name="source", on_delete=models.CASCADE)
    destination = models.ForeignKey(
        Term, related_name="destination", on_delete=models.CASCADE
    )
    kind = models.ForeignKey(RelationshipKind, on_delete=models.CASCADE)

    def _update_version(self, term):
        has_change = False
        if self.source.identifier == term.identifier:
            self.source = term
            has_change = True
        if self.destination.identifier == term.identifier:
            self.destination = term
            has_change = True
        if has_change:
            self.save()

    def sop_repr(self):
        return f"({self.source}, {self.destination}, {self.kind}))"

    def __str__(self) -> str:
        return self.sop_repr()


class Change(models.Model):
    id = models.AutoField(primary_key=True)
    term = models.ForeignKey(Term, on_delete=models.CASCADE)
    message = models.TextField()
    created_on = models.DateTimeField(auto_created=True)


class Message(models.Model):
    id = models.AutoField(primary_key=True)
    author = models.TextField()
    term = models.ForeignKey(Term, on_delete=models.CASCADE)
    content = models.TextField()
    created_on = models.DateTimeField(auto_created=True)


class Agreement(models.Model):
    id = models.AutoField(primary_key=True)
    term = models.ForeignKey(Term, on_delete=models.CASCADE)
    party = models.TextField()
    created_on = models.DateTimeField(auto_created=True)


class Place(NamedMixin, models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=512)


class PlaceAlias(NamedMixin, models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=512)
    place = models.ForeignKey(Place, related_name="alias", on_delete=models.CASCADE)


class Recording(NamedMixin, models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(
        User, on_delete=models.PROTECT, related_name="+", null=True, blank=True
    )
    name = models.CharField(max_length=512)
    file = models.FileField(upload_to="recordings/%Y/%m/%d/")
    location = models.ForeignKey(Place, on_delete=models.PROTECT, null=True, blank=True)
    comment = models.TextField(null=True, blank=True)


class Text(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(
        User, on_delete=models.PROTECT, related_name="+", null=True, blank=True
    )
    record = models.ForeignKey(
        Recording, on_delete=models.PROTECT, null=True, blank=True
    )
    title = models.CharField(max_length=255)
    date = models.DateField(null=True, blank=True)
    location = models.ForeignKey(Place, on_delete=models.PROTECT, null=True, blank=True)
    comment = models.TextField(null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return self.title

    def first_paragraph(self):
        try:
            return self.paragraph_set.order_by("order").first().content
        except:
            return "???"

    def annotations(self):
        annotations = []
        for par in self.paragraph_set.all():
            annotations.extend(list(par.annotation.all()))
        return annotations

    def annotation_count(self):
        n = 0
        for par in self.paragraph_set.all():
            n += par.annotation.count()
        return n


class Person(NamedMixin, models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)


class InterviewRole(NamedMixin, models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)


class InterviewParticipant(models.Model):
    class Meta:
        ordering = ["text"]

    id = models.AutoField(primary_key=True)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    role = models.ForeignKey(InterviewRole, on_delete=models.CASCADE)
    text = models.ForeignKey(
        Text, on_delete=models.CASCADE, related_name="participants"
    )

    def __str__(self) -> str:
        return f"{self.person}, {self.role}"


class ParAnnotation(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(
        User, on_delete=models.PROTECT, related_name="+", null=True, blank=True
    )
    paragraph = models.ForeignKey(
        "Paragraph", related_name="annotation", on_delete=models.CASCADE
    )
    term = models.ForeignKey(Term, related_name="annotation", on_delete=models.CASCADE)
    start = models.IntegerField()
    end = models.IntegerField()
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return f"{self.paragraph.text.title} ({self.paragraph.order}) [{self.start}, {self.end}]"

    def _update_version(self, term):
        if self.term.identifier == term.identifier:
            self.term = term
            self.save()

    def overlaps(self):
        others = []
        for o in self.paragraph.annotation.all():
            if (
                o.id != self.id
                and self.start <= o.end
                and self.end >= o.start
                and (o.term.id != self.term.id)
            ):
                others.append(o)
        return others

    @property
    def content(self):
        return self.paragraph.clean_content[self.start : self.end]


class Paragraph(models.Model):
    class Meta:
        ordering = ["text", "order"]

    id = models.AutoField(primary_key=True)
    text = models.ForeignKey(Text, on_delete=models.CASCADE)
    participant = models.ForeignKey(
        InterviewParticipant,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        # limit_choices_to=Q(paragraph__participant__in=F("text__participants")),
    )

    record_start = models.IntegerField(default=0)
    record_end = models.IntegerField(default=0)
    content = models.TextField()
    order = models.IntegerField()

    def __str__(self) -> str:
        return self.first_words()

    def first_words(self):
        return " ".join(self.content.split(" ")[:6])

    @property
    def clean_content(self):
        return self.content.replace("\r", "")


class Image(NamedMixin, models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(
        User, on_delete=models.PROTECT, related_name="+", null=True, blank=True
    )
    name = models.CharField(max_length=512)
    file = ImageField(upload_to="images/%Y/%m/%d/")
    location = models.ForeignKey(Place, on_delete=models.PROTECT, null=True, blank=True)
    comment = models.TextField(null=True, blank=True)

    def __str__(self) -> str:
        return self.name.replace("_", " ")


class ImageAnnotation(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(
        User, on_delete=models.PROTECT, related_name="+", null=True, blank=True
    )
    image = models.ForeignKey("Image", on_delete=models.CASCADE)
    term = models.ForeignKey(
        Term,
        related_name="images",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    category = models.ForeignKey(
        TermCategory,
        on_delete=models.CASCADE,
        related_name="images",
        null=True,
        blank=True,
    )
    created_on = models.DateTimeField(auto_now_add=True)
    triangles = models.JSONField()

    def __str__(self) -> str:
        if self.term is not None:
            return f"Term({self.term.name})"
        return f"Category({self.category.name})"

    def _update_version(self, term):
        if self.term is not None and self.term.identifier == term.identifier:
            self.term = term
            self.save()


class Path(NamedMixin, models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.PROTECT, related_name="+")
    name = models.CharField(max_length=512)


class PathPart(models.Model):
    class Meta:
        ordering = ["path", "order"]

    id = models.AutoField(primary_key=True)
    path = models.ForeignKey(Path, on_delete=models.CASCADE, related_name="parts")
    relationship = models.ForeignKey(
        Relationship, on_delete=models.CASCADE, related_name="+"
    )
    order = models.IntegerField()

    def sop_repr(self):
        return self.relationship.sop_repr()


class Qualifier(NamedMixin, models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=256)


class AnnotationQuality(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(
        User, on_delete=models.PROTECT, related_name="+", null=True, blank=True
    )
    qualifier = models.ForeignKey(Qualifier, related_name="+", on_delete=models.CASCADE)
    annotation = models.ForeignKey(
        ParAnnotation, related_name="quality", on_delete=models.CASCADE
    )
