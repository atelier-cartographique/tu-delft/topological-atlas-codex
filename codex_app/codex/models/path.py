from django.db import models
from django.contrib.auth.models import User

from codex.models.codex import Term, Image, ParAnnotation, ImageAnnotation


class Path2(models.Model):
    class Meta:
        verbose_name = "Path"
        verbose_name_plural = "Paths"

    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(
        User, on_delete=models.PROTECT, related_name="+", null=True, blank=True
    )
    name = models.CharField(max_length=256, null=True, blank=True)
    closed = models.BooleanField(default=False)

    def add_node(self, node_type, target):
        if self.nodes.count() == 0:
            order = 0
        else:
            order = self.nodes.last().order + 1

        return PathNode.objects.create(
            order=order, path=self, type=node_type, tid=target.id
        )

    def swap_nodes(self, node_id1, node_id2):
        node1 = self.nodes.get(pk=node_id1)
        node2 = self.nodes.get(pk=node_id2)
        tmp = node1.order
        node1.order = node2.order
        node2.order = tmp
        node1.save()
        node2.save()

    def delete_node(self, node_id):
        node = self.nodes.get(pk=node_id)
        node.delete()

    def terms(self):
        return self.nodes.filter(type=PathNode.TERM)

    def images(self):
        return self.nodes.filter(type=PathNode.IMAGE)

    def text_annotations(self):
        return self.nodes.filter(type=PathNode.TEXT_ANNOTATION)

    def image_annotations(self):
        return self.nodes.filter(type=PathNode.IMAGE_ANNOTATION)

    def transitional_texts(self):
        return self.nodes.filter(type=PathNode.TRANSITIONAL_TEXT)

    def __str__(self) -> str:
        return f"{self.name}"


class TransitionalText(models.Model):
    id = models.AutoField(primary_key=True)
    content = models.TextField()


class PathNode(models.Model):
    TERM = "te"
    IMAGE = "im"
    TEXT_ANNOTATION = "ta"
    IMAGE_ANNOTATION = "ia"
    TRANSITIONAL_TEXT = "tt"
    TYPES = (
        (TERM, "Term"),
        (IMAGE, "Image"),
        (TEXT_ANNOTATION, "Text annotation"),
        (IMAGE_ANNOTATION, "Image annotation"),
        (TRANSITIONAL_TEXT, "Transitional text"),
    )

    MODELS = {
        TERM: Term,
        IMAGE: Image,
        TEXT_ANNOTATION: ParAnnotation,
        IMAGE_ANNOTATION: ImageAnnotation,
        TRANSITIONAL_TEXT: TransitionalText,
    }

    id = models.AutoField(primary_key=True)
    order = models.IntegerField()
    path = models.ForeignKey(Path2, related_name="nodes", on_delete=models.CASCADE)
    type = models.CharField(max_length=2, choices=TYPES)
    tid = models.IntegerField()

    class Meta:
        ordering = ["order"]

    @staticmethod
    def get_target_from(node_type, target_id):
        if node_type in PathNode.MODELS:
            return PathNode.MODELS[node_type].objects.get(pk=target_id)
        known_types = "; ".join([short for short, long in PathNode.TYPES])
        raise TypeError(f'Expected one of [{known_types}], got "{node_type}"')

    def get_target(self):
        return PathNode.get_target_from(self.type, self.tid)

    def __str__(self) -> str:
        # for reasons, it might happen that the target does not exist
        try:
            return str(self.get_target())
        except Exception:
            return f"{self.type}/{self.tid}"

    def _update_version(self, term):
        if self.type == self.TERM:
            versioned = Term.versions.get(id=self.tid)
            if versioned.identifier == term.identifier:
                self.tid = term.id
                self.save()
