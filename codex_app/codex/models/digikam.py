# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
# from django.contrib.gis.db import models
from django.db import models
from django.conf import settings

DIGIKAM_CONNECTION = getattr(settings, "DIGIKAM_CONNECTION", "digikam")


class Managed(models.Model):
    class Meta:
        abstract = True

    objects = models.Manager().db_manager(DIGIKAM_CONNECTION)


class Albumroots(Managed):
    label = models.TextField(blank=True, null=True)
    status = models.IntegerField()
    type = models.IntegerField()
    identifier = models.TextField(blank=True, null=True)
    specificpath = models.TextField(
        db_column="specificPath", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "AlbumRoots"


class Albums(Managed):
    albumroot = models.IntegerField(db_column="albumRoot")  # Field name made lowercase.
    relativepath = models.TextField(
        db_column="relativePath"
    )  # Field name made lowercase.
    date = models.DateField(blank=True, null=True)
    caption = models.TextField(blank=True, null=True)
    collection = models.TextField(blank=True, null=True)
    icon = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "Albums"


class Downloadhistory(Managed):
    identifier = models.TextField(blank=True, null=True)
    filename = models.TextField(blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    filedate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "DownloadHistory"


class Imagecomments(Managed):
    imageid = models.IntegerField()
    type = models.IntegerField(blank=True, null=True)
    language = models.TextField(blank=True, null=True)
    author = models.TextField(blank=True, null=True)
    date = models.DateTimeField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "ImageComments"


class Imagecopyright(Managed):
    imageid = models.IntegerField()
    property = models.TextField(blank=True, null=True)
    value = models.TextField(blank=True, null=True)
    extravalue = models.TextField(
        db_column="extraValue", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "ImageCopyright"


class Imagehistory(Managed):
    imageid = models.AutoField(primary_key=True)
    uuid = models.TextField(blank=True, null=True)
    history = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "ImageHistory"


class Imageinformation(Managed):
    imageid = models.AutoField(primary_key=True)
    rating = models.IntegerField(blank=True, null=True)
    creationdate = models.DateTimeField(
        db_column="creationDate", blank=True, null=True
    )  # Field name made lowercase.
    digitizationdate = models.DateTimeField(
        db_column="digitizationDate", blank=True, null=True
    )  # Field name made lowercase.
    orientation = models.IntegerField(blank=True, null=True)
    width = models.IntegerField(blank=True, null=True)
    height = models.IntegerField(blank=True, null=True)
    format = models.TextField(blank=True, null=True)
    colordepth = models.IntegerField(
        db_column="colorDepth", blank=True, null=True
    )  # Field name made lowercase.
    colormodel = models.IntegerField(
        db_column="colorModel", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "ImageInformation"


class Imagemetadata(Managed):
    imageid = models.AutoField(primary_key=True)
    make = models.TextField(blank=True, null=True)
    model = models.TextField(blank=True, null=True)
    lens = models.TextField(blank=True, null=True)
    aperture = models.FloatField(blank=True, null=True)
    focallength = models.FloatField(
        db_column="focalLength", blank=True, null=True
    )  # Field name made lowercase.
    focallength35 = models.FloatField(
        db_column="focalLength35", blank=True, null=True
    )  # Field name made lowercase.
    exposuretime = models.FloatField(
        db_column="exposureTime", blank=True, null=True
    )  # Field name made lowercase.
    exposureprogram = models.IntegerField(
        db_column="exposureProgram", blank=True, null=True
    )  # Field name made lowercase.
    exposuremode = models.IntegerField(
        db_column="exposureMode", blank=True, null=True
    )  # Field name made lowercase.
    sensitivity = models.IntegerField(blank=True, null=True)
    flash = models.IntegerField(blank=True, null=True)
    whitebalance = models.IntegerField(
        db_column="whiteBalance", blank=True, null=True
    )  # Field name made lowercase.
    whitebalancecolortemperature = models.IntegerField(
        db_column="whiteBalanceColorTemperature", blank=True, null=True
    )  # Field name made lowercase.
    meteringmode = models.IntegerField(
        db_column="meteringMode", blank=True, null=True
    )  # Field name made lowercase.
    subjectdistance = models.FloatField(
        db_column="subjectDistance", blank=True, null=True
    )  # Field name made lowercase.
    subjectdistancecategory = models.IntegerField(
        db_column="subjectDistanceCategory", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "ImageMetadata"


class Imagepositions(Managed):
    imageid = models.AutoField(primary_key=True)
    latitude = models.TextField(blank=True, null=True)
    latitudenumber = models.FloatField(
        db_column="latitudeNumber", blank=True, null=True
    )  # Field name made lowercase.
    longitude = models.TextField(blank=True, null=True)
    longitudenumber = models.FloatField(
        db_column="longitudeNumber", blank=True, null=True
    )  # Field name made lowercase.
    altitude = models.FloatField(blank=True, null=True)
    orientation = models.FloatField(blank=True, null=True)
    tilt = models.FloatField(blank=True, null=True)
    roll = models.FloatField(blank=True, null=True)
    accuracy = models.FloatField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "ImagePositions"


class Imageproperties(Managed):
    imageid = models.IntegerField()
    property = models.TextField()
    value = models.TextField()

    class Meta:
        managed = False
        db_table = "ImageProperties"


class Imagerelations(Managed):
    subject = models.IntegerField(blank=True, null=True)
    object = models.IntegerField(blank=True, null=True)
    type = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "ImageRelations"


class Imagetagproperties(Managed):
    imageid = models.IntegerField()
    tagid = models.IntegerField(blank=True, null=True)
    property = models.TextField(blank=True, null=True)
    value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "ImageTagProperties"


class Imagetags(Managed):
    imageid = models.IntegerField(primary_key=True)
    tagid = models.IntegerField()

    class Meta:
        managed = False
        db_table = "ImageTags"


class Images(Managed):
    album = models.IntegerField(blank=True, null=True)
    name = models.TextField()
    status = models.IntegerField()
    category = models.IntegerField()
    modificationdate = models.DateTimeField(
        db_column="modificationDate", blank=True, null=True
    )  # Field name made lowercase.
    filesize = models.IntegerField(
        db_column="fileSize", blank=True, null=True
    )  # Field name made lowercase.
    uniquehash = models.TextField(
        db_column="uniqueHash", blank=True, null=True
    )  # Field name made lowercase.
    manualorder = models.IntegerField(
        db_column="manualOrder", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "Images"


class Searches(Managed):
    type = models.IntegerField(blank=True, null=True)
    name = models.TextField()
    query = models.TextField()

    class Meta:
        managed = False
        db_table = "Searches"


class Settings(Managed):
    keyword = models.TextField(unique=True)
    value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "Settings"


class Tagproperties(Managed):
    tagid = models.IntegerField(blank=True, null=True)
    property = models.TextField(blank=True, null=True)
    value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "TagProperties"


class Tags(Managed):
    pid = models.IntegerField(blank=True, null=True)
    name = models.TextField()
    icon = models.IntegerField(blank=True, null=True)
    iconkde = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "Tags"


class Tagstree(Managed):
    id = models.IntegerField(primary_key=True)
    pid = models.IntegerField()

    class Meta:
        managed = False
        db_table = "TagsTree"


class Videometadata(Managed):
    imageid = models.AutoField(primary_key=True)
    aspectratio = models.TextField(
        db_column="aspectRatio", blank=True, null=True
    )  # Field name made lowercase.
    audiobitrate = models.TextField(
        db_column="audioBitRate", blank=True, null=True
    )  # Field name made lowercase.
    audiochanneltype = models.TextField(
        db_column="audioChannelType", blank=True, null=True
    )  # Field name made lowercase.
    audiocompressor = models.TextField(
        db_column="audioCompressor", blank=True, null=True
    )  # Field name made lowercase.
    duration = models.TextField(blank=True, null=True)
    framerate = models.TextField(
        db_column="frameRate", blank=True, null=True
    )  # Field name made lowercase.
    exposureprogram = models.IntegerField(
        db_column="exposureProgram", blank=True, null=True
    )  # Field name made lowercase.
    videocodec = models.TextField(
        db_column="videoCodec", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "VideoMetadata"
