from datetime import timedelta, datetime
from collections import namedtuple
from django.conf import settings


class CacheMiss(Exception):
    pass


CacheRecord = namedtuple("CacheRecord", ["item", "eol"])


class Cache:
    def __init__(self, seconds):
        self._data = dict()
        self._lifetime = timedelta(seconds=seconds)

    def push(self, key, item):
        self._data[key] = CacheRecord(item, datetime.now() + self._lifetime)
        return item

    def find(self, key):
        if key not in self._data:
            raise CacheMiss()
        record = self._data[key]
        now = datetime.now()
        if record.eol < now:
            self._data.pop(key)
            raise CacheMiss()

        return record.item

    def find_or_else(self, key, default):
        try:
            return self.find(key)
        except CacheMiss:
            return self.push(key, default())

    def clear_all(self):
        self._data = dict()
