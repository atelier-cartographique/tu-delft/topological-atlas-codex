#  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from pathlib import PosixPath
from django.core.management.base import BaseCommand
from codex.models.codex import Place, PlaceAlias

import csv


def alias(row):
    a = row[3]
    if len(a):
        return a
    return None


def name(row):
    n = row[1]
    if len(n):
        return n
    return None


def process_rows(rows):
    for i, row in enumerate(rows):
        n = name(row)
        if n is None:
            continue
        try:
            place = Place.objects.get(name__iexact=n)
        except Place.DoesNotExist:
            place = Place.objects.create(name=n)
        except Exception as ex:
            print("OOps")
            print(n)
            print(str(ex))
            continue
        a = alias(row)
        if a is not None:
            PlaceAlias.objects.create(place=place, name=a)
            for nextrow in rows[i + 1 :]:
                nn = name(nextrow)
                aa = alias(nextrow)
                if nn is None and aa is not None:
                    PlaceAlias.objects.create(place=place, name=aa)
                else:
                    break


class Command(BaseCommand):
    help = """Import places
"""

    output_transaction = True

    def add_arguments(self, parser):
        parser.add_argument("csv")

    def handle(self, *args, **options):
        csv_path = PosixPath(options["csv"])
        with csv_path.open() as f:
            reader = csv.reader(f)
            process_rows([row for row in reader])
