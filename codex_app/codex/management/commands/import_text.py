#
#  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from pathlib import PosixPath
from django.core.management.base import BaseCommand
import re

from codex.models import Text, Paragraph

"""
- remplacer \' par une apostrophe
-  \... par ...
- \] par \]
- \[ par \[
- Ne pas prendre en compte les lignes qui commencent par un #
(NO) - Ne pas prendre en compte les phrases entre double astérisque en début de ligne  **phrase**
"""

re_apos = re.compile(r"\\'")
re_brak_open = re.compile(r"\\\[")
re_brak_close = re.compile(r"\\]")
re_ellipsis = re.compile(r"\\...")


def replace_apostrophe(text):
    return re_apos.sub("’", text)


def replace_ellipsis(text):
    return re_ellipsis.sub("…", text)


def replace_brakets(text):
    return re_brak_close.sub("]", re_brak_open.sub("[", text))


def process_paragraph(text):
    return replace_ellipsis(replace_brakets(replace_apostrophe(text)))


def is_comment(text):
    return text.startswith("#")


class Command(BaseCommand):
    help = """Import a text
"""

    output_transaction = True

    def add_arguments(self, parser):
        parser.add_argument("text_file")
        # parser.add_argument("title")
        parser.add_argument(
            "--sep",
            action="store",
            help="How many newline characters make the paragraph separation",
            default=2,
        )

    def handle(self, *args, **options):
        file_path = PosixPath(options["text_file"])
        title = file_path.stem
        sep = "\n" * int(options["sep"])
        text = Text.objects.create(title=title)
        try:
            with file_path.open() as file:
                body = "\n".join(
                    filter(
                        lambda line: is_comment(line) is False, file.read().split("\n")
                    )
                )
                for i, chunk in enumerate(body.split(sep)):
                    chunk = chunk.strip()
                    if len(chunk) > 1:
                        Paragraph.objects.create(
                            content=process_paragraph(chunk),
                            text=text,
                            order=(i + 1) * 10,
                        )
        except Exception as ex:
            self.stdout.write(
                self.style.ERROR(
                    f"FAILED to import paragraphs for  #{text.id}: {text.title}"
                )
            )
            return
        self.stdout.write(self.style.SUCCESS(f"Text #{text.id}: {text.title}"))
        self.stdout.write(f"created {i} paragraphs")
