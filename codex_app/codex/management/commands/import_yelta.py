#
#  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from os import name
from pathlib import PosixPath
from django.core.management.base import BaseCommand
from codex.models import Image, User, Place
from codex.models import digikam as dk
from django.core.files import File

SUPPORTED = ["JPEG", "JPG", "PNG"]


class Command(BaseCommand):
    help = """Import Yelta's images
"""

    output_transaction = True

    def add_arguments(self, parser):
        parser.add_argument("root")
        parser.add_argument("album", type=int)
        parser.add_argument("user")
        # parser.add_argument(
        #     "--sep",
        #     action="store",
        #     help="How many newline characters make the paragraph separation",
        #     default=2,
        # )

    def handle(self, *args, **options):
        root_path = PosixPath(options["root"])
        user = User.objects.get(username=options["user"])
        album = options["album"]
        images = dk.Images.objects.filter(album=album)
        places = dk.Tags.objects.get(name="Places")

        for i in images:
            md = dk.Imageinformation.objects.get(imageid=i.id)
            if md.format not in SUPPORTED:
                continue
            its = dk.Imagetags.objects.filter(imageid=i.id)
            for it in its:
                tag = dk.Tags.objects.get(id=it.tagid)
                if tag.pid == places.id:
                    try:
                        place = Place.objects.get(name=tag.name)
                    except Exception:
                        place = Place.objects.create(name=tag.name)
                    fp = root_path.joinpath(i.name.decode())

                    with fp.open("rb") as f:
                        df = File(f)
                        name = "{} {}".format(i.id, md.creationdate.isoformat())
                        df.name = fp.name
                        img = Image.objects.create(
                            name=name,
                            user=user,
                            location=place,
                            file=df,
                            comment="Imported",
                        )
                        self.stdout.write(
                            self.style.SUCCESS(f"Image #{img.id}: {img.name}")
                        )
