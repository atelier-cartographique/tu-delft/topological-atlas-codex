from codex.models import (
    Image,
    ImageAnnotation,
    Term,
    Paragraph,
    ParAnnotation,
    TermCategory,
    Path2,
    PathNode,
    AnnotationQuality,
    Qualifier,
    Place,
    PlaceAlias,
    TransitionalText,
)
from sorl.thumbnail import get_thumbnail


def serialize_term(term: Term):
    return {
        "id": term.pk,
        "uuid": str(term.identifier),
        "name": term.name,
        "statement": term.statement,
        "lang": term.lang,
        "categories": [cc.category.id for cc in term.termcatcat_set.all()],
    }


def serialize_category(category: TermCategory):
    return {
        "id": category.pk,
        "name": category.name,
        "description": category.description,
        "lang": category.lang,
    }


def serialize_paragraph(par: Paragraph):
    return {
        "id": par.id,
        "index": par.order,
        "textId": par.text.id,
        "content": par.clean_content,
    }


def serialize_paragraph_annotation(a: ParAnnotation):
    return {
        "id": a.id,
        "user": a.user.id,
        "parId": a.paragraph.id,
        "termId": a.term.id,
        "textId": a.paragraph.text.id,
        "start": a.start,
        "end": a.end,
        "content": a.content,
        "qualities": [q.qualifier.id for q in a.quality.all()],
    }


def serialize_qualifier(q: Qualifier):
    return {
        "id": q.id,
        "name": q.name,
    }


def serialize_annotation_quality(q: AnnotationQuality):
    return {
        "id": q.id,
        "qualifierId": q.qualifier.id,
        "annotationId": q.annotation.id,
    }


def serialize_image_annotation(a: ImageAnnotation):
    if a.category is None:
        tag = "term"
        term_id = a.term.id
        cat_id = None
    else:
        tag = "category"
        term_id = None
        cat_id = a.category.id
    return {
        "tag": tag,
        "id": a.id,
        "user": a.user.id,
        "categoryId": cat_id,
        "termId": term_id,
        "imageId": a.image.id,
        "triangles": a.triangles,
    }


def serialize_image(i: Image):
    file = i.file
    tw, th = file.width // 4, file.height // 4
    thumbnail = get_thumbnail(file, f"{tw}x{th}", quality=99)
    return {
        "id": i.id,
        "name": i.name,
        "file": {
            "url": file.url,
            "width": file.width,
            "height": file.height,
        },
        "thumbnail": {
            "url": thumbnail.url,
            "width": thumbnail.width,
            "height": thumbnail.height,
        },
        "location": i.location.id if i.location is not None else None,
    }


def serialize_pathnode(node: PathNode):
    return {
        "id": node.id,
        "type": node.type,
        "tid": node.tid,
    }


def serialize_path(path: Path2):
    return {
        "id": path.id,
        "name": path.name,
        "closed": path.closed,
        "nodes": [serialize_pathnode(n) for n in path.nodes.all()],
        "username": path.user.username,
    }


def serialize_place_alias(alias: PlaceAlias):
    return alias.name


def serialize_transitional_text(text: TransitionalText):
    return {"id": text.id, "content": text.content}


def _find_feature(pid, features):
    for f in features:
        if f["properties"]["place_id"] == pid:
            return f["geometry"]
    return None


def serialize_place(place: Place, features):
    return {
        "id": place.id,
        "name": place.name,
        "alias": [serialize_place_alias(a) for a in place.alias.all()],
        "point": _find_feature(place.id, features),
    }


def deserialize_paragraph_annotation(data):
    paragraph = Paragraph.objects.get(pk=data["parId"])
    term = Term.objects.get(pk=data["termId"])
    return ParAnnotation(
        paragraph=paragraph,
        term=term,
        user=data["user"],
        start=data["start"],
        end=data["end"],
    )


def deserialize_annotation_quality(data):
    annotation = ParAnnotation.objects.get(pk=data["annotationId"])
    qualifier = Qualifier.objects.get(pk=data["qualifierId"])
    return AnnotationQuality(
        user=data["user"],
        annotation=annotation,
        qualifier=qualifier,
    )


def deserialize_image_annotation(data):
    image = Image.objects.get(pk=data["imageId"])
    if "categoryId" in data:
        category = TermCategory.objects.get(pk=data["categoryId"])
    else:
        category = None
    if "termId" in data:
        term = Term.objects.get(pk=data["termId"])
    else:
        term = None
    triangles = data["triangles"]
    return ImageAnnotation(
        image=image,
        category=category,
        term=term,
        user=data["user"],
        triangles=triangles,
    )
