from django.contrib import admin
from django import forms
from codex.models import (
    ImageAnnotation,
    ParAnnotation,
    Term,
    Relationship,
    Text,
    TermCategory,
    Paragraph,
    Image,
    Place,
    InterviewParticipant,
    InterviewRole,
    Person,
    Recording,
    Path,
    PathPart,
    TermCatCat,
    Path2,
    PathNode,
    Qualifier,
)


class TermCatInline(admin.StackedInline):
    model = TermCatCat


class TermAdmin(admin.ModelAdmin):
    list_display = ("name", "is_relation", "lang", "statement")
    inlines = [TermCatInline]


def image_id(a: ImageAnnotation):
    return str(a.image.id)


class ImageAnnotationAdmin(admin.ModelAdmin):
    list_display = ("id", image_id, "__str__")


class RelationshipAdmin(admin.ModelAdmin):
    list_display = ("source", "destination", "kind")


class InterviewParticipantAdmin(admin.ModelAdmin):
    list_display = ("text", "person", "role")


class InterviewParticipantInline(admin.TabularInline):
    model = InterviewParticipant


class ParagraphInline(admin.StackedInline):
    # template = "codex/tabular-paragraph.html"
    model = Paragraph
    fieldsets = (
        ("text", {"fields": ("participant",)}),
        (
            "audio",
            {
                # "classes": ("collapse",),
                "fields": ("record_start", "record_end")
            },
        ),
        (
            "paragraph",
            {
                "fields": ("content", "order"),
            },
        ),
    )

    def get_formset(self, request, obj=None, **kwargs):
        # print("get_form", obj)
        form_set = super().get_formset(request, obj, **kwargs)
        try:
            form_set.form.base_fields["participant"].queryset = obj.participants
        except Exception:
            pass
        return form_set


class TextAdmin(admin.ModelAdmin):
    list_display = ("title",)
    inlines = [InterviewParticipantInline, ParagraphInline]


class ParagraphAdmin(admin.ModelAdmin):
    list_display = ("first_words", "order", "text")

    def get_form(self, request, obj=None, change=False, **kwargs):
        form = super().get_form(request, obj, change, **kwargs)
        form.base_fields["participant"].queryset = obj.text.participants
        return form


class PathPartAdmin(admin.ModelAdmin):
    list_display = (
        "path",
        "sop_repr",
        "order",
    )


class QualifierAdmin(admin.ModelAdmin):
    list_display = ("name",)


class PathPartInline(admin.TabularInline):
    model = PathPart


# class PathAdmin(admin.ModelAdmin):
#     inlines = [
#         PathPartInline,
#     ]


class PathNodeInline(admin.TabularInline):
    model = PathNode


class Path2Admin(admin.ModelAdmin):
    list_display = ("name", "user", "closed")
    inlines = [
        PathNodeInline,
    ]


admin.site.register(Term, TermAdmin)
admin.site.register(Relationship, RelationshipAdmin)
admin.site.register(Text, TextAdmin)
admin.site.register(TermCategory)
# admin.site.register(Agreement)
# admin.site.register(Change)
# admin.site.register(Message)
admin.site.register(Paragraph, ParagraphAdmin)
admin.site.register(ParAnnotation)
admin.site.register(Image)
admin.site.register(ImageAnnotation, ImageAnnotationAdmin)
admin.site.register(Place)
admin.site.register(Person)
admin.site.register(InterviewParticipant, InterviewParticipantAdmin)
admin.site.register(InterviewRole)
admin.site.register(Recording)
# admin.site.register(Path, PathAdmin)
# admin.site.register(PathPart, PathPartAdmin)

admin.site.register(Path2, Path2Admin)
# admin.site.register(PathNode)
admin.site.register(Qualifier, QualifierAdmin)
