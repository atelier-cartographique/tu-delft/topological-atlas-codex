from io import BytesIO
from PIL import Image, ImageDraw
from codex.models import ImageAnnotation

GRID_SIZE = 6 * 4
TRIANGLE_SIDE = 100


def coords0(x, y):
    return [
        (x, y),
        (x + TRIANGLE_SIDE, y),
        (x, y + TRIANGLE_SIDE),
        (x, y),
    ]


def coords1(x, y):
    return [
        (x + TRIANGLE_SIDE, y),
        (x + TRIANGLE_SIDE, y + TRIANGLE_SIDE),
        (x, y + TRIANGLE_SIDE),
        (x, y),
    ]


def coords2(x, y):
    return [
        (x, y),
        (x + TRIANGLE_SIDE, y + TRIANGLE_SIDE),
        (x, y + TRIANGLE_SIDE),
        (x, y),
    ]


def coords3(x, y):
    return [
        (x, y),
        (x + TRIANGLE_SIDE, y),
        (x + TRIANGLE_SIDE, y + TRIANGLE_SIDE),
        (x, y),
    ]


def next0(x, y):
    return x, y


def next1(x, y):
    return x + TRIANGLE_SIDE, y


def next2(x, y):
    return x, y


def next3(x, y):
    return x + TRIANGLE_SIDE, y


def pick(n):
    if n == 0:
        return coords0, next0
    elif n == 1:
        return coords1, next1
    elif n == 2:
        return coords2, next2
    elif n == 3:
        return coords3, next3


class Triangle:
    def __init__(self, coords, x, y, index) -> None:
        self.index = index
        self._coords = coords(x, y)

    def polygon(self, x_scale, y_scale):
        return list(map(lambda c: (x_scale * c[0], y_scale * c[1]), self._coords))

    def bbox(self, x_scale, y_scale):
        minx = 9999999999.0
        miny = 9999999999.0
        maxx = -minx
        maxy = -miny

        for x, y in self.polygon(x_scale, y_scale):
            minx = min(minx, x)
            miny = min(miny, y)
            maxx = max(maxx, x)
            maxy = max(maxy, y)

        return minx, miny, maxx, maxy


def merge_rects(a, b):
    if a is None:
        return b
    elif b is None:
        return a
    return (
        min(a[0], b[0]),
        min(a[1], b[1]),
        max(a[2], b[2]),
        max(a[3], b[3]),
    )


# class Row:
#     def __init__(self, y) -> None:
#         self.y = y
#         self.x = 0


def row(y, index):
    x = 0
    for i in range(GRID_SIZE):
        coords, nex = pick(i % 4)
        yield Triangle(coords, x, y, index + i)
        x, y = nex(x, y)


def clip_with_annotation(annotation: ImageAnnotation):
    with Image.open(annotation.image.file.path) as rgb_im:
        gridWidth = (GRID_SIZE * TRIANGLE_SIDE) / 2
        gridHeight = GRID_SIZE * TRIANGLE_SIDE
        im = rgb_im.convert(mode="RGBA")
        xScale = im.width / gridWidth
        yScale = im.height / gridHeight
        triangle_indices = annotation.triangles
        draw = ImageDraw.Draw(im)
        bbox = None
        for i in range(GRID_SIZE):
            for triangle in row(i * TRIANGLE_SIDE, i * GRID_SIZE):
                if triangle.index in triangle_indices:
                    bbox = merge_rects(bbox, triangle.bbox(xScale, yScale))
                    continue
                draw.polygon(triangle.polygon(xScale, yScale), fill=(255, 255, 255, 0))

        stream = BytesIO()
        im.crop(bbox).save(stream, format="PNG")
        stream.seek(0)
        return stream


def picto_with_annotation(annotation: ImageAnnotation, color):
    im = Image.new("RGBA", (64 * 4, 64 * 4), color)
    gridWidth = (GRID_SIZE * TRIANGLE_SIDE) / 2
    gridHeight = GRID_SIZE * TRIANGLE_SIDE
    xScale = im.width / gridWidth
    yScale = im.height / gridHeight
    triangle_indices = annotation.triangles
    draw = ImageDraw.Draw(im)
    bbox = None
    for i in range(GRID_SIZE):
        for triangle in row(i * TRIANGLE_SIDE, i * GRID_SIZE):
            if triangle.index in triangle_indices:
                bbox = merge_rects(bbox, triangle.bbox(xScale, yScale))
                continue
            draw.polygon(triangle.polygon(xScale, yScale), fill=(0, 0, 0, 0))

    stream = BytesIO()
    im.crop(bbox).save(stream, format="PNG")
    stream.seek(0)
    return stream
