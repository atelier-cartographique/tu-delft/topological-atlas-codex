"""codex_app URL Configuration
"""
from django.urls import path

from .views import (
    get_index,
)


def prefix(p):
    return f"topo.{p}"


urlpatterns = [
    path(
        "index.html",
        get_index,
        name=prefix("index"),
    ),
]
