from django.apps import AppConfig


class TopoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'topo'
