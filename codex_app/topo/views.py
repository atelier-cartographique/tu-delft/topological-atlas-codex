import os

from django.shortcuts import render
from codex.cache import Cache
from codex.models.codex import (
    Image,
    ImageAnnotation,
    Term,
    TermCategory,
    ParAnnotation,
    Place,
)
from codex.models.path import Path2
from codex.serializers import (
    serialize_image,
    serialize_image_annotation,
    serialize_path,
    serialize_place,
    serialize_paragraph_annotation,
    serialize_term,
    serialize_category,
)
from json import dumps, load


index_cache = Cache(3600 * 24)
NOMI_PATH = os.path.join(os.path.dirname(__file__), "../nomi.geojson")
print(NOMI_PATH)
with open(NOMI_PATH) as f:
    NOMI = load(f)["features"]


def get_index(request):
    images = index_cache.find_or_else(
        "images", lambda: dumps([serialize_image(i) for i in Image.objects.all()])
    )
    image_annotations = index_cache.find_or_else(
        "image_annotations",
        lambda: dumps(
            [serialize_image_annotation(a) for a in ImageAnnotation.objects.all()]
        ),
    )
    paragraph_annotations = index_cache.find_or_else(
        "paragraph_annotations",
        lambda: dumps(
            [serialize_paragraph_annotation(a) for a in ParAnnotation.objects.all()]
        ),
    )
    terms = index_cache.find_or_else(
        "terms", lambda: dumps([serialize_term(t) for t in Term.objects.all()])
    )
    categories = index_cache.find_or_else(
        "categories",
        lambda: dumps([serialize_category(t) for t in TermCategory.objects.all()]),
    )

    categories = index_cache.find_or_else(
        "categories",
        lambda: dumps([serialize_category(t) for t in TermCategory.objects.all()]),
    )
    places = index_cache.find_or_else(
        "places",
        lambda: dumps([serialize_place(p, NOMI) for p in Place.objects.all()]),
    )
    paths = index_cache.find_or_else(
        "paths",
        lambda: dumps([serialize_path(p) for p in Path2.objects.all()]),
    )
    return render(
        request,
        "topo/index.html",
        {
            "images": images,
            "image_annotations": image_annotations,
            "paragraph_annotations": paragraph_annotations,
            "terms": terms,
            "categories": categories,
            "places": places,
            "paths": paths,
        },
    )
